var express = require('express');
var server = express();
var expjwt = require('express-jwt');
var _config = require('./config');
var bodyParser = require('body-parser');
var orphanageApi = require('./api/orphanage/orphanage-api');
var masterAddressApi = require('./api/master-dropdown/address/address.api');
var dropdownApi = require('./api/master-dropdown/dropdowns.api');
var patronageApi = require('./api/patronage/evaluation.api');
var patronApi = require('./api/patronage/patron.api');
var orphanApi = require('./api/orphan/orphan.api');
var orphanDetailApi = require('./api/orphan/evaluation.api');
var evaluationApi = require('./api/evaluation/evaluation.api');
var validateApi = require('./api/validate/validate.api');
var authenApi = require('./api/authen/authen.api');
var fosterApi = require('./api/foster/foster.api');
var advancementApi = require('./api/orphan/advancement.api');
var userApi = require('./api/user/user.api.js');
var originalfamilyApi = require('./api/originalfamily/originalfamily.api');
var expdf = require('./report/example-pdf');
var docx = require('./report/example-docx-tb');
var advancementorphan = require('./report/advancementorphan');
var evaluationorphan = require('./report/evaluationorphan');
var evaluationpatron = require('./report/evaluationpatron');
var historyorphan = require('./report/historyorphan');
var orphan = require('./report/orphan');
var patron = require('./report/patron');
server.use(bodyParser.json({limit:'5mb'})); // support json encoded bodies
server.use(bodyParser.urlencoded({ extended: true,limit:'5mb' })); // support encoded bodies

// Add headers
server.use(function (req, res, next) {
  console.log("Call");
  // Website you wish to allow to connect
  var allowedOrigins = ['http://babylover.ddns.net', 'https://babylover.herokuapp.com', 'http://localhost:4200'];
  var origin = req.headers.origin;
  console.log("origin = " +origin);
  if(allowedOrigins.indexOf(origin) > -1){
    console.log("origin Allow = " + origin);
   res.setHeader('Access-Control-Allow-Origin',  origin);
  }
  //res.setHeader('Access-Control-Allow-Origin',  "*");
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', false);
  // set json
  res.setHeader('Content-Type', 'text/json; charset=utf-8');
  // Pass to next layer of middleware
  next();
});

server.use('/api/orphanage', orphanageApi);
server.use('/api/master', masterAddressApi);
server.use('/api/master/dropdown', dropdownApi);
server.use('/api/patronage/evaluation', patronageApi);
server.use('/api/patronage/patron', patronApi);
// server.use('/api/orphan',expjwt({secret: 'babylover'}),ensureToken,orphanApi);
server.use('/api/orphan', orphanApi);
server.use('/api/evaluation', evaluationApi);
server.use('/api/orphan/evaluation', orphanDetailApi);
server.use('/api/validate',validateApi);
server.use('/authen',authenApi);
server.use('/api/foster', fosterApi);
server.use('/api/originalfamily',originalfamilyApi);
server.use('/api/advancement',advancementApi);
server.use('/api/user',userApi);
server.use('/report', expdf);
server.use('/report', docx);
server.use('/report', advancementorphan);
server.use('/report', evaluationorphan);
server.use('/report', evaluationpatron);
server.use('/report', historyorphan);
server.use('/report', orphan);
server.use('/report', patron);
server.use(_config.image_public_path, express.static(__dirname + '/upload'));

function ensureToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
}

var port = process.env.PORT || 3000;
server.listen(port, "0.0.0.0", function() {
  console.log("Listening on Port " + port);
  console.log("http://localhost:" + port);
});