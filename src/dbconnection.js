const mysql = require('mysql');
const _conf = require('./config');
var util = require('util');

if(_conf.prod){
    
}else{

}

const connection = mysql.createConnection({
    host            : _conf.getDBHost(),
    user            : _conf.getDBUser(),
    password        : _conf.getDBPassword(),
    database        : _conf.getDBName(),
    dateStrings: true
});
connection.connect((err)=>{
    if(err) throw err;
    console.log("Database Connected: " + _conf.getDBHost())
})
connection.query = util.promisify(connection.query);
module.exports = connection;