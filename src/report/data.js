var connection = require('../dbconnection');


var data = {
    GetAdvancement: async (orphanid, orphanadvancementid) => {
        let result;
        let error;
        try {
            let sql = `
            SELECT
            weight,
            height,
            aroundhead,
            chest,
            pastchanges,
            eat,
            sleep,
            excretion,
            characters,
            orphanadvancement.updatedate AS progressdate,
            personorphan.citizenid,
            personorphan.firstname,
            personorphan.lastname,
            personorphan.nickname,
            orphanadvancement.picturepath,
            personorphan.birthdate AS birthdateorphan,
            fn_fullname(personorphan.prefixid, personorphan.firstname , personorphan.lastname) AS fullnameorphan,
            fn_age(personorphan.birthdate) AS ageorphan,
            personpatron.firstname,
            personpatron.lastname,
            fn_fullname(personpatron.prefixid, personpatron.firstname , personpatron.lastname) AS fullnamepatron,
            patronage.joinfamilydate
        FROM
            orphanadvancement
        LEFT JOIN orphan ON orphanadvancement.orphanid = orphan.orphanid
        LEFT JOIN person personorphan ON orphan.personid = personorphan.personid
        LEFT JOIN patronage ON orphanadvancement.orphanid = patronage.orphanid
        LEFT JOIN patron ON patronage.patronid = patron.patronid
        LEFT JOIN person personpatron ON patron.personid = personpatron.personid
        WHERE
            orphanadvancement.orphanid = ?
        AND
            orphanadvancementid = ?        
           
            `

            result = await connection.query(sql, [orphanid, orphanadvancementid])

        } catch (err) {
            error = err;
        }
        console.log("Result > > >", result);

        return [error, result]
    },
    GetOrphan: async (orphanid) => {
        let result;
        let error;
        try {
            let sql = `
            SELECT
                fn_fullname(personorphan.prefixid, personorphan.firstname, personorphan.lastname) AS fullnameorphan,
                personorphan.citizenid,
                personorphan.nickname,
                personorphan.birthdate,
                fn_age(personorphan.birthdate) AS age,
                m_gender.gendername AS gendername,
                m_orphanstatus.name AS orphanstatusname,
                personorphan.picturepath,
                fn_fullname(personpatron.prefixid, personpatron.firstname, personpatron.lastname) AS fullnamepatron,
                addresspatron.homeno AS homenopatron,
                addresspatron.moono AS moonopatron,
                tumbolpatron.name AS tumbolpatronname,
                amphurpatron.name AS amphurpatronname,
                provincepatron.name AS provincepatronname,
                fn_fullname(personfoster.prefixid, personfoster.firstname, personfoster.lastname) AS fullnamefoster,
                addressfoster.homeno AS homenofoster,
                addressfoster.moono AS moonofoster,
                tumbolfoster.name AS tumbolfostername,
                amphurfoster.name AS amphurfostername,
                provincefoster.name AS provincepatronfoster,
                fn_fullname(personoriginalfamily.prefixid, personoriginalfamily.firstname, personoriginalfamily.lastname) AS fullnameoriginalfamily,
                addressoriginalfamily.homeno AS homenooriginalfamily,
                addressoriginalfamily.moono AS moonooriginalfamily,
                tumboloriginalfamily.name AS tumboloriginalfamilyname,
                amphuroriginalfamily.name AS amphuroriginalfamilyname,
                provinceoriginalfamily.name AS provinceoriginalfamilyname						
            FROM
            orphan 
                LEFT JOIN person personorphan ON orphan.personid = personorphan.personid
                LEFT JOIN m_gender ON personorphan.genderid = m_gender.genderid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid 
                LEFT JOIN patronage ON orphan.orphanid = patronage.orphanid
                LEFT JOIN patron ON patronage.patronid = patron.patronid
                LEFT JOIN person personpatron ON patron.personid = personpatron.personid
                LEFT JOIN address addresspatron ON personpatron.personid = addresspatron.personid
                LEFT JOIN m_tumbol tumbolpatron ON addresspatron.tumbolcode = tumbolpatron.code
                LEFT JOIN m_amphur amphurpatron ON addresspatron.amphurcode = amphurpatron.code 
                LEFT JOIN m_province provincepatron ON addresspatron.provincecode = provincepatron.code
                LEFT JOIN foster  ON orphan.orphanid = foster.orphanid
                LEFT JOIN person personfoster ON foster.personid = personfoster.personid
                LEFT JOIN address addressfoster ON personfoster.personid = addressfoster.personid
                LEFT JOIN m_tumbol tumbolfoster ON addressfoster.tumbolcode = tumbolfoster.code
                LEFT JOIN m_amphur amphurfoster ON addressfoster.amphurcode = amphurfoster.code 
                LEFT JOIN m_province provincefoster ON addressfoster.provincecode = provincefoster.code
                LEFT JOIN originalfamily ON orphan.orphanid = originalfamily.orphanid
                LEFT JOIN person personoriginalfamily ON originalfamily.personid = personoriginalfamily.personid
                LEFT JOIN address addressoriginalfamily ON personoriginalfamily.personid = addressoriginalfamily.personid
                LEFT JOIN m_tumbol tumboloriginalfamily ON addressoriginalfamily.tumbolcode = tumboloriginalfamily.code
                LEFT JOIN m_amphur amphuroriginalfamily ON addressoriginalfamily.amphurcode = amphuroriginalfamily.code 
                LEFT JOIN m_province provinceoriginalfamily ON addressoriginalfamily.provincecode = provinceoriginalfamily.code
            WHERE
                orphan.orphanid = ?
            `
            result = await connection.query(sql, [orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result]
    },
    GetHistoryOrphan: async (orphanid) => {
        let result;
        let vaccineresult;
        let error;
        try {
            let sql = `
        SELECT
                personorphan.citizenid,
                personorphan.firstname,
                personorphan.lastname,
				fn_fullname(personorphan.prefixid, personorphan.firstname, personorphan.lastname) AS fullnameorphan,
                personorphan.nickname,
                personorphan.birthdate,
				personpatron.firstname,
                personpatron.lastname,
                personorphan.picturepath,
				fn_fullname(personpatron.prefixid, personpatron.firstname, personpatron.lastname) AS fullnamepatron,
				patronage.joinfamilydate,
				addresspatron.homeno,
				addresspatron.moono,
				m_tumbol.name AS tumbolname,
				m_amphur.name AS amphurname,
				m_province.name AS provincename,
                orphanhistory.orphanhistoryid,
				orphanhistory.bornlocation,
                orphanhistory.fathername,
                orphanhistory.mathername,
				orphanhistory.historybefore,
                orphanhistory.bodygrowth,
                orphanhistory.emotionsocial,
                orphanhistory.languagelearn,
                orphanhistory.planworkwithorphan
            FROM orphan
                INNER JOIN person personorphan ON orphan.personid = personorphan.personid
                LEFT JOIN orphanhistory ON orphan.orphanid = orphanhistory.orphanid
                LEFT JOIN orphanage ON orphan.orphanageid = orphanage.orphanageid	
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
				LEFT JOIN patronage ON orphan.orphanid = patronage.orphanid
				LEFT JOIN patron ON patronage.patronid = patron.patronid
				LEFT JOIN person personpatron ON patron.personid = personpatron.personid
				LEFT JOIN address addresspatron ON personpatron.personid = addresspatron.personid
				LEFT JOIN m_tumbol ON addresspatron.tumbolcode = m_tumbol.code
				LEFT JOIN m_amphur ON addresspatron.amphurcode = m_amphur.code
				LEFT JOIN m_province ON addresspatron.provincecode = m_province.code
            WHERE  
                orphan.orphanid = ?
        `
            result = await connection.query(sql, [orphanid]);
            sql = `
            SELECT
                orphan.orphanid,
                orphan.personid,
                vccinject.vaccineinjectionid,
                vccinject.vaccineid,
                vccinject.vaccinedate,
				m_vaccine.name AS vaccinename
            FROM orphan
                 INNER JOIN vaccineinjection vccinject ON orphan.orphanid = vccinject.orphanid
				LEFT JOIN m_vaccine ON vccinject.vaccineid = m_vaccine.vaccineid
            WHERE 
                orphan.orphanid = ?
        `;
            vaccineresult = await connection.query(sql, [orphanid]);

        } catch (err) {
            error = err;
        }
        return [error, result, vaccineresult]
    },
    GetPatron: async (patronid) => {
        let result;
        let error;
        try {
            let sql = `
            SELECT
                personpatron.citizenid,
	            fn_fullname(personpatron.prefixid, personpatron.firstname, personpatron.lastname) AS fullnamepatron,
	            personpatron.nickname AS nicknamepatron,
	            personpatron.birthdate AS birthdatepatron,
                fn_age(personpatron.birthdate) AS agepatron,
                personpatron.picturepath,
	            m_gender.gendername,
	            m_race.name AS race,
	            m_nationality.name AS nationality,
	            m_religion.name AS religion,
	            m_education.name AS education,
	            personpatron.schoolname AS schoolname,
	            address.homeno AS homeno,
	            address.moono AS moono,
	            m_tumbol.name AS tumbolname,
	            m_amphur.name AS amphurname,
	            m_province.name AS provincename,
	            fn_fullname(personorphan.prefixid, personorphan.firstname, personorphan.lastname) AS fullnameorphan,
	            personorphan.birthdate AS birthdateorphan,
	            personorphan.nickname AS nicknameorphan,
	            fn_age(personorphan.birthdate) AS ageorphan,
	            patronage.patronageid,
	            patronage.joinFamilydate
            FROM
	            patron
                LEFT JOIN person personpatron ON patron.personid = personpatron.personid
                LEFT JOIN m_gender ON personpatron.genderid = m_gender.genderid
                LEFT JOIN m_race ON personpatron.racecode = m_race.code
                LEFT JOIN m_nationality ON personpatron.nationcode = m_nationality.code
                LEFT JOIN m_religion ON personpatron.religioncode = m_religion.code
                LEFT JOIN	m_education ON personpatron.educationcode = m_education.code
                LEFT JOIN address ON personpatron.personid = address.personid
                LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
                LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
                LEFT JOIN m_province ON address.provincecode = m_province.code
                LEFT JOIN patronage ON patron.patronid = patronage.patronid
                LEFT JOIN orphan ON patronage.orphanid = orphan.orphanid
                LEFT JOIN person personorphan ON orphan.personid = personorphan.personid
            WHERE
                patron.patronid = ?
                `

            result = await connection.query(sql, [patronid]);
        } catch (err) {
            error = err;
        }
        return [error, result]
    },
    GetPatronEvalution: async (patronid) => {
        let resultpatronevalution;
        let resultmaritalevalution;
        let resultmembers;
        let error;
        try {
            let sqlpatronevalution = `
            SELECT
                personpatron.citizenid,
	            fn_fullname(personpatron.prefixid, personpatron.firstname, personpatron.lastname) AS fullnamepatron,
                personpatron.nickname AS nicknamepatron,
	            personpatron.birthdate AS birthdatepatron,
                fn_age(personpatron.birthdate) AS agepatron,
                personpatron.picturepath,
	            m_gender.gendername,
	            m_race.name AS race,
	            m_nationality.name AS nationality,
	            m_religion.name AS religion,
	            m_education.name AS education,
	            personpatron.schoolname AS schoolname,
	            address.homeno AS homeno,
	            address.moono AS moono,
	            m_tumbol.name AS tumbolname,
	            m_amphur.name AS amphurname,
                m_province.name AS provincename,
                patron.maritalid,
				patronhistory,
				couplehistory,
				patronhealth,
				patronpositionworkplace,
				patronpersonality,
				patronoccupationdetail,
				income,
				expenses,
				lifestyle,
				familystatus,
				homedetail_sub1,
				homedetail_sub2,
				homedetail_sub3,
				homedetail_sub4,
				environment_sub1,
				environment_sub2,
				otherfact_sub1,
				otherfact_sub2,
				otherfact_sub3,
				otherfact_sub4,
				otherfact_sub5,
				otherfact_sub6,
				otherfact_sub7,
				otherfact_sub8,
				otherfact_sub9,
				otherfact_sub10,
				notice,
				comment AS Comments		
            FROM
	            patron
                LEFT JOIN person personpatron ON patron.personid = personpatron.personid
                LEFT JOIN m_gender ON personpatron.genderid = m_gender.genderid
                LEFT JOIN m_race ON personpatron.racecode = m_race.code
                LEFT JOIN m_nationality ON personpatron.nationcode = m_nationality.code
                LEFT JOIN m_religion ON personpatron.religioncode = m_religion.code
                LEFT JOIN m_education ON personpatron.educationcode = m_education.code
                LEFT JOIN address ON personpatron.personid = address.personid
                LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
                LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
                LEFT JOIN m_province ON address.provincecode = m_province.code
				LEFT JOIN patronevaluation ON patron.patronid = patronevaluation.patronid
            WHERE 
                patron.patronid = ?
                `
                resultpatronevalution = await connection.query(sqlpatronevalution,[patronid]);

            let sqlmaritalevalution = 
            `
                SELECT
                fn_fullname(personmarital.prefixid, personmarital.firstname, personmarital.lastname) AS fullnamemarital,
                personmarital.birthdate AS birthdatemarital,
                fn_age(personmarital.birthdate) AS agemarital,
                m_race.name AS racemarital,
                m_nationality.name AS nationnalitymarital,
                m_religion.name AS religionmarital,
                m_education.name AS educationmarital,
                evalutionmarital.maritalhealth,
                evalutionmarital.maritalpositionworkplace,
                evalutionmarital.maritalpersonality,
                evalutionmarital.maritaloccupationdetail,
                evalutionmarital.maritalhistory,
                m_maritalstatus.name AS namestatusmarital
            FROM
                marital
                LEFT JOIN patron ON marital.maritalid = patron.maritalid 
                LEFT JOIN person personmarital ON marital.personid = personmarital.personid
                LEFT JOIN m_race ON personmarital.racecode = m_race.code
                LEFT JOIN m_nationality ON personmarital.nationcode = m_nationality.code
                LEFT JOIN m_religion ON personmarital.religioncode = m_religion.code
                LEFT JOIN m_education ON personmarital.educationcode = m_education.code
                LEFT JOIN patronevaluation evalutionmarital ON patron.patronid = evalutionmarital.patronid
                LEFT JOIN m_maritalstatus ON marital.maritalstatusid = m_maritalstatus.maritalstatusid
            WHERE
                patron.patronid = ?
                `
                resultmaritalevalution = await connection.query(sqlmaritalevalution,[patronid]);

            let sqlmembers = 
            `
            SELECT
                fn_fullname(personmembers.prefixid, personmembers.firstname, personmembers.lastname) AS fullnamemembers,
                personmembers.birthdate AS birthdatemembers,
                fn_age(personmembers.birthdate) AS agemembers,
                familymember.educationcareer,
                m_relationship.name AS relationshipname
            FROM
                patron
                LEFT JOIN patronevaluation ON patron.patronid = patronevaluation.patronid
                LEFT JOIN familymember ON patronevaluation.patronevaluationid = familymember.patronevaluationid
                LEFT JOIN person personmembers ON familymember.personid = personmembers.personid
                LEFT JOIN m_relationship ON familymember.relationshipid = m_relationship.relationshipid
            WHERE
            patron.patronid = ?
            `
            resultmembers = await connection.query(sqlmembers,[patronid]);
        } catch (err) {
            error = err;
        }
        return [error,resultpatronevalution , resultmaritalevalution ,resultmembers]
    },
    GetDetailOrphanEvalution: async (orphaid) => {
        let error;
        let result;
        try {
            let sql = `
        SELECT
            fn_fullname(person.prefixid, person.firstname, lastname) AS fullnameorphan,
            person.nickname,
            person.birthdate,
            fn_age(person.birthdate) AS ageorphan,
            m_orphanstatus.name AS orphanstatusname 
        FROM
            orphan
            LEFT JOIN person ON orphan.personid = person.personid
            LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
            LEFT JOIN m_orphanstatus ON orphanstatus.statusid = m_orphanstatus.orphanstatusid
        WHERE
            orphan.orphanid = ?    
            `
         result = await connection.query(sql,[orphaid]);   
        } catch (err) {
            error = err;
        }
        return [error , result]
    },
    GetOrphanEvalution: async (ranks, orphanid, traimas) => {
        let error;
        let results;

        console.log("ranks >>>>",ranks);
        
        try {
            let sql = `
            SELECT
                orphan.orphanid, 
                orphan.personid,               
                topic.orphanevaluation_topicid, 
                topic.name AS topicname, 
                sub.orphanevaluation_subid, 
                eva.orphanevaluationid, 
                eva.orphanevaluationtopic_subid, 
                sub.name AS subname, 
                eva.value,
                eva.orphanevaluationtime, 
                eva.ranks
            FROM m_orphanevaluation_topic topic
                INNER JOIN m_orphanevaluation_sub sub ON  sub.orphanevaluation_topicid = topic.orphanevaluation_topicid
                LEFT JOIN orphanevaluation eva ON  eva.orphanevaluationtopic_subid = sub.orphanevaluation_subid AND eva.orphanid = ? AND eva.orphanevaluationtime = ?
                LEFT JOIN orphan ON orphan.orphanid = eva.orphanid
            WHERE 
                eva.ranks = ? 
                AND (eva.orphanid = ? OR ISNULL(eva.orphanid))
                AND (eva.orphanevaluationtime = ? OR ISNULL(eva.orphanevaluationtime))      
            ORDER BY 
                topic.ordinal ASC, 
                sub.ordinal ASC
            `;
            topics = await connection.query(sql, [orphanid, traimas, ranks, orphanid, traimas]);
            let evals = [];
            for (let topic of topics) {
                let head;
                for (let result of evals) {
                    if (topic.orphanevaluation_topicid == result.topic_id) {
                        head = result;
                        continue;
                    }
                }
                if (!head) {
                    head = {
                        topic_id: topic.orphanevaluation_topicid,
                        topic_name: topic.topicname,
                        subs: []
                    };
                    evals.push(head);
                }                
                let sub = {
                    orphanevaluationid: topic.orphanevaluationid,
                    sub_id: topic.orphanevaluation_subid,
                    sub_name: topic.subname,
                    orphanevaluationtopic_subid: topic.orphanevaluationtopic_subid,
                    value: topic.value,
                    traimas: topic.orphanevaluationtime
                };
                head.subs.push(sub);
                
                
                
                
            }
            let [error, result] = await data.GetDetailOrphanEvalution(orphanid);
            results = {
                orphan: result[0],
                evaluations: evals
            };
            
        } catch (err) {
            error = err;
        }
        return [error , results]
    }
}

module.exports = data;