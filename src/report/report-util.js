var docx = require('docx');
module.exports = {
    download_DOCX: async (res, doc, filename)=>{
        var packer = new docx.Packer();
        const b64string = await packer.toBase64String(doc);
        res.setHeader('Content-Disposition', 'attachment; filename='+filename+'.docx');
        res.send(Buffer.from(b64string, 'base64'));
    }
}