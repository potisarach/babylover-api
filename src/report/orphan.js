var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');

//ข้อมูลเด็ก
route.get('/orphan', async (req, res)=>{
    let orphanid = req.query.orphanid; // รับข้อมูล orphanid จากแท็ก a ในไฟล์ orphan-management.component.ts
    let [error, result] = await data.GetOrphan(orphanid);
    let response;
    if(error){
        repoonse = api.error(error);
    }else{
        response = api.success(result, 'Successed')
    }
    let fullname;
    let nickname;
    let birthdate;
    let age;
    let gender;
    let status;
    let patronname;
    let homeP;
    let mooP;
    let tumbolP;
    let amphurP;
    let proviceP;
    let fostername;
    let homeF;
    let mooF;
    let tumbolF;
    let amphurF;
    let proviceF;
    let originname;
    let homeO;
    let mooO;
    let tumbolO;
    let amphurO;
    let proviceO;
    let pic;
    let citizenid;
      
    for(let data of result){
        fullname = data.fullnameorphan,
        nickname = data.nickname,
        birthdate = data.birthdate,
        age = data.age,
        gender = data.gendername,
        status = data.orphanstatusname,
        
        patronname = data.fullnamepatron,
        homeP = data.homenopatron,
        mooP = data.moonopatron,
        tumbolP = data.tumbolpatronname,
        amphurP = data.amphurpatronname,
        proviceP = data.provincepatronname,

        fostername = data.fullnamefoster,
        homeF = data.homenofoster,
        mooF = data.moonofoster,
        tumbolF = data.tumbolfostername,
        amphurF = data.amphurfostername,
        proviceF = data.provincepatronfoster,

        originname = data.fullnameoriginalfamily,
        homeO = data.homenooriginalfamily,
        mooO = data.moonooriginalfamily,
        tumbolO = data.tumboloriginalfamilyname,
        amphurO = data.amphuroriginalfamilyname,
        proviceO = data.provinceoriginalfamilyname,

        pic = data.picturepath
        citizenid = data.citizenid
    }
    let fullbirthdate = birthdate;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }

    birthdate = d + " " + m + " " + y;


    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1500, bottom: 1000, right: 1500 }
        // The theme support is NOT working yet...
        // themeXml: themeXml    
        
    });

    docx.on('error', function (err) {
        console.log(err)
    });
    var fontface = 'TH SarabunPSK';
    var fontsizemaintopic = '18';
    var fontsizesubheading ='16';
    var fontsizedetail = '14';
    var pObj = docx.createP({align: 'left'})
    pObj.addText('ข้อมูลของเด็กในโครงการ', {font_face: fontface,font_size: fontsizemaintopic,bold: true, underline: true});
    

//
if(pic != null){
    var pObj = docx.createP({align: 'center'})
    pObj.addImage ( path.resolve(__dirname,'../upload/img/person/'+pic),{cx:150,cy:200} );
}

var table = [
    [{
      val: "ชื่อ-นามสกุล : "+fullname,
      opts: {
        cellColWidth: 5000,
        fontFamily: fontface,
        sz: '28'
      }
    },{
      val: "ชื่อเล่น : "+nickname,
      opts: {
        fontFamily: fontface,
        sz: '28'
      }
    }],
    [{
        val:'วัน/เดือน/ปี เกิด : '+birthdate,
        opts: {
           fontFamily: fontface,
           sz: '28'
        }
    },{ 
        val:'อายุ : '+age + ' ปี',
        opts: {
            fontFamily: fontface,
            sz: '28'
         }
    }],
    [{
        val:'เพศ : '+gender,
        opts: {
            fontFamily: fontface,
            sz: '28'
         }
    }],
    [{
        val:'สถานะ : '+status,
        opts: {
            fontFamily: fontface,
            sz: '28'
         }  
    }],
  ]
   
  var tableStyle = {
    tableColWidth: 4261,
    // tableSize: 20,
    // tableColor: "ada",
    tableAlign: "left",
    borders: false
  }
   
  docx.createTable (table, tableStyle); 

  
  if(patronname != null && fostername == null && originname == null){
    var pObj = docx.createP({align: 'center'})
    pObj.addText('----------------------------------------------------------------------------------------------------------------------------------------');
    var pObj = docx.createP()
    pObj.addText('ข้อมูลอุปถัมภ์',{font_face: fontface,font_size: fontsizesubheading,bold: true, underline: true}); 
    var pObj = docx.createP()
    pObj.addText('ชื่อ-นามสกุล : '+ patronname ,{font_face: fontface,font_size: fontsizedetail}); 
    var pObj = docx.createP()
    pObj.addText('ที่อยู่ : บ้านเลขที่ '+ homeP +' หมู่ที่ '+ mooP +' ตำบล '+ tumbolP +' อำเภอ '+ amphurP +' จังหวัด '+ proviceP,{font_face: fontface,font_size: fontsizedetail});    

  }else if(fostername != null && originname == null && patronname == null){
    var pObj = docx.createP({align: 'center'})
    pObj.addText('----------------------------------------------------------------------------------------------------------------------------------------');
    var pObj = docx.createP()
    pObj.addText('ข้อมูลผู้บุญธรรม',{font_face: fontface,font_size: '22',bold: true, underline: true}); 
    var pObj = docx.createP()
    pObj.addText('ชื่อ-นามสกุล : '+ fostername ,{font_face: fontface,font_size: '18'}); 
    var pObj = docx.createP()
    pObj.addText('ที่อยู่ : บ้านเลขที่ '+ homeF +' หมู่ที่ '+ mooF +' ตำบล '+ tumbolF +' อำเภอ '+ amphurF +' จังหวัด '+ proviceF ,{font_face: fontface,font_size: fontsizedetail});    

  }else if(originname!=null && patronname == null && fostername == null){
    var pObj = docx.createP({align: 'center'})
    pObj.addText('----------------------------------------------------------------------------------------------------------------------------------------');
    var pObj = docx.createP()
    pObj.addText('ข้อมูลผู้ปกครอง',{font_face: fontface,font_size: fontsizedetail,bold: true, underline: true}); 
    var pObj = docx.createP()
    pObj.addText('ชื่อ-นามสกุล : '+ originname ,{font_face: fontface,font_size: fontsizedetail}); 
    var pObj = docx.createP()
    pObj.addText('ที่อยู่ : บ้านเลขที่ '+ homeO +' หมู่ที่ '+ mooO +' ตำบล '+ tumbolO +' อำเภอ '+ amphurO +' จังหวัด '+ proviceO ,{font_face: fontface,font_size: fontsizedetail});    
  }
  var toDay = new Date().toISOString().slice(0,10);
      res.setHeader('Content-Disposition', 'attachment; filename=orphan_'+citizenid+'_'+toDay+'.docx');
      docx.generate(res);
    });

module.exports = route;
