var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');

//Patron
route.get('/patron', async (req, res)=>{

    let patronid = req.query.patronid; 
    let [error, result] = await data.GetPatron(patronid);
    let response;
    if(error){
        repoonse = api.error(error);
    }else{
        response = api.success(result, 'Successed')
    }
    let citizenid;
    let fullnamepatron;
    let nicknamepatron;
    let birthdatepatron;
    let agepatron;
    let gendername;
    let race;
    let nationality;
    let religion;
    let education;
    let schoolname;
    let homeno;
    let moono;
    let tumbolname;
    let amphurname;
    let provincename;
    let fullnameorphan;
    let birthdateorphan;
    let nicknameorphan;
    let ageorphan;  
    let joinFamilydate;
    let pic;
    
    for(let data of result){
        citizenid = data.citizenid,
        fullnamepatron = data.fullnamepatron,
        nicknamepatron = data.nicknamepatron,
        birthdatepatron = data.birthdatepatron,
        agepatron = data.agepatron,
        gendername = data.gendername,
        fullnameorphan = data.fullnameorphan,
        birthdateorphan = data.birthdateorphan,
        nicknameorphan = data.nicknameorphan,
        ageorphan = data.ageorphan,
        race = data.race,
        nationality = data.nationality,
        religion = data.religion,
        education = data.education,
        schoolname = data.schoolname,
        homeno = data.homeno,
        moono = data.moono,
        tumbolname = data.tumbolname,
        amphurname = data.amphurname,
        provincename = data.provincename,
        joinFamilydate = data.joinFamilydate,
        pic = data.picturepath
    }

    let fullbirthdate = birthdatepatron;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;
    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }

    birthdatepatron = d + " " + m + " " + y;
    // จบวันที่

    console.log("birthdateorphan : ",birthdateorphan);
    
    if(birthdateorphan != null){
    let fullbirthdateorphan = birthdateorphan;
    let yearorphan = fullbirthdateorphan.substr(0, 4);
    let monthorphan = fullbirthdateorphan.substr(5, 2);
    let dayorphan = fullbirthdateorphan.substr(8, 2);
    let yorphan;
    let morphan;
    let dorphan;
    if (monthorphan == "01") {
        morphan = "มกราคม";
    } else if (monthorphan == "02") {
        morphan = "กุมภาพันธ์";
    } else if (monthorphan == "03") {
        morphan = "มีนาคม";
    } else if (monthorphan == "04") {
        morphan = "เมษายน";
    } else if (monthorphan == "05") {
        morphan = "พฤษภาคม";
    } else if (monthorphan == "06") {
        morphan = "มิถุนายน";
    } else if (monthorphan == "07") {
        morphan = "กรกฎาคม";
    } else if (monthorphan == "08") {
        morphan = "สิงหาคม";
    } else if (monthorphan == "09") {
        morphan = "กันยายน";
    } else if (monthorphan == "10") {
        morphan = "ตุลาคม";
    } else if (monthorphan == "11") {
        morphan = "พฤศจิกายน";
    } else if (monthorphan == "12") {
        morphan = "ธันวาคม";
    }

    let yearsorphan = parseInt(yearorphan) + 543;
    yorphan = yearsorphan;

    if (dayorphan < "10") {
        dorphan = dayorphan.substr(1, 1);
    } else {
        dorphan = dayorphan;
    }

    birthdateorphan = dorphan + " " + morphan + " " + yorphan;
    // จบวันที่
    }
    if(joinFamilydate != null){
        
    let fullbirthdatejoin = joinFamilydate;
    let yearjoin = fullbirthdatejoin.substr(0, 4);
    let monthjoin = fullbirthdatejoin.substr(5, 2);
    let dayjoin = fullbirthdatejoin.substr(8, 2);
    let yjoin;
    let mjoin;
    let djoin;
    if (monthjoin == "01") {
        mjoin = "มกราคม";
    } else if (monthjoin == "02") {
        mjoin = "กุมภาพันธ์";
    } else if (monthjoin == "03") {
        mjoin = "มีนาคม";
    } else if (monthjoin == "04") {
        mjoin = "เมษายน";
    } else if (monthjoin == "05") {
        mjoin = "พฤษภาคม";
    } else if (monthjoin == "06") {
        mjoin = "มิถุนายน";
    } else if (monthjoin == "07") {
        mjoin = "กรกฎาคม";
    } else if (monthjoin == "08") {
        mjoin = "สิงหาคม";
    } else if (monthjoin == "09") {
        mjoin = "กันยายน";
    } else if (monthjoin == "10") {
        mjoin = "ตุลาคม";
    } else if (monthjoin == "11") {
        mjoin = "พฤศจิกายน";
    } else if (monthjoin == "12") {
        mjoin = "ธันวาคม";
    }

    let yearsjoin = parseInt(yearjoin) + 543;
    yjoin = yearsjoin;

    if (dayjoin < "10") {
        djoin = dayjoin.substr(1, 1);
    } else {
        djoin = dayjoin;
    }

    joinFamilydate = djoin + " " + mjoin + " " + yjoin;
    }

    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1000, bottom: 1000, right: 1000 }
        // The theme support is NOT working yet...
        // themeXml: themeXml    
        
    });

    docx.on('error', function (err) {
        console.log(err)
    });
    var fontface = 'TH SarabunPSK';
    var fontsizemaintopic = '18';
    var fontsizesubheading ='16';
    var fontsizedetail = '14';
    var pObj = docx.createP({align: 'center'})
    pObj.addText('ข้อมูลผู้อุปถัมภ์', {font_face: fontface, font_size: fontsizemaintopic, bold: true, underline: true});
console.log(pic);

if(pic != null){
    var pObj = docx.createP({align: 'center'})
    pObj.addImage ( path.resolve(__dirname,'../upload/img/person/'+pic),{cx:150,cy:200} );
}

    var table = [
        [{
            val: " ชื่อ-นามสกุล:  " + fullnamepatron,
            opts: {
                cellColWidth: 5000,
                
                fontFamily: fontface,
                sz: '28'
            }
        },{
            val: " ชื่อเล่น:  " +  nicknamepatron,
            opts: {
                cellColWidth: 5000,
                
                fontFamily: fontface,
                sz: '28'
            }
        }],

        [{
            val:' วัน/เดือน/ปี เกิด:  ' + birthdatepatron,
            opts: {
                
                fontFamily: fontface,
                sz: '28'
                }
        },{
            val: ' อายุ:  ' + agepatron + ' ปี ',
            opts:{
                
                fontFamily: fontface,
                sz: '28'
            }
        }],   

        [{
           val:' เพศ:  ' + gendername,
           opts: {
                    
            fontFamily: fontface,
            sz: '28'
                }    
        },{
               val: ' สัญชาติ:  ' + race,
           opts: {
                    
            fontFamily: fontface,
            sz: '28'
                }
            }],

        [{
            val:' เชื้อชาติ:  ' + nationality,
        opts:{
            fontFamily: fontface,
            sz: '28'
        }
        },{
            val: ' ศาสนา:  ' + religion,
            opts:{
            
                fontFamily: fontface,
                sz: '28'
            }
        }],


        [{
            val:' ระดับการศึกษา:  ' + education,
            opts: {
                    
                fontFamily: fontface,
                sz: '28'
            }
        },{
            val: ' ชื่อสถานศึกษา:  ' + schoolname,
            opts:{
                    
                fontFamily: fontface,
                sz: '28'
            }
        }],

        // [{  
        //     val: ' ที่อยู่ ' + homeno + " หมู่ " + moono + " ตำบล " + tumbolname + " อำเภอ " + amphurname + " จังหวัด " + provincename,
        // opts: {
        //         b:true,
        //         fontFamily: "TH Sarabun"
        //     }
        // }],

    ]
     
    var tableStyle = {
        sz:33,
        tableColWidth: 4261,
        tableSize: 24,
        tableColor: "ada",
        tableAlign: "left",
        // tableFontFamily: "Comic Sans MS"
    }
     
    docx.createTable (table, tableStyle);

    var pObj = docx.createP()
    pObj.addText('ที่อยู่:  '+ homeno + " หมู่ " + moono + " ตำบล " + tumbolname + " อำเภอ " + amphurname + " จังหวัด " + provincename, {font_face: fontface , font_size: fontsizedetail});

    if(fullnameorphan != null){
    var pObj = docx.createP({align: 'left'})
    pObj.addText('--------------------------------------------------------------------------------------------------------------------------------------------------------');

    var pObj = docx.createP()
    pObj.addText('ข้อมูลเด็กที่รับอุปถัมภ์', {font_face: fontface , font_size: fontsizemaintopic, bold: true ,underline: true});

    var pObj = docx.createP()
    pObj.addText('ชื่อ-นามสกุล:  '+ fullnameorphan, {font_face: fontface , font_size: fontsizedetail});
    
    var pObj = docx.createP()
    pObj.addText('ชื่อเล่น:  '+ nicknameorphan , {font_face: fontface , font_size: fontsizedetail});

    var pObj = docx.createP()
    pObj.addText('อายุ:  '+ ageorphan + ' ปี ', {font_face: fontface , font_size: fontsizedetail});

    var pObj = docx.createP()
    pObj.addText('วันเกิด:  '+ birthdateorphan , {font_face: fontface , font_size: fontsizedetail});

    var pObj = docx.createP()
    pObj.addText('วันที่เข้าสู่ครอบครัวอุปถัมภ์:  '+ joinFamilydate , {font_face: fontface , font_size: fontsizedetail});
    }
    var toDay = new Date().toISOString().slice(0,10);
      res.setHeader('Content-Disposition', 'attachment; filename=patron_'+citizenid+'_'+toDay+'.docx');
      docx.generate(res);
    });
    module.exports = route;
