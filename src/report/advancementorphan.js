var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');


// report รายงานความก้าวหน้า (เมนูประเมินเด็กฯ)
route.get('/advancement', async (req, res) => {

    let orphanid = req.query.orphanid;
    let orphanadvancementid = req.query.orphanadvancementid;
    let [error, result] = await data.GetAdvancement(orphanid, orphanadvancementid);
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(result, 'Successed')
    }
    let picturepath;
    let weight;
    let height;
    let aroundhead;
    let chest;
    let pastchanges;
    let eat;
    let sleep;
    let excretion;
    let characters;
    let progressdate;
    let fullnameorphan;
    let nickname;
    let birthdateorphan;
    let ageorphan;
    let fullnamepatron;
    let citizenid;

    for (let data of result) {
        picturepath = data.picturepath;
        weight = data.weight;
        height = data.height;
        aroundhead = data.aroundhead;
        chest = data.chest;
        pastchanges = data.pastchanges;
        eat = data.eat;
        sleep = data.sleep;
        excretion = data.excretion;
        characters = data.characters;
        progressdate = data.progressdate;
        fullnameorphan = data.fullnameorphan;
        nickname = data.nickname;
        birthdateorphan = data.birthdateorphan;
        ageorphan = data.ageorphan;
        fullnamepatron = data.fullnamepatron;
        citizenid = data.citizenid;
        joinfamilydate = data.joinfamilydate;
    }

    let fullbirthdate = birthdateorphan;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }

    birthdateorphan = d + " " + m + " " + y;
    if(joinfamilydate != null){
        let fulljoinfamilydate = joinfamilydate;
        let yearjoinfamilydate = fulljoinfamilydate.substr(0, 4);
        let monthjoinfamilydate = fulljoinfamilydate.substr(5, 2);
        let dayjoinfamilydate = fulljoinfamilydate.substr(8, 2);
        let yjoinfamilydate;
        let mjoinfamilydate;
        let djoinfamilydate;
    
        if (monthjoinfamilydate == "01") {
            mjoinfamilydate = "มกราคม";
        } else if (monthjoinfamilydate == "02") {
            mjoinfamilydate = "กุมภาพันธ์";
        } else if (monthjoinfamilydate == "03") {
            mjoinfamilydate = "มีนาคม";
        } else if (monthjoinfamilydate == "04") {
            mjoinfamilydate = "เมษายน";
        } else if (monthjoinfamilydate == "05") {
            mjoinfamilydate = "พฤษภาคม";
        } else if (monthjoinfamilydate == "06") {
            mjoinfamilydate = "มิถุนายน";
        } else if (monthjoinfamilydate == "07") {
            mjoinfamilydate = "กรกฎาคม";
        } else if (monthjoinfamilydate == "08") {
            mjoinfamilydate = "สิงหาคม";
        } else if (monthjoinfamilydate == "09") {
            mjoinfamilydate = "กันยายน";
        } else if (monthjoinfamilydate == "10") {
            mjoinfamilydate = "ตุลาคม";
        } else if (monthjoinfamilydate == "11") {
            mjoinfamilydate = "พฤศจิกายน";
        } else if (monthjoinfamilydate == "12") {
            mjoinfamilydate = "ธันวาคม";
        }
    
        let yearsjoinfamilydate = parseInt(yearjoinfamilydate) + 543;
        yjoinfamilydate = yearsjoinfamilydate;
    
        if (dayjoinfamilydate < "10") {
            djoinfamilydate = dayjoinfamilydate.substr(1, 1);
        } else {
            djoinfamilydate = dayjoinfamilydate;
        }
    
        joinfamilydate = djoinfamilydate + " " + mjoinfamilydate + " " + yjoinfamilydate;
    }
    

    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1500, bottom: 1000, right: 1500 }
        // The theme support is NOT working yet...
        // themeXml: themeXml    

    });

    docx.on('error', function (err) {
        console.log(err)
    });

    var fontface = 'TH SarabunPSK';
    var fontsizemaintopic = '18';
    var fontsizesubheading ='16';
    var fontsizedetail = '14';
    
    var pObj = docx.createP({ align: 'center' })
    pObj.addText('มูลนิธิพัฒนาชีวิตชนบท', { font_face: fontface, font_size: fontsizemaintopic, bold: true });
    var pObj = docx.createP({ align: 'center' })
    pObj.addText('รายงานความก้าวหน้าของเด็กในโครงการ', { font_face: fontface, font_size: fontsizemaintopic, bold: true });
        
    if(picturepath != null){
        var pObj = docx.createP({ align: 'right' })
        pObj.addImage(path.resolve(__dirname, '../upload/img/person/' + picturepath), { cx: 200, cy: 200 });
    }
    
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ชื่อ : ' + fullnameorphan + " (" + nickname + ")", { font_face: fontface, font_size: fontsizedetail });
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('วันเดินปีเกิด : ' + birthdateorphan + "     " + "อายุ : " + ageorphan + " ปี", { font_face: fontface, font_size: fontsizedetail });
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('น้ำหนัก : ' + weight + " กิโลกรัม" + "     " + "ส่วนสูง : " + height + " เซนติเมตร", { font_face: fontface, font_size: fontsizedetail });
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('รอบศรีษะ : ' + aroundhead + " เซนติเมตร" + "     " + "รอบอก : " + chest + " เซนติเมตร", { font_face: fontface, font_size: fontsizedetail });
    if(fullnamepatron == null){
        fullnamepatron = '-'
        joinfamilydate = '-'
    }
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ชื่อครอบครัวอุปการะ : ' + fullnamepatron, { font_face: fontface, font_size: fontsizedetail });
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('วันที่มาอยู่กับครอบครัว : ' + joinfamilydate, { font_face: fontface, font_size: fontsizedetail });


    var pObj = docx.createP()

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('สภาพเด็กโดยทั่วไป และการเปลี่ยนแปลงที่ผ่านมา', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP()
    pObj.addText(pastchanges, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('การรับประทานอาหาร', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP()
    pObj.addText(eat, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('การนอน', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP()
    pObj.addText(sleep, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('การขับถ่าย', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP()
    pObj.addText(excretion, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('อุปนิสัยและลักษณ์ทั่วไป', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP()
    pObj.addText(characters, { font_face: fontface, font_size: fontsizedetail });
    var toDay = new Date().toISOString().slice(0,10);
    res.setHeader('Content-Disposition', 'attachment; filename=advancement_'+citizenid+'_'+toDay+'.docx');
    docx.generate(res);
});
module.exports = route;
