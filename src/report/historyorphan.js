var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');

// report ประวัติก่อนเข้าโครงการเด็ก (เมนูจัดการเด็กในอุปถัมภ์)
route.get('/historyorphan', async (req, res) => {
    let orphanid = req.query.orphanid; // รับข้อมูล orphanid จากแท็ก a ในไฟล์ orphan-management.component.ts
    let [error, result, vaccineresult] = await data.GetHistoryOrphan(orphanid);
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(result, 'Successed')

    }
    let citizenid = result[0].citizenid;
    let picturepath = result[0].picturepath;
    let fullnameorphan = result[0].fullnameorphan;
    let birthdate;
    let bornlocation = result[0].bornlocation;
    let fathername = result[0].fathername;
    let mathername = result[0].mathername;
    let historybefore = result[0].historybefore;
    let bodygrowth = result[0].bodygrowth;
    let emotionsocial = result[0].emotionsocial;
    let languagelearn = result[0].languagelearn;
    let planworkwithorphan = result[0].planworkwithorphan;

    // วันที่

    let fullbirthdate = result[0].birthdate;;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }

    birthdate = d + " " + m + " " + y;

    // จบวันที่

    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1500, bottom: 1000, right: 1500 }
        // The theme support is NOT working yet...
        // themeXml: themeXml    

    });

    docx.on('error', function (err) {
        console.log(err)
    });
   
    var fontface = 'TH SarabunPSK';
    var fontsizemaintopic = '18';
    var fontsizesubheading ='16';
    var fontsizedetail = '14';

    var pObj = docx.createP({ align: 'center' })
    pObj.addText('มูลนิธิพัฒนาชีวิตชนบท', { font_face: fontface, font_size: fontsizemaintopic, bold: true });
    var pObj = docx.createP({ align: 'center' })
    pObj.addText('ประวัติเด็กก่อนเข้าโครงการครอบครัวอุปการะ', { font_face: fontface, font_size: fontsizemaintopic, bold: true });

    if(picturepath != null){
        var pObj = docx.createP({ align: 'center' })
        pObj.addImage(path.resolve(__dirname, '../upload/img/person/' + picturepath), { cx: 150, cy: 200 });
    }
    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ชื่อ :     ', { font_face: fontface, font_size: fontsizedetail});
    pObj.addText(fullnameorphan, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('วันเดือนปีเกิด :     ', { font_face: fontface, font_size: fontsizedetail });
    pObj.addText(birthdate, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('สถานที่เกิด :     ', { font_face: fontface, font_size: fontsizedetail });
    pObj.addText(bornlocation, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ชื่อบิดา :     ', { font_face: fontface, font_size: fontsizedetail });
    pObj.addText(fathername, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ชื่อมารดา :     ', { font_face: fontface, font_size: fontsizedetail });
    pObj.addText(mathername, { font_face: fontface, font_size: fontsizedetail });

    // var pObj = docx.createP()
    // pObj.options.align = 'left'
    // pObj.addText('รหัส : ', { font_face: 'TH SarabunPSK', font_size: '16', bold: true });


    var pObj = docx.createP({ align: 'center' })
    pObj.addText('_____________________________________________________________');

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ประวัติก่อนเข้าโครงการ', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText(historybefore, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ประวัติการฉีดวัคซีนป้อนกันโรค', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });

    for (let index = 0; index < vaccineresult.length; index++) {
        // วนลูปแสดงค่าวันที่และชื่อวัคซีนที่เด็กฉีดแล้ว
        if (index == 0) {
            var table = [

                [{
                    val: "วัน เดือน ปี",
                    opts: {
                        b: true,
                        align: "center",
                        sz: '28',
                    }
                }, {
                    val: "ชื่อวัคซีน",
                    opts: {
                        b: true,
                        sz: '28',
                        align: "center",
                    }
                }],
            ]

            var tableStyle = {
                tableColWidth: 4261,
                // tableSize: 48,
                tableColor: "ada",
                tableAlign: "left",
                tableFontFamily: "TH SarabunPSK",
                borders: true
            }

            docx.createTable(table, tableStyle); // สร้างตาราง

        }

        // วันที่

        let vaccinedate = vaccineresult[index].vaccinedate;
        let year = vaccinedate.substr(0, 4);
        let month = vaccinedate.substr(5, 2);
        let day = vaccinedate.substr(8, 2);
        let y;
        let m;
        let d;

        if (month == "01") {
            m = "มกราคม";
        } else if (month == "02") {
            m = "กุมภาพันธ์";
        } else if (month == "03") {
            m = "มีนาคม";
        } else if (month == "04") {
            m = "เมษายน";
        } else if (month == "05") {
            m = "พฤษภาคม";
        } else if (month == "06") {
            m = "มิถุนายน";
        } else if (month == "07") {
            m = "กรกฎาคม";
        } else if (month == "08") {
            m = "สิงหาคม";
        } else if (month == "09") {
            m = "กันยายน";
        } else if (month == "10") {
            m = "ตุลาคม";
        } else if (month == "11") {
            m = "พฤศจิกายน";
        } else if (month == "12") {
            m = "ธันวาคม";
        }

        let years = parseInt(year) + 543;
        y = years;

        if (day < "10") {
            d = day.substr(1, 1);
        } else {
            d = day;
        }

        vaccineresult[index].vaccinedate = d + " " + m + " " + y;

        // จบวันที่



        var table = [

            [{
                val: vaccineresult[index].vaccinedate,
                opts: {

                    align: "center",

                    sz: '28',


                }
            }, {
                val: vaccineresult[index].vaccinename,
                opts: {
                    sz: '28',
                    align: "left",
                }

            }],

        ]

        var tableStyle = {
            tableColWidth: 4261,
            // tableSize: 48,
            tableColor: "ada",
            tableAlign: "left",
            tableFontFamily: fontface,
            borders: true
        }

        docx.createTable(table, tableStyle); // สร้างตาราง

    }

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('แรกรับ พัฒนาการด้านต่าง ๆ', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ด้านร่างกาย', { font_face: fontface, font_size: fontsizesubheading, bold: true });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText(bodygrowth, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ด้าสังคมและอารมณ์', { font_face: fontface, font_size: fontsizesubheading, bold: true });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText(emotionsocial, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP()
    pObj.options.align = 'left'
    pObj.addText('ด้านภาษาและการเรียนรู้', { font_face: fontface, font_size: fontsizesubheading, bold: true });

    var pObj = docx.createP({ align: 'left' })
    pObj.addText(languagelearn, { font_face: fontface, font_size: fontsizedetail });

    var pObj = docx.createP({ align: 'center' })
    pObj.addText('_____________________________________________________________');

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('แผนการดำเนินงานกับเด็ก', { font_face: fontface, font_size: fontsizesubheading, bold: true, underline: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText(planworkwithorphan, { font_face: fontface, font_size: fontsizedetail });

    var toDay = new Date().toISOString().slice(0,10);
    res.setHeader('Content-Disposition', 'attachment; filename=historyorphan_'+citizenid+'_'+toDay+'.docx');
    docx.generate(res);
});

module.exports = route;
