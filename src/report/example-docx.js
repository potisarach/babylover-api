var docx = require("docx");
var fs = require('fs');
var express = require('express');
var route = express.Router();

module.exports = route.get('/example-doc', (req, res) => {
    var doc = new docx.Document();
    var paragraph = new docx.Paragraph();

    doc.Styles.createParagraphStyle('header1').color("000000").bold().size(40).font('TH SarabunPSK').center();
    doc.Styles.createParagraphStyle('header2').bold().size(32).font('TH SarabunPSK').left().next('Normal');

    doc.addParagraph(new docx.Paragraph("แบบประเมินสภาพความเป็นอยู่ และความเหมาะสมของผู้อุปการะเด็ก โครงการส่งเสริมพัฒนาเยาวชนภายใต้ครอบครัวอุปการและชุมชนวัฒนธรรม มูลนิธิพัฒนาชีวิตชนบท").style('header1').thematicBreak());
    doc.addParagraph(new docx.Paragraph(""));
    var arr = ["30 มกราคม 2552 เวลา 13.00 น.", "26 สิงหาคม 2556 เวลา 09.00 น.", "04 มกราคม 2559 เวลา 08.40 น."];

    for (let i = 0; i < arr.length; i++) {
        doc.addParagraph(new docx.Paragraph("สอบครั้งที่ " + (i + 1) + "      :  วันที่ " + arr[i]).style('header2'));
    }

    doc.addParagraph(new docx.Paragraph("ชื่อผู้อุปการะเด็ก :  นายสมพร - นางสุพรรณ์ บึงชาลี").style('header2'));


    let address = {
        homeno: "176",
        moono: "1",
        tumbolname: "บุโพธิ์",
        amphurname: "ลำปลายมาศ",
        provincename: "บุรีรัมย์"
    }
    doc.addParagraph(new docx.Paragraph("ที่อยู่ปัจจุบัน      :  เลขที่ " + address.homeno + " หมู่ที่ " + address.moono + " ตำบล" + address.tumbolname + " อำเภอ" + address.amphurname + " จังหวัด" + address.provincename).style('header2').thematicBreak());
    doc.addParagraph(new docx.Paragraph(""));

    doc.addParagraph(new docx.Paragraph("1.  ข้อเท็จจริงเกี่ยวกับตัวผู้ขออุปการะเด็ก").style('header2'));

    var paragraph = new docx.Paragraph("    ก.  ชื่อ - นามสกุล  ").style('header2');
    doc.addParagraph(paragraph);

    paragraph.addRun(new docx.TextRun("นางสุพรรณ์ บึงชาลี"));


    const packer = new docx.Packer();

    packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync("Test_Document.docx", buffer);
        res.download("Test_Document.docx");
    });


});