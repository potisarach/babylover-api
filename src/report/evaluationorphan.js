var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');

// แบบประเมินพัฒนาการเด็ก
route.get('/evaluationorphan', async (req, res)=>{
    let orphanid = req.query.orphanid;
    let age = req.query.age;
    let traimas = req.query.traimas;
    let datarank;
    console.log("age",age);
    
    if(age >= 5.1 && age <= 6.0 ){
        datarank = {"ranks": '5-6'};
    }else if(age >= 4.1 && age <= 5.0 ){
        datarank = {"ranks": '4-5'};
    }else if(age >= 3.1 && age <= 4.0 ){
        datarank = {"ranks": '3-4'};
    }else if(age >= 2.1 && age <= 3.0 ){
        datarank = {"ranks": '2-3'};
    }else if(age >= 1.0 && age <= 2.0 ){
        datarank = {"ranks": '1-2'};
    }else{
        responseData.error('error');
    }
    let [error, results] = await data.GetOrphanEvalution(datarank.ranks, orphanid, traimas);
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(results, 'Successed')
    }

    let fullbirthdate = results.orphan.birthdate;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;
    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }

    results.orphan.birthdate = d + " " + m + " " + y;

    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1500, bottom: 1000, right: 1500 }   

    });

    docx.on('error', function (err) {
        console.log(err)
    });
   
    var fontface = 'TH SarabunPSK';
    var fontsizemaintopic = '18';
    var fontsizesubheading ='16';
    var fontsizedetail = '14';
 
    var pObj = docx.createP({ align: 'center' })
    pObj.addText('การประเมินพัฒนาการสำหรับเด็กครั้งที่ '+traimas , {font_face: fontface , font_size: fontsizemaintopic , bold: true});

    var pObj = docx.createP()
    pObj.addText('ชื่อ-นามสกุล:  '+ results.orphan.fullnameorphan + "          " + 'ชื่อเล่น:  '+ results.orphan.nickname , {font_face: fontface , font_size: fontsizedetail});

    var pObj = docx.createP()
    pObj.addText('วันเกิด:  '+ results.orphan.birthdate + "                                 " + 'อายุ:  '+ results.orphan.ageorphan + ' ปี ', {font_face: fontface , font_size: fontsizedetail});

    var pObj = docx.createP()
    pObj.addText('สถานะ:  '+ results.orphan.orphanstatusname , {font_face: fontface , font_size: fontsizedetail});

    let i = 0;
    let j = 1;
    let can;
    let cannot;
    for(let item of results.evaluations){
        var pObj = docx.createP()
        var pObj = docx.createP ({ backline: '' });
        pObj.addText(item.topic_name , {font_face: fontface , font_size: fontsizesubheading, underline: true, bold: true});
        
        if(i == 0){
            var table = [
                [{
                  val: "ความสามารถ",
                  opts: {
                    b:true,
                    align: "center",
                    cellColWidth: 4261,
                    fontFamily: fontface,
                    sz: 32
                  }
                },{
                  val: "ทำได้",
                  opts: {
                    b: true,
                    align: "center",
                    // cellColWidth: 200,
                    fontFamily: fontface,
                    sz: 32
                  }
                },{
                    val: "ทำไม่ได้",
                    opts: {
                    b: true,
                    align: "center",
                    // cellColWidth: 200,
                    fontFamily: fontface,
                    sz: 32
                    }
                  }],
              ]
               
              var tableStyle = {
                tableColWidth: 4261,
                // tableSize: 20,
                // tableColor: "ada",
                tableAlign: "left",
                borders: true
              }
               
              docx.createTable (table, tableStyle); 
               
        }
      
        for(let sub of item.subs){
            if(sub.value == 1){
                can = '/'
            }else{
                can = ''
            }
            if(sub.value == 0){
                cannot = '/'  
            }else{
                cannot = ' '
            }            
                
            var table = [
                [{
                  val: j+"."+sub.sub_name,
                  opts: {
                    cellColWidth: 4261,
                    fontFamily: fontface,
                    sz: 28
                  }
                },{
                  val: can,
                  opts: {
                    align: "center",
                    // cellColWidth: 200,
                    fontFamily: fontface,
                    sz: 28
                  }
                },{
                    val: cannot,
                    opts: {
                      align: "center",
                    // cellColWidth: 200,
                    fontFamily: fontface,
                    sz: 28
                    }
                  }],
              ]
               
              var tableStyle = {
                tableColWidth: 4261,
                // tableSize: 20,
                // tableColor: "ada",
                tableAlign: "left",
                borders: true
              }
               
              docx.createTable (table, tableStyle); 
        
            j++;
        } 
            j = 1;  
    }
    

    var toDay = new Date().toISOString().slice(0,10);
    res.setHeader('Content-Disposition', 'attachment; filename=evaluationorphan_'+toDay+'.docx');
    docx.generate(res);
    
    });

    module.exports = route;
