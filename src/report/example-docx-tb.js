var express = require('express');
var route = express.Router();
var docx = require('docx');
var rutil = require('./report-util');
const fs = require('fs');
route.get('/docx-tb', (req, res)=>{
    var doc = new docx.Document(undefined, {
        top: 800,
        right: 800,
        bottom: 800,
        left: 800
    });

    doc.Styles.createParagraphStyle('header1').bold().size(36).center().font('TH Sarabun New');
    doc.Styles.createParagraphStyle('th-head').bold().size(32).center().font('TH Sarabun New');
    doc.Styles.createParagraphStyle('style2').color("0069ad").size(38);// = font size x 2

    doc.createParagraph("การประเมินพัฒนาการสำหรับเด็กอายุ 5-6 ปี").style('header1');

    var table = new docx.Table(36,4);
   // table.setFixedWidthLayout();
    table.setWidth(docx.WidthType.PERCENTAGE, "100%");
    let row1 = table.getRow(0);
    row1.getCell(0).addContent(new docx.Paragraph('อายุ').style('th-head')).CellProperties.setWidth(600);
   // row1.getCell(1).addContent(new docx.Paragraph('ความสามารถ').style('th-head')).CellProperties.setWidth(4000);
   // row1.getCell(2).addContent(new docx.Paragraph('ทำได้').style('th-head')).CellProperties.setWidth(800);
   // row1.getCell(3).addContent(new docx.Paragraph('ทำไม่ได้').style('th-head')).CellProperties.setWidth(800);

   // row1.getCell(0).CellProperties.setShading({fill: '92CDDC'});
   // row1.getCell(1).CellProperties.setShading({fill: '92CDDC'});
   // row1.getCell(2).CellProperties.setShading({fill: '92CDDC'});
   // row1.getCell(3).CellProperties.setShading({fill: '92CDDC'});

    //table.getRow(0).addGridSpan(0,3);
    // table.getCell(0,2).CellProperties.setWidth(2000);
    // table.getCell(0,3).CellProperties.setWidth(2000);
    //const image = doc.createImage(fs.readFileSync('tmp/xx.jpg'), 400,500);

     table.getRow(0).getCell(1).CellProperties.addVerticalMerge(docx.VMergeType.CONTINUE);
     table.getRow(1).getCell(1).CellProperties.addVerticalMerge(docx.VMergeType.CONTINUE);
     table.getRow(2).getCell(1).CellProperties.addVerticalMerge(docx.VMergeType.CONTINUE);
  
    const image = docx.Media.addImage(doc,fs.readFileSync('tmp/xx.jpg'))
   table.getRow(2).getCell(1).addContent(image.Paragraph);



    // for(let i=1; i<36; i++){
    //     let row = table.getRow(i); 
    //     row.getCell(0).addContent(new docx.Paragraph(""+i).style('th-head'));
    // }

    doc.addTable(table);

    rutil.download_DOCX(res, doc, 'My Report');


});

module.exports = route;
