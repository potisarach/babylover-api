var officegen = require('officegen');
var express = require('express');
var route = express.Router();
var rutil = require('./report-util');
var path = require('path');
var data = require('./data');
var api = require('../response-data');

// แบบประเมินสภาพความเป็นอยู่
route.get('/evaluationpatron', async (req, res) => {
    let patronid = req.query.patronid;
    let [error, resultpatronevalution, resultmaritalevalution, resultmembers] = await data.GetPatronEvalution(patronid);
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success('Successed')
    }
    let citizenid;
    let fullnamePatron; let nicknamePatron; let birthdatePatron;
    let agePatron; let pic; let gendername;
    let race; let nationality; let religion;
    let education; let schoolname; let homeno;
    let moono; let tumbolname; let amphurname;
    let provincename; let maritalhistory; let patronhistory;
    let couplehistory; let patronhealth; let patronpositionworkplace;
    let patronpersonality; let patronoccupationdetail; let income;
    let expenses; let lifestyle; let familystatus;
    let homedetail_sub1; let homedetail_sub2; let homedetail_sub3;
    let homedetail_sub4; let environment_sub1; let environment_sub2;
    let otherfact_sub1; let otherfact_sub2; let otherfact_sub3;
    let otherfact_sub4; let otherfact_sub5; let otherfact_sub6;
    let otherfact_sub7; let otherfact_sub8; let otherfact_sub9;
    let otherfact_sub10; let notice; let Comments;
    //
    let fullnamemarital; let birthdatemarital;
    let agemarital; let racemarital;
    let nationnalitymarital; let religionmarital;
    let educationmarital; let maritalhealth;
    let maritalpositionworkplace; let maritalpersonality;
    let maritaloccupationdetail; let namestatusmarital;
    //
    let fullnamemembers; let birthdatemembers;
    let agemembers; let educationcareer;
    let relationshipname;


    for (let data of resultpatronevalution) {
        citizenid = data.citizenid,
            fullnamePatron = data.fullnamepatron,
            nicknamePatron = data.nicknamepatron,
            birthdatePatron = data.birthdatepatron,
            agePatron = data.agepatron,
            pic = data.picturepath,
            gendername = data.gendername,
            race = data.race,
            nationality = data.nationality,
            religion = data.religion,
            education = data.education,
            schoolname = data.schoolname,
            homeno = data.homeno,
            moono = data.moono,
            tumbolname = data.tumbolname,
            amphurname = data.amphurname,
            provincename = data.provincename,
            maritalid = data.maritalid,
            patronhistory = data.patronhistory,
            couplehistory = data.couplehistory,
            patronhealth = data.patronhealth,
            patronpositionworkplace = data.patronpositionworkplace,
            patronpersonality = data.patronpersonality,
            patronoccupationdetail = data.patronoccupationdetail,
            income = data.income,
            expenses = data.expenses,
            lifestyle = data.lifestyle,
            familystatus = data.familystatus,
            homedetail_sub1 = data.homedetail_sub1,
            homedetail_sub2 = data.homedetail_sub2,
            homedetail_sub3 = data.homedetail_sub3,
            homedetail_sub4 = data.homedetail_sub4,
            environment_sub1 = data.environment_sub1,
            environment_sub2 = data.environment_sub2,
            otherfact_sub1 = data.otherfact_sub1,
            otherfact_sub2 = data.otherfact_sub2,
            otherfact_sub3 = data.otherfact_sub3,
            otherfact_sub4 = data.otherfact_sub4,
            otherfact_sub5 = data.otherfact_sub5,
            otherfact_sub6 = data.otherfact_sub6,
            otherfact_sub7 = data.otherfact_sub7,
            otherfact_sub8 = data.otherfact_sub8,
            otherfact_sub9 = data.otherfact_sub9,
            otherfact_sub10 = data.otherfact_sub10,
            notice = data.notice,
            Comments = data.Comments
        //

        //  

    }
    for (let data of resultmaritalevalution) {
        fullnamemarital = data.fullnamemarital,
            birthdatemarital = data.birthdatemarital,
            agemarital = data.agemarital,
            racemarital = data.racemarital,
            nationnalitymarital = data.nationnalitymarital,
            religionmarital = data.religionmarital,
            educationmarital = data.educationmarital,
            maritalhealth = data.maritalhealth,
            maritalpositionworkplace = data.maritalpositionworkplace,
            maritalpersonality = data.maritalpersonality,
            maritaloccupationdetail = data.maritaloccupationdetail,
            namestatusmarital = data.namestatusmarital,
            maritalhistory = data.maritalhistory
    }

    for (let data of resultmembers) {

        fullnamemembers = data.fullnamemembers,
            birthdatemembers = data.birthdatemembers,
            agemembers = data.agemembers,
            educationcareer = data.educationcareer,
            relationshipname = data.relationshipname
    }
    let fullbirthdatepatron = birthdatePatron;
    let yearpatron = fullbirthdatepatron.substr(0, 4);
    let monthpatron = fullbirthdatepatron.substr(5, 2);
    let daypatron = fullbirthdatepatron.substr(8, 2);
    let ypatron;
    let mpatron;
    let dpatron;

    if (monthpatron == "01") {
        mpatron = "มกราคม";
    } else if (monthpatron == "02") {
        mpatron = "กุมภาพันธ์";
    } else if (monthpatron == "03") {
        mpatron = "มีนาคม";
    } else if (monthpatron == "04") {
        mpatron = "เมษายน";
    } else if (monthpatron == "05") {
        mpatron = "พฤษภาคม";
    } else if (monthpatron == "06") {
        mpatron = "มิถุนายน";
    } else if (monthpatron == "07") {
        mpatron = "กรกฎาคม";
    } else if (monthpatron == "08") {
        mpatron = "สิงหาคม";
    } else if (monthpatron == "09") {
        mpatron = "กันยายน";
    } else if (monthpatron == "10") {
        mpatron = "ตุลาคม";
    } else if (monthpatron == "11") {
        mpatron = "พฤศจิกายน";
    } else if (monthpatron == "12") {
        mpatron = "ธันวาคม";
    }

    let yearspatron = parseInt(yearpatron) + 543;
    ypatron = yearspatron;

    if (daypatron < "10") {
        dpatron = daypatron.substr(1, 1);
    } else {
        dpatron = daypatron;
    }

    birthdatePatron = dpatron + " " + mpatron + " " + ypatron;

    if (birthdatemarital != null) {
        let fullbirthdatemarital = birthdatemarital;
        let yearmaritals = fullbirthdatemarital.substr(0, 4);
        let monthmaritals = fullbirthdatemarital.substr(5, 2);
        let daymaritals = fullbirthdatemarital.substr(8, 2);
        let ymaritals;
        let mmaritals;
        let dmaritals;

        if (monthmaritals == "01") {
            mmaritals = "มกราคม";
        } else if (monthmaritals == "02") {
            mmaritals = "กุมภาพันธ์";
        } else if (monthmaritals == "03") {
            mmaritals = "มีนาคม";
        } else if (monthmaritals == "04") {
            mmaritals = "เมษายน";
        } else if (monthmaritals == "05") {
            mmaritals = "พฤษภาคม";
        } else if (monthmaritals == "06") {
            mmaritals = "มิถุนายน";
        } else if (monthmaritals == "07") {
            mmaritals = "กรกฎาคม";
        } else if (monthmaritals == "08") {
            mmaritals = "สิงหาคม";
        } else if (monthmaritals == "09") {
            mmaritals = "กันยายน";
        } else if (monthmaritals == "10") {
            mmaritals = "ตุลาคม";
        } else if (monthmaritals == "11") {
            mmaritals = "พฤศจิกายน";
        } else if (monthmaritals == "12") {
            mmaritals = "ธันวาคม";
        }

        let yearsmaritals = parseInt(yearmaritals) + 543;
        ymaritals = yearsmaritals;

        if (daymaritals < "10") {
            dmaritals = daymaritals.substr(1, 1);
        } else {
            dmaritals = daymaritals;
        }

        birthdatemarital = dmaritals + " " + mmaritals + " " + ymaritals;


    }

    if (birthdatemembers != null) {
        // วันที่
        let fullbirthdatemembers = birthdatemembers;
        let yearmembers = fullbirthdatemembers.substr(0, 4);
        let monthmembers = fullbirthdatemembers.substr(5, 2);
        let daymembers = fullbirthdatemembers.substr(8, 2);
        let ymembers;
        let mmembers;
        let dmembers;

        if (monthmembers == "01") {
            mmembers = "มกราคม";
        } else if (monthmembers == "02") {
            mmembers = "กุมภาพันธ์";
        } else if (monthmembers == "03") {
            mmembers = "มีนาคม";
        } else if (monthmembers == "04") {
            mmembers = "เมษายน";
        } else if (monthmembers == "05") {
            mmembers = "พฤษภาคม";
        } else if (monthmembers == "06") {
            mmembers = "มิถุนายน";
        } else if (monthmembers == "07") {
            mmembers = "กรกฎาคม";
        } else if (monthmembers == "08") {
            mmembers = "สิงหาคม";
        } else if (monthmembers == "09") {
            mmembers = "กันยายน";
        } else if (monthmembers == "10") {
            mmembers = "ตุลาคม";
        } else if (monthmembers == "11") {
            mmembers = "พฤศจิกายน";
        } else if (monthmembers == "12") {
            mmembers = "ธันวาคม";
        }

        let yearsmembers = parseInt(yearmembers) + 543;
        ymembers = yearsmembers;

        if (daymembers < "10") {
            dmembers = daymembers.substr(1, 1);
        } else {
            dmembers = daymembers;
        }

        birthdatemembers = dmembers + " " + mmembers + " " + ymembers;

    }

    var docx = officegen({
        type: 'docx',
        orientation: 'portrait',
        pageMargins: { top: 1000, left: 1500, bottom: 1000, right: 1500 }
        // The theme support is NOT working yet...
        // themeXml: themeXml    

    });

    docx.on('error', function (err) {
        console.log(err)
    });
    // function SetStyleFont(){
       var fontface = 'TH SarabunPSK';
       var fontsizemaintopic = '18';
       var fontsizesubheading ='16';
       var fontsizedetail = '14';
    // }
    // SetStyleFont();  
    var pObj = docx.createP({ align: 'center' })
    pObj.addText('มูลนิธิพัฒนาชีวิตชนบท', { font_face: fontface, font_size: fontsizemaintopic, bold: true });
    var pObj = docx.createP({ backline: '' });
    pObj.addText('1. ข้อเท็จจริงเกี่ยวกับตัวผู้อุปการะเด็ก', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข้อมูลส่วนตัว', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if(patronhealth == null || patronhealth == undefined){
        patronhealth = '-'
    }
    var table = [
        [{
            val: "ชื่อ-นามสกุล : " + fullnamePatron + ' ' + "(" + nicknamePatron + ")",
            opts: {
                cellColWidth: 5000,
                fontFamily: fontface,
                sz: '28'
            }
        }, {
            val: "เพศ : " + gendername,
            opts: {
                // align: "right",
                fontFamily: fontface,
                sz: '28'
            }
        }],
        [{
            val: 'วัน/เดือน/ปี เกิด : ' + birthdatePatron,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }, {
            val: 'อายุ : ' + agePatron + ' ปี',
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }],
        [{
            val: 'สัญชาติ : ' + nationality,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }, {
            val: 'เชื้อชาติ : ' + race,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }],
        [{
            val: 'ศาสนา : ' + religion,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }, {
            val: 'ระดับการศึกษา : ' + education,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }],
        [{
            val: 'ชื่อสถานศึกษา : ' + schoolname,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }, {
            val: 'สุขภาพ : ' + patronhealth,
            opts: {
                fontFamily: fontface,
                sz: '28'
            }
        }]
    ]
    var tableStyle = {
        tableColWidth: 4261,
        // tableSize: 20,
        // tableColor: "ada",
        tableAlign: "left",
        borders: false
    }
    docx.createTable(table, tableStyle);

    if (patronoccupationdetail != "" && patronoccupationdetail != undefined) {
        var pObj = docx.createP()
        pObj.addText('อาชีพและรายได้ : ' + patronoccupationdetail, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP()
        pObj.addText('อาชีพและรายได้ : -', { font_face: fontface, font_size: fontsizedetail });
    }
    if (patronpositionworkplace != "" && patronpositionworkplace != undefined) {
        var pObj = docx.createP()
        pObj.addText('ตำแหน่งและสถานที่ทำงานประจำ : ' + patronpositionworkplace, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP()
        pObj.addText('ตำแหน่งและสถานที่ทำงานประจำ : -', { font_face: fontface, font_size: fontsizedetail });
    }
    if (patronpersonality != "" && patronpersonality != undefined) {
        var pObj = docx.createP()
        pObj.addText('ลักษณะท่าทางและการแต่งกาย : ' + patronpersonality, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP()
        pObj.addText('ลักษณะท่าทางและการแต่งกาย : -', { font_face: fontface, font_size: fontsizedetail });
    }
  

    //address
    // var pObj = docx.createP({align: 'left'})
    // pObj.addText('ที่อยู่', {font_face: 'TH SarabunPSK',font_size: '18',bold: true});
    var pObj = docx.createP()
    pObj.addText('ที่อยู่ : ' + 'บ้านเลขที่ ' + homeno + ' หมูที่ ' + moono + ' ซอย '
        + ' ถนน ' + ' ตำบล' + tumbolname + ' อำเภอ' + amphurname
        + ' จังหวัด' + provincename, { font_face: fontface, font_size: fontsizedetail });


    //marital
    var pObj = docx.createP({ backline: '' });
    pObj.addText('2. ข้อเท็จจริงเกี่ยวกับสามี/ภรรยาของผู้ขออุปการะเด็ก', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    if (fullnamemarital != "" && fullnamemarital != undefined) {
        var table = [
            [{
                val: "ชื่อ-นามสกุล : " + fullnamemarital,
                opts: {
                    cellColWidth: 5000,
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: "วัน/เดือน/ปี เกิด : " + birthdatemarital,
                opts: {
                    // align: "right",
                    fontFamily: fontface,
                    sz: '28'
                }
            }],
            [{
                val: 'อายุ : ' + agemarital,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: 'สัญชาติ : ' + nationnalitymarital,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }],
            [{
                val: 'เชื้อชาติ : ' + racemarital,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: 'ศาสนา : ' + religionmarital,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }],
            [{
                val: 'ระดับการศึกษา : ' + educationmarital,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: 'สุขภาพ : ' + maritalhealth,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }]
        ]
        var tableStyle = {
            tableColWidth: 4261,
            // tableSize: 20,
            // tableColor: "ada",
            tableAlign: "left",
            borders: false
        }
        docx.createTable(table, tableStyle);
        //   docx.putPageBreak ();
        var pObj = docx.createP()
        pObj.addText('อาชีพและรายได้ : ' + maritaloccupationdetail, { font_face: fontface, font_size: fontsizedetail });
        var pObj = docx.createP()
        pObj.addText('ตำแหน่งและสถานที่ทำงานประจำ : ' + maritalpositionworkplace, { font_face: fontface, font_size: fontsizedetail });
        var pObj = docx.createP()
        pObj.addText('ลักษณะท่าทางและการแต่งกาย : ' + maritalpersonality, { font_face: fontface, font_size: fontsizedetail });
        var pObj = docx.createP()
        pObj.addText('สถานภาพปัจจุบัน : ' + namestatusmarital, { font_face: fontface, font_size: fontsizedetail });
    } else {
        var pObj = docx.createP()
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    //member
    if (fullnamemembers != "" && fullnamemembers == "") {
        var pObj = docx.createP({ backline: '' });
        pObj.addText('3. สมาชิกในครอบครัวเดียวกัน (ไม่รวมผู้ขออุปการะเด็ก)', { font_face: fontface, font_size: fontsizesubheading, bold: true });
        var table = [
            [{
                val: "ชื่อ-นามสกุล : " + fullnamemembers,
                opts: {
                    cellColWidth: 5000,
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: "วัน/เดือน/ปี เกิด : " + birthdatemembers,
                opts: {
                    // align: "right",
                    fontFamily: fontface,
                    sz: '28'
                }
            }],
            [{
                val: 'อายุ : ' + agemembers,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }, {
                val: 'การศึกษาหรืออาชีพ : ' + educationcareer,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }],
            [{
                val: 'ความเกี่ยวข้อง : ' + relationshipname,
                opts: {
                    fontFamily: fontface,
                    sz: '28'
                }
            }]
        ]
        var tableStyle = {
            tableColWidth: 4261,
            // tableSize: 20,
            // tableColor: "ada",
            tableAlign: "left",
            borders: false
        }
        docx.createTable(table, tableStyle);
    } else {
        var pObj = docx.createP({ backline: '' });
        pObj.addText('3. สมาชิกในครอบครัวเดียวกัน (ไม่รวมผู้ขออุปการะเด็ก)', { font_face: 'TH SarabunPSK', font_size: fontsizesubheading, bold: true });
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    //history
    var pObj = docx.createP({ backline: '' });
    pObj.addText('4. ประวัติความเป็นมา', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ก.ประวัติส่วนตัวของผู้อุปการะเด็ก', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (patronhistory != null && patronhistory != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + patronhistory, { font_face: fontface, font_size: fontsizedetail });
    } else {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข.ประวัติส่วนตัวของสามีหรือภรรยา', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (couplehistory != null && couplehistory != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + couplehistory, { font_face: fontface, font_size: fontsizedetail });
    } else {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ค.ประวัติการสมรสของผู้ขออุปการะเด็ก', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (maritalhistory != null && maritalhistory != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + maritalhistory, { font_face: fontface, font_size: fontsizedetail });
    } else {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }


    //   docx.putPageBreak ();
    //5.
    var pObj = docx.createP({ backline: '' });
    pObj.addText('5. การครองชีพและสภาพความเป็นอยู่ทางครอบครัว', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ก.การครองชีพ', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (income != null && income != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'รายได้ : ' + income, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'รายได้ : -', { font_face: fontface, font_size: fontsizedetail });
    }
    if (expenses != null && expenses != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'รายจ่าย : ' + expenses, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'รายจ่าย : -', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข.สภาพความเป็นอยู่ในปัจจุบันและมาตรฐานการครองชีพ', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (lifestyle != null && lifestyle != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'การดำเนินชีวิต : ' + lifestyle, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'การดำเนินชีวิต : - ', { font_face: fontface, font_size: fontsizedetail });
    }
    if (familystatus != null && familystatus != undefined) {
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'ฐานะทางครอบครัว : ' + familystatus, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + 'ฐานะทางครอบครัว : - ', { font_face: fontface, font_size: fontsizedetail });

    }

    //6
    var pObj = docx.createP({ backline: '' });
    pObj.addText('6. ที่อยู่และเครื่องอุปกรณ์การดำรงชีพ', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ก.ลักษณะและขนาดของที่อยู่', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (homedetail_sub1 != null && homedetail_sub1 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + homedetail_sub1, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข.สภาพของที่อยู่', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (homedetail_sub2 != null && homedetail_sub2 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + homedetail_sub2, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ค.สถานที่ซึ่งจะจัดให้เด็กอยู่', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (homedetail_sub3 != null && homedetail_sub3 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + homedetail_sub3, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ง.เครื่องอุปกรณ์การเลี้ยงเด็กและส่งเสริมการศึกษา', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (homedetail_sub4 != null && homedetail_sub4 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + homedetail_sub4, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }


    //   docx.putPageBreak ();
    //7
    var pObj = docx.createP({ backline: '' });
    pObj.addText('7. สิ่งแวดล้อม', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ก.บริเวณบ้าน', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (environment_sub1 != null && environment_sub1 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + environment_sub1, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข.ละเเวกบ้าน', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (environment_sub2 != null && environment_sub2 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + environment_sub2, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    //8
    var pObj = docx.createP({ backline: '' });
    pObj.addText('8. ข้อเท็จจริงอื่น ๆ', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ก.เวลาที่จะอยู่กับเด็ก', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub1 != null && otherfact_sub1 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub1, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ข.สาเหตุที่ขออุปการะเด็ก', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub2 != null && otherfact_sub2 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub2, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ค.ความเห็นชอบของผู้อยู่ร่วมครอบครัว', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub3 != null && otherfact_sub3 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub3, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ง.โครงการหรือความคิดเห็นที่จะเลี้ยงดูส่งเสริมเด็กต่อไป', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub4 != null && otherfact_sub4 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub4, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('จ.เคยอุปการะเด็กซึ่งมิใช่บุตรหลานมาบ้างหรือไม่', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub5 != null && otherfact_sub5 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub5, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }


    //   docx.putPageBreak ();

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ฉ.ยินยอมให้บิดามารดาหรือญาติของเด็กมาเยี่ยมบ้างหรือไม่ เพียงใด', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub6 != null && otherfact_sub6 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub6, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ช.ภาษาที่ใช้พูดในครอบครัว', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub7 != null && otherfact_sub7 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub7, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ซ.ความเห็นชอบตามข้อตกลงในการรับเด็กไปอุปการะ', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub8 != null && otherfact_sub8 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub8, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ฌ.เพื่อนบ้าน', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub9 != null && otherfact_sub9 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub9, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    var pObj = docx.createP({ align: 'left' })
    pObj.addText('ญ.เด็กที่ประสงค์จะรับไปอุปการะ', { font_face: fontface, font_size: fontsizedetail, bold: true });
    if (otherfact_sub10 != null && otherfact_sub10 != undefined) {  
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + otherfact_sub10, { font_face: fontface ,  font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }


    //9
    var pObj = docx.createP({ backline: '' });
    pObj.addText('9. ข้อสังเกต', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    if (notice != null && notice != undefined) {  
        var pObj = docx.createP({ align: 'left' })      
        pObj.addText('     ' + notice, { font_face: fontface, font_size: fontsizedetail });

    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }
    //10
    var pObj = docx.createP({ backline: '' });
    pObj.addText('10. ความเห็น', { font_face: fontface, font_size: fontsizesubheading, bold: true });
    if (Comments != null && Comments != undefined) {        
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('     ' + Comments, { font_face: fontface, font_size: fontsizedetail });
    }else{
        var pObj = docx.createP({ align: 'left' })
        pObj.addText('-', { font_face: fontface, font_size: fontsizedetail });
    }

    var toDay = new Date().toISOString().slice(0, 10);
    res.setHeader('Content-Disposition', 'attachment; filename=evaluation_' + citizenid + '_' + toDay + '.docx');
    docx.generate(res);
});


module.exports = route;
