var connection = require('../../dbconnection');
var fs = require('fs');
var uuid = require('uuidv4');
var _conf = require('../../config');

let putUser = async (data) => {

    let pathimg;
    if (data.picturepath) {
        let base64toImage = data.picturepath.split(';base64,').pop();
        picture_name = uuid() + ".jpg";
        pathimg = picture_name;
        fs.writeFile(_conf.getUploadPathImgPerson() + "/" + picture_name, base64toImage, 'base64', function (err) {
            console.log("Error Upload : "+err);
            if(!err){          
            }
        });
    }
    console.log('pictureName>>>>>', pathimg)
    
    let values = {
        // "userid": data.userid, //ห้ามเปิด
        "firstname": data.firstname,
        "lastname": data.lastname,
        "username": data.username,
        "password": data.password,
        "email": data.email,
        "phone": data.phone,
        "userstatusid": data.userstatusid,
        "picturepath": pathimg
    };
    let valuesupdatenotpic = {
        "userid": data.userid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "username": data.email,
        "password": data.password,
        "email": data.email,
        "phone": data.phone,
        "userstatusid": data.userstatusid,
    };
    let valuesupdate = {
        "userid": data.userid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "username": data.email,
        "password": data.password,
        "email": data.email,
        "phone": data.phone,
        "userstatusid": data.userstatusid,
        "picturepath": pathimg
    };
    let sql;
    console.log("Put User");
    console.log("data.userid  > > >",data.userid);
    // console.log("data.picturepath  > > >",data.picturepath);
    
    
    if (data.userid && data.picturepath == '') {
        console.log("Update & NoPic");
        sql = 'UPDATE user SET ? WHERE userid=?';
        let user = await connection.query(sql, [valuesupdatenotpic, data.userid]);
        console.log("user> > >",user);
        
        return user.affectedRows;
    } else if (data.userid && data.picturepath != '') {
        sql = 'UPDATE user SET ? WHERE userid=?';
        let user = await connection.query(sql, [valuesupdate, data.userid]);
        return user.affectedRows;
    } else {
        console.log("Insert");
        sql = 'INSERT INTO user SET ?';
        let user = await connection.query(sql, [values]);
        let userId = user.insertId;
        return userId;
    }
    // let sql = 'UPDATE user SET ? WHERE username = ?'
    // value = {
    //     "firstname": data.firstname,
    //     "lastname": data.lastname,
    //     "email": data.email,
    //     "phone": data.phone
    // }
    // let result = await connection.query(sql, [value, data.username]);
    // let userid = result.insertId;
    // console.log("User ID > > >", userid);
}

var data = {
    update: (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let error;
            let result;
            let userId = data.userid
            let path;
            try {
                if (userId) {
                    console.log("Have UserId");
                    let effects = await putUser(data,path);
                    result = { success: true };
                } else {
                    console.log("Not Have UserId");
                    let userId = await putUser(data,path);
                    result = {
                        "userId": userId,
                    }
                }
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                    } else {
                        console.log('COMMIT');
                        callback({ error: error, data: result })
                    }
                })

            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') });

            }
            callback({ error: error, data: result })
        })

    },
    // list: async (userid) => {
    //     let error
    //     let result
    //     try {
    //         let sql = `
    //             SELECT  
    //                 user.userid,
    //                 fn_fullnameuser(firstname, lastname) AS fullname,
    //                 user.username,
    //                 user.password,
    //                 user.email,
    //                 user.phone,
    //                 user.picturePath
    //             FROM user
    //             ORDER BY 
    //                 user.userid ASC`;

    //         result = await connection.query(sql, [userid, userid]);

    //     } catch (err) {
    //         error = err;
    //     }
    //     return [error, result];
    // },
    UpdatePassword: async (username, password) => {
        let error;
        let results;
        try {
            let sql = 'UPDATE user SET ? WHERE username = ?'
            let value = {
                "password": password
            }
            let results = await connection.query(sql, [value, username]);
        } catch (err) {
            error = err;
        }
        return [error, results]
    },

    Get: async (userid,serverhost) => {
        let error;
        let results;
        try {
            let sql = `
            SELECT  
            user.userid,
			user.firstname, 
            user.lastname,
            fn_fullnameuser(firstname, lastname) AS fullname,
            user.username,
            user.password,
            user.email,
            user.phone,
            CONCAT('${serverhost}' , user.picturePath) AS picturepath,
            user_status.userstatusname,
            user_status.userstatusid  
            FROM user
				JOIN user_status ON user.userstatusid = user_status.userstatusid  
            WHERE 
                user.userid = ? OR ? = 0
            ORDER BY 
                user.userid ASC
            `;
        results = await connection.query(sql, [userid, userid]);

        } catch (err) {
            error = err;
        }
        return [error, results]
    },

    GetUser: async (username,serverhost) => {
        let error;
        let results;
        try {
            let sql = `
            SELECT 
            user.firstname,
            user.lastname,
            fn_fullnameuser(firstname, lastname) AS fullname,
            user.username,
            user.password,
            user.email,  
            user.phone,
            CONCAT('${serverhost}' , user.picturePath) AS picturepath,
            user_status.userstatusid,
            user_status.userstatusname
             FROM
            user 
             JOIN user_status ON user.userstatusid = user_status.userstatusid
         WHERE
            username = ?
        `;
            results = await connection.query(sql, username);

        } catch (err) {
            error = err;
        }
        return [error, results]
    },

    GetPassword: async (username) => {
        let error;
        let results;
        try {
            let sql = 'SELECT password FROM user WHERE username = ?';
            results = await connection.query(sql, username)

        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    search: async (fullname) => {
        let error
        let result
        fullname = '%' + fullname + '%';
        try {
            let sql = `
         SELECT  
            user.userid,
            fn_fullnameuser(firstname, lastname) AS fullname,
            user.username,
            user.password,
            user.email,
            user.phone,
            user.picturePath,
            user_status.userstatusname AS status,
            user_status.userstatusid
        FROM user
				JOIN user_status ON user.userstatusid = user_status.userstatusid  
        WHERE 
            (fn_fullnameuser(firstname, lastname) LIKE ? OR (? = "")) 
        ORDER BY 
            user.userstatusid, firstname ASC
                
            `;
            result = await connection.query(sql, [fullname, fullname]);
        } catch (err) {
            error = err;
        }
        return [error, result];

    },
    GetStatus: async () => {
        let error;
        let result;
         try {
             let sql = 'SELECT * FROM user_status'
            result = await connection.query(sql)
            } catch (err) {
                error = err;
         }
         
         return [error ,result]
    },
    DeleteUser : async (userid) => {
        let error;
        let results;
        try {
            sql = `
            DELETE 
            FROM user 
            WHERE userid = ?
            `
            results = await connection.query(sql, [userid]);
        } catch (err) {
            error = err;
        }
        return [error, results]

    }
} //ปิด var data  

module.exports = data;
