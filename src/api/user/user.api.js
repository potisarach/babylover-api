var express = require('express');
var route = express.Router();
var data = require('./user.data');
var api = require('../../response-data');
var nodeMailer = require('nodemailer');
var _conf = require('../../config');


route.post('/search', async (req, res) => {
    let fullname = req.body.fullname;
    console.log("Fullname >  > >",fullname);
    
    let [error, results] = await data.search(fullname);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Search User');
    }
    api.sending(req, res, response);
});

// route.post('/user/list', async (req, res) => { 
//     let [error, results] = await data.list(0);
//     let response;
//     if (error) {
//         response = api.error(error);
//     } else {
//         response = api.success(results, 'Successed, Get User');
//     }
//     api.sending(req, res, response);
// });

route.post('/update', async (req, res) => {
    // let username = req.body.username;
    let username = req.body.email;

    let response;
    if (!username) {
        response = api.error('Not Found');
        api.sending(req, res, repoonse)
    }
    data.update(req.body, (results) => {
        if (results.error) {
            response = api.error(results.error);
        } else {
            response = api.success(results, 'Success');
        }
        api.sending(req, res, response)
    });
});

route.post('/updateUser', async (req,res) => { //function นี้ใช้งาน
    let userid = req.body.userid;
    let response;
    if(!userid){
        response = api.error('userid Not found');
        api.sending(req, res, response);
    }
    data.update(req.body, (results) => {
        if(results.error){
            response = api.error(results.error);
        }else{
            response = api.success(results, 'Success');
        }
        api.sending(req, res, response);
    });
});

route.post('/get', async (req, res) => {
    let serverhost = _conf.getImagePersonUrl()+'/';
    let username = req.body.username;
    let [error, results] = await data.GetUser(username, serverhost);
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(results[0], 'Successed');  
    }
    api.sending(req, res, response);
});

route.post('/getEdit', async (req, res) => {
    let serverhost = _conf.getImagePersonUrl()+'/';
    let userid = req.body.userid;
    console.log("id >>>>>", userid);  
    let [error, results] = await data.Get(userid,serverhost);
    let response;
    if (error) {
        response = api.error(error);
        console.log("ผิดที่นี่");      
    } else {
        // response = api.success(results[0], 'Successed');
        response = api.success(results[0], 'Successed');
        console.log("ถูกแล้ว");
    }
    api.sending(req, res, response);
});

route.post('/gatpassword', async (req, res) => {
    let username = req.body.username;
    let [error, results] = await data.GetPassword(username);
    let response;
    if (error) {
        respoonse = api.error(error);
    } else {
        response = api.success(results[0], 'Successed');
    }
    api.sending(req, res, response);
});

route.post('/updatepassword', async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let [error, results] = await data.UpdatePassword(username, password);
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(results, 'Successed')
    }
    api.sending(req, res, response);
});
route.post('/sendemail', async (req, res) => {
    // console.log(req.body.username);
    console.log(req.body.password);

    let transporter = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'babyloverteam@gmail.com',
            pass: 'Aw7n5SyW'
        }
    });
    let mailOptions = {
        from: '"ADMIN BABY LOVER" <xx@gmail.com>', // sender address
        to: req.body.to, // list of receivers
        subject: req.body.subject, // Subject line
        // html: 'Username : ' + req.body.username + '<br>' + 'Password : ' + req.body.password
        html: 'Password : ' + req.body.password

    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.render('index');
    });

});
route.post('/getstatus', async (req, res) => {
    let [error, result] = await data.GetStatus()
    let response;
    if (error) {
        repoonse = api.error(error);
    } else {
        response = api.success(result, 'Successed')
    }
    console.log("Result > > >",result);
    
    api.sending(req, res, response);
});
route.post('/delete', async(req, res) => {
    console.log("rep.body", req.body.userid);
    let userid = req.body.userid

    let error, results = await data.DeleteUser(userid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }

    api.sending(req, res, response);

})



module.exports = route;  