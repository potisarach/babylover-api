var express = require('express');
var route = express.Router();
var api = require('../../response-data');
var data = require('./evaluation.data');
var _conf = require('../../config');

route.post('/get', async (req, res) => { //function นี้ใช้งาน
    let patronid = req.body.patronid;
    let serverhost = _conf.getImagePersonUrl()+'/';
    let response;
    if (!patronid) {
        response = api.error('patronid Notfound.');
        api.sending(req, res, response);
        return;
    }
    let [error, resultpatron,resultaddress,resultmarital,resultmembers,resultsurvey] = await data.get(patronid, serverhost);
    if(error){
        response = api.error(error);
    }else{
        let result = resultpatron[0];
        if(resultaddress.length == 0){
            result.address = {
                "Personid": null,
                "addressid": null,
                "amphurcode": null,
                "fulladdress": null,
                "homeno": null,
                "moono": null,
                "name": null,
                "patronid": null,
                "provincecode": null,
                "road": null,
                "soi": null,
                "tumbolcode": null,
                "zipcode": null
            } ;
        }else{
            result.address = resultaddress[0];
        }
        if(resultmarital.length == 0){
            result.marital = {
            "patronid": null,
            "personid": null,
            "maritalid": null,
            "citizenid": null,
            "firstname": null,
            "lastname": null,
            "nickname": null,
            "genderid": null,
            "prefixid": null,
            "birthdate": null,
            "occupcode": null,
            "racecode": null,
            "nationcode": null,
            "religioncode": null,
            "educationcode": null,
            "schoolname": null
            };
        }else{
            result.marital = resultmarital[0];
        }
        result.members = resultmembers;
        result.survey = resultsurvey;
        response = api.success(result, 'Successed Get Orphan.');
    }
    console.log(req.protocol +"://"+ req.get('host') + '/images/person/');
    api.sending(req, res, response);
});
route.post('/patron/add', (req, res)=>{
    data.addPatron(req.body,(result)=>{
        let response; 
        if(result.error){
            response = api.error(result.error);
        }else{
            response = api.success(result.data, 'Successed, Added Patron');
        }
       api.sending(req, res, response);
    });
});
route.post('/patron/evaluation', (req, res)=>{ //function นี้ใช้งาน
    data.addEvaluation(req.body,(result)=>{
        let response; 
        if(result.error){
            response = api.error(error);
        }else{
            response = api.success(result.data, 'Successed, Added Patron');
        }
       api.sending(req, res, response);
    });
});
route.post('/patron/list', async (req, res)=>{
    
    let [error, results] = await data.get(0);
    console.log("results => ",results);
    let response; 
    if(error){
        response = api.error(error);
    }else{
        response = api.success(results, 'Successed, Get Patron'); 
    }
    api.sending(req, res, response);
});

route.post('/patron/get', async (req, res)=>{
    let patronid = req.body.patronid;
    let response; 
    if(!patronid){
        response = api.error('patronid Notfound.');
        api.sending(req, res, response);
        return; 
    }
    let [error, results] = await data.get(patronid, serverhost);
    
    if(error){
        response = api.error(error);
    }else{
        response = api.success(results[0], 'Successed, Get Patron');
    }
    api.sending(req, res, response);
});

route.post('/patron/update', async (req,res)=>{
    let personid = req.body.personid;
    console.log(req.body)
    console.log("personid ++++++++++++++++++++++++++++++",req.body.personid)
    let response; 
    if(!personid){
        response = api.error('personid Not found.');
        api.sending(req, res, response);
        return; 
    }
    let [error, results] = await data.update(req.body,personid);
    
    if(error){
        response = api.error(error);
    }else{
        response = api.success(results, 'Successed, update person');
    }
    api.sending(req, res, response);
});


module.exports = route;