var connection = require('../../dbconnection');

let insertPerson = async (data) => {
    let sql = 'INSERT INTO person SET ?';
    let values = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "occupcode": data.occupcode,
        "racecode": data.racecode,
        "nationcode": data.nationcode,
        "religioncode": data.religioncode,
        "educationcode": data.educationcode,
        "schoolname": data.schoolname
    };
    let person = await connection.query(sql, values);
    let personId = person.insertId;

    return personId;
}
let insertAddress = async (data, personId) => {
    let sql = 'INSERT INTO address SET ?';
    let values = {
        "personid": personId,
        "homeno": data.homeno,
        "moono": data.moono,
        "road": data.road,
        "soi": data.soi,
        "tumbolcode": data.tumbolcode,
        "amphurcode": data.amphurcode,
        "provincecode": data.provincecode,
        "zipcode": data.zipcode
    } 
    let address = await connection.query(sql, values)
    let addressId = address.insertId;

    return addressId;
}
let insertPatron = async (data, personId) => {
    let sql = 'INSERT INTO patron SET ?';
    let values = {
        "personid": personId,
        "maritalid": data.maritalid
    }
    let patron = await connection.query(sql, values)
    let patronId = patron.insertId

    return patronId;
}
let insertPatronevaluation = async (data) => {

    if (!data.patronevaluationid) {
        sql = `INSERT INTO patronevaluation SET ?`;
        values = {
            "patronid": data.patronid,
            "patronhistory": data.patronhistory,
            "couplehistory": data.couplehistory,
            "patronhealth": data.health,
            "maritalhealth": data.marital.health,
            "patronpositionworkplace": data.positionworkplace,
            "maritalpositionworkplace": data.marital.positionworkplace,
            "patronpersonality": data.personality,
            "maritalpersonality": data.marital.personality,
            "patronoccupationdetail": data.occupationdetail,
            "maritaloccupationdetail": data.marital.occupationdetail,
            "maritalhistory": data.maritalhistory,
            "income": data.income,
            "expenses": data.expenses,
            "lifestyle": data.lifestyle,
            "familystatus": data.familystatus,
            "homedetail_sub1": data.homedetail_sub1,
            "homedetail_sub2": data.homedetail_sub2,
            "homedetail_sub3": data.homedetail_sub3,
            "homedetail_sub4": data.homedetail_sub4,
            "environment_sub1": data.environment_sub1,
            "environment_sub2": data.environment_sub2,
            "otherfact_sub1": data.otherfact_sub1,
            "otherfact_sub2": data.otherfact_sub2,
            "otherfact_sub3": data.otherfact_sub3,
            "otherfact_sub4": data.otherfact_sub4,
            "otherfact_sub5": data.otherfact_sub5,
            "otherfact_sub6": data.otherfact_sub6,
            "otherfact_sub7": data.otherfact_sub7,
            "otherfact_sub8": data.otherfact_sub8,
            "otherfact_sub9": data.otherfact_sub9,
            "otherfact_sub10": data.otherfact_sub10,
            "notice": data.notice,
            "comment": data.comment,
        }
        let patronEvaluation = await connection.query(sql, values);
        let patronEvaluationId = patronEvaluation.insertId;

        return patronEvaluationId;
    } else {
        sql = `UPDATE patronevaluation SET ? WHERE patronevaluationid= ? `;
        values = {
            "patronhistory": data.patronhistory,
            "couplehistory": data.couplehistory,
            "patronhealth": data.health,
            "maritalhealth": data.marital.health,
            "patronpositionworkplace": data.positionworkplace,
            "maritalpositionworkplace": data.marital.positionworkplace,
            "patronpersonality": data.personality,
            "maritalpersonality": data.marital.personality,
            "patronoccupationdetail": data.occupationdetail,
            "maritaloccupationdetail": data.marital.occupationdetail,
            "maritalhistory": data.maritalhistory,
            "income": data.income,
            "expenses": data.expenses,
            "lifestyle": data.lifestyle,
            "familystatus": data.familystatus,
            "homedetail_sub1": data.homedetail_sub1,
            "homedetail_sub2": data.homedetail_sub2,
            "homedetail_sub3": data.homedetail_sub3,
            "homedetail_sub4": data.homedetail_sub4,
            "environment_sub1": data.environment_sub1,
            "environment_sub2": data.environment_sub2,
            "otherfact_sub1": data.otherfact_sub1,
            "otherfact_sub2": data.otherfact_sub2,
            "otherfact_sub3": data.otherfact_sub3,
            "otherfact_sub4": data.otherfact_sub4,
            "otherfact_sub5": data.otherfact_sub5,
            "otherfact_sub6": data.otherfact_sub6,
            "otherfact_sub7": data.otherfact_sub7,
            "otherfact_sub8": data.otherfact_sub8,
            "otherfact_sub9": data.otherfact_sub9,
            "otherfact_sub10": data.otherfact_sub10,
            "notice": data.notice,
            "comment": data.comment,
        }
        let patronEvaluation = await connection.query(sql, [values, data.patronevaluationid]);
        return data.patronevaluationid;
    }


}
let insertMarital = async (data, personid) => {
    let sql = "INSERT INTO marital SET ?"

    let values = {
        "personid": personid,
        "maritalstatusid": data.marital.maritalstatusid
    }

    let maritalId = await connection.query(sql, values)

    return maritalId.insertId
}
let insertFamilyMember = async (data, patronevaluationid) => {

    let sqlmember = `
        SELECT 
            patron.patronid,
            person.personid,
			familymember.familymemberid,	
            peva.patronevaluationid,
            person.prefixid,
			person.firstname,	
			person.lastname,
			person.birthdate,								
			familymember.educationcareer,			
            familymember.note,
            familymember.relationshipid
        FROM patron           
            INNER JOIN patronevaluation peva
                ON patron.patronid = peva.patronid
			INNER JOIN familymember
				ON peva.patronevaluationid = familymember.patronevaluationid
			INNER JOIN person
				ON familymember.personid = person.personid
        WHERE 
            (patron.patronid = ? OR (? = 0))
        `;

            resultmember = await connection.query(sqlmember, [data.patronid, data.patronid]);
            let numRows = resultmember.length;

    let members = data.members;
    for (let newitem of members) {
        if(newitem.personid){
            for (let item of resultmember) {
                if(newitem.personid == item.personid){
                    let personvalues = {
                        "firstname": newitem.firstname,
                        "lastname": newitem.lastname,
                        "prefixid": newitem.prefixid,
                        "birthdate": newitem.birthdate
                    }
                    let membervalues = {
                        "personid": newitem.personid,
                        "patronevaluationid": newitem.patronevaluationid,
                        "relationshipid": newitem.relationshipid,
                        "educationcareer": newitem.educationcareer,
                        "note": newitem.note
                    }
                    
                let sqlupdateperson = 'UPDATE person SET ? WHERE personid = ?';
                let member = await connection.query(sqlupdateperson, [personvalues, newitem.personid]);

                let sqlupdatefamily = `UPDATE FamilyMember SET ? WHERE familymemberid = ?`;
                let familymember = await connection.query(sqlupdatefamily, [membervalues, newitem.familymemberid]);   
                }
            }
        }else{

            let personvaluesinsert = {
                "firstname": newitem.firstname,
                "lastname": newitem.lastname,
                "prefixid": newitem.prefixid,
                "birthdate": newitem.birthdate
            }

            let sqlperson = `INSERT INTO Person SET ?`;
             let member = await connection.query(sqlperson, personvaluesinsert);
             let memberId = member.insertId;
 
             let valuesmemberInsert = {
                 "patronevaluationid": patronevaluationid,
                 "personid": memberId,
                 "relationshipid": newitem.relationshipid,
                 "educationcareer": newitem.educationcareer,
                 "note": newitem.note
             };
             // INSERT FamilyMember
             let sqlfamily = `INSERT INTO FamilyMember SET ?`;
             let familymember = await connection.query(sqlfamily, valuesmemberInsert);
        }
    }
    for (let item of resultmember) {
        let isHave = false;
        for (let newitem of members) {
            if(newitem.personid == item.personid){
                isHave = true;
            }
        }
        if(!isHave){
            //TODO DELETE
            // member, person
            let sqldelmember = 'DELETE FROM familymember WHERE personid = ?';
            let delmember = await connection.query(sqldelmember, item.personid);

            let sqldelperson = 'DELETE FROM person WHERE personid = ?';
            let delperson = await connection.query(sqldelperson, item.personid);
        }
    }
}
let updatePerson = async (data) => {
    let sqlPerson = "UPDATE person SET ? WHERE personid=?";
    let valuesPerson = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "racecode": data.racecode,
        "nationcode": data.nationcode,
        "religioncode": data.religioncode,
        "educationcode": data.educationcode,
        "schoolname": data.schoolname
    }
    person = await connection.query(sqlPerson, [valuesPerson, data.personid]);

    return person;
}
let updateAddress = async (data,personid) => {
    let valuesAddress = {
        "homeno": data.homeno,
        "moono": data.moono,
        "road": data.road,
        "soi": data.soi,
        "tumbolcode": data.tumbolcode,
        "amphurcode": data.amphurcode,
        "provincecode": data.provincecode,
        "zipcode": data.zipcode
    }
    let sqlAddress = "UPDATE address SET ? WHERE personid = ?"
    address = await connection.query(sqlAddress, [valuesAddress, personid]);
}
let updatePatron = async (data, maritalInsertid) => {
    let sql = "UPDATE patron SET ? WHERE patronid = ?"
    let values = {
        "maritalid": maritalInsertid
    }
    let updatepatron = connection.query(sql, [values, data.patronid]);
    return updatepatron;
}
let putSurvey = async (data) => {

    let sqlsurvey = `
    SELECT
        patronsurveyid,
        patronid,
        surveydate,
        createdate
    FROM patronsurvey
    WHERE 
        (patronid = ? OR (? = 0))
    `;
    resultsurvey = await connection.query(sqlsurvey, [data.patronid, data.patronid]);
    let num = resultsurvey.length;
    
    let survey = data.survey
    for (let newitem of survey) {
        if (newitem.patronsurveyid) {
            for (let item of resultsurvey) {
                if (newitem.patronsurveyid == item.patronsurveyid) {
                    let updatesurveyvalues = {
                        "patronid": data.patronid,
                        "surveydate": newitem.surveydate
                    }

                    let sqlupdatesurvey = 'UPDATE patronsurvey SET ? WHERE patronsurveyid = ?';
                    updatesurvey = await connection.query(sqlupdatesurvey, [updatesurveyvalues, newitem.patronsurveyid]);
                }
            }
        }
        else {
            let insertsurveyvalues = {
                "patronid": data.patronid,
                "surveydate": newitem.surveydate
            }

            let sqlinsertsurvey = 'INSERT INTO patronsurvey SET ?';
            insertsurvey = await connection.query(sqlinsertsurvey, insertsurveyvalues)
        }
    }
    for (let item of resultsurvey) {
        let isHere = false;
        for (let newitem of survey) {
            if (item.patronsurveyid == newitem.patronsurveyid) {
                isHere = true;
            }
        }
        if (!isHere) {
            let sqldelsurvey = 'DELETE FROM patronsurvey WHERE patronsurveyid = ?';
            delsurvey = await connection.query(sqldelsurvey, item.patronsurveyid);
        }
    }
}

var data = {
    addPatron: (data, callback) => {
        connection.beginTransation(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result;

            try {
                let personId = await insertPerson(data);
                let addressId = await insertAddress(data, personId)
                let patronId = await insertPatron(data, personId);

                let result = {
                    "personId": personId,
                    "patronId": patronId,
                    "addressId": addressId
                }
                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;

                    } else {
                        console.log('COMMIT !')
                        // callback({error:error,data:result})
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }

            callback({ error: error, data: result });
        })
    },
    addEvaluation: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let result;
            let error;

            try {
                // Update Person & Address 
                let updateperson = await updatePerson(data);
                let updateaddress = await updateAddress(data.address,data.personid);
                // Insert Person as Marital 
                let insertPersonMarital = await insertPerson(data.marital);
                // Insert Marital
                let maritalInsertid = await insertMarital(data, insertPersonMarital);
                //Update patron (add maritalId)
                let patronupdate = await updatePatron(data, maritalInsertid);
                // Insert evaluation Patron 
                let patronevaluationid = await insertPatronevaluation(data);
                //Insert Family members 
                let insertmember = await insertFamilyMember(data, patronevaluationid);
                //Put Family members 
                let putsurvey = await putSurvey(data, data.survey);

                result = {
                    "patronid": data.patronid,
                    "personid": data.personid,
                    "maritalid": maritalInsertid,
                    "patronevaluationid": patronevaluationid,
                    "patronsurveyid": putsurvey
                }

                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log("ROLLBACK 1");
                        });
                        error = err;
                    } else {
                        console.log("COMMITED");
                        //callback({ error: error, data: result });
                    }
                });
            } catch (err) {
                error = err;
            }

            if (error) {
                console.log("ERR2", error);
                connection.rollback(() => {
                    console.log("ROLLBACK 2");
                });
            }
            console.log("RETURN", [error, result]);
            callback({ error: error, data: result });

        });
    },
    update: async (data, personid) => {
        let results;
        let error;
        console.log({ "data": data, "personid": personid })
        try {
            let personUpdate = await updatePerson(data, personid)

            let addressUpdate = await updateAddress(data, personid)


            results = {
                "person": personUpdate,
                "address": addressUpdate
            }


        } catch (err) {
            error = err
        }

        return [error, results]
    },
    get: async (patronid, serverhost) => {
        let error;
        let resultpatron;
        let resultaddress;
        let resultmarital;
        let resultmember;
        let resultsurvey;
        try {
            let sqlpatron = `
            SELECT 
            person.personid,              	
            patron.patronid,            
            person.citizenid,
            person.firstname,             	
            person.lastname,          	
            person.nickname,
            person.genderid,
            CONCAT('${serverhost}' , person.picturepath) as picturepath,
            m_gender.gendername,              	
            person.prefixid,
            m_prefix.name prefixname,
            person.birthdate,             	
            person.racecode,
            m_race.name racename,
            person.nationcode,
            m_nationality.name nationname,
            person.religioncode,
            m_religion.name religionname,
            person.educationcode,
            m_education.name educationname,
            person.schoolname,          
            peva.patronevaluationid,    
            peva.patronhistory,         
            peva.couplehistory,             
            peva.patronhealth AS health,
            peva.patronpositionworkplace AS positionworkplace,
            peva.patronpersonality AS personality,    
            peva.patronoccupationdetail AS occupationdetail,   
            peva.maritalhistory,        
            peva.income,
            peva.expenses,                  
            peva.lifestyle,             
            peva.familystatus,
            peva.homedetail_sub1,           
            peva.homedetail_sub2,       
            peva.homedetail_sub3,
            peva.homedetail_sub4,           
            peva.environment_sub1,      
            peva.environment_sub2,
            peva.otherfact_sub1,            
            peva.otherfact_sub2,        
            peva.otherfact_sub3,
            peva.otherfact_sub4,            
            peva.otherfact_sub5,        
            peva.otherfact_sub6,
            peva.otherfact_sub7,            
            peva.otherfact_sub8,        
            peva.otherfact_sub9,
            peva.otherfact_sub10,           
            peva.notice,
            peva.comment,
            fn_fullname(person.prefixid, person.firstname, person.lastname) AS fullname  
        FROM patron
            INNER JOIN person
                ON patron.personid = person.personid
            LEFT JOIN patronevaluation peva
                ON patron.patronid = peva.patronid
            LEFT JOIN m_gender
                ON person.genderid = m_gender.genderid
            LEFT JOIN m_prefix
                ON person.prefixid = m_prefix.prefixid
            LEFT JOIN m_race
                ON person.racecode = m_race.code
            LEFT JOIN m_nationality
                ON person.nationcode = m_nationality.code
            LEFT JOIN m_religion
                ON person.religioncode = m_religion.code
            LEFT JOIN m_education
                ON person.educationcode = m_education.code
        WHERE 
            (patron.patronid = ? OR (? = 0))
        `;

            resultpatron = await connection.query(sqlpatron, [patronid, patronid]);

            let sqladdress = `
        SELECT 
            patron.patronid,
            person.Personid,
            address.addressid,
            address.homeno,
            address.moono,
            address.road,
            address.soi,
            address.tumbolcode,
            tumbol.name,
            address.amphurcode,
            amphur.name,
            address.provincecode,
            province.name,
            address.zipcode,
            concat('เลขที่ ',address.homeno,' หมู่ที่ ',address.moono,' ถนน ',address.road,' ซอย ',address.soi,' ตำบล ',tumbol.name,' อำเภอ ',amphur.name,' จังหวัด ',province.name,' รหัสไปรษณีย์ ',address.zipcode) AS fulladdress
        FROM patron
            INNER JOIN person ON patron.personid = person.personid
            INNER JOIN address ON person.personid = address.personid
            LEFT JOIN m_tumbol tumbol ON  address.tumbolcode = tumbol.code
            LEFT JOIN m_amphur amphur ON address.amphurcode = amphur.code
            LEFT JOIN m_province province ON address.provincecode = province.code
        WHERE 
            (patron.patronid = ? OR (? = 0))
        `;

            resultaddress = await connection.query(sqladdress, [patronid, patronid]);

            let sqlmarital = `
            SELECT 
                patron.patronid,					
			    person.personid,            	
			    marital.maritalid,
			    person.citizenid,	
			    peva.patronevaluationid,
			    peva.maritalhealth AS health,
			    peva.maritalpositionworkplace AS positionworkplace,
			    peva.maritalpersonality AS personality,
			    peva.maritaloccupationdetail AS occupationdetail,			
			    person.firstname,             	
			    person.lastname,          
                person.nickname,					
                person.genderid,
                m_gender.gendername,              	
                person.prefixid,
                m_prefix.name prefixname,          	
                person.birthdate,             	
			    person.racecode,
                m_race.name racename,
                person.nationcode,
                m_nationality.name nationname,
                person.religioncode,
                m_religion.name religionname,
                person.educationcode,
                m_education.name educationname,     	
                person.schoolname,
                marital.maritalstatusid
            FROM marital 
		        INNER JOIN patron
                    ON marital.maritalid = patron.maritalid
		        INNER JOIN person
                    ON person.personid = marital.personid
		        LEFT JOIN patronevaluation peva
                    ON patron.patronid = peva.patronid
                LEFT JOIN m_gender
                    ON person.genderid = m_gender.genderid
                LEFT JOIN m_prefix
                    ON person.prefixid = m_prefix.prefixid
                LEFT JOIN m_race
                    ON person.racecode = m_race.code
                LEFT JOIN m_nationality
                    ON person.nationcode = m_nationality.code
                LEFT JOIN m_religion
                    ON person.religioncode = m_religion.code
                LEFT JOIN m_education
                    ON person.educationcode = m_education.code
            WHERE 
                (patron.patronid = ? OR (? = 0))
            `;

            resultmarital = await connection.query(sqlmarital, [patronid, patronid]);

            let sqlmember = `
        SELECT 
            patron.patronid,
            person.personid,
			familymember.familymemberid,	
            peva.patronevaluationid,
            person.prefixid,
			person.firstname,	
			person.lastname,
            person.birthdate,								
			familymember.educationcareer,			
            familymember.note,
            familymember.relationshipid
        FROM patron           
            INNER JOIN patronevaluation peva
                ON patron.patronid = peva.patronid
			INNER JOIN familymember
				ON peva.patronevaluationid = familymember.patronevaluationid
			INNER JOIN person
				ON familymember.personid = person.personid
        WHERE 
            (patron.patronid = ? OR (? = 0))
        `;

            resultmember = await connection.query(sqlmember, [patronid, patronid]);
            let numRows = resultmember.length;
            
            let sqlsurvey = `
            SELECT
                patron.patronid,
                patron.personid,
                patronsurvey.patronsurveyid,
                patronsurvey.surveydate,
                patronsurvey.createdate
            FROM patron
                INNER JOIN patronsurvey ON patron.patronid = patronsurvey.patronid 
            WHERE 
                (patron.patronid = ? OR (? = 0))
            `;
            resultsurvey = await connection.query(sqlsurvey, [patronid, patronid]);

        } catch (err) {
            error = err;
        }
        return [error, resultpatron, resultaddress, resultmarital, resultmember, resultsurvey];

    }

}

module.exports = data;