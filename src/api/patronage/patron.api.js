var express = require('express');
var route = express.Router();
var data = require('./patron.data');
var api = require('../../response-data');
var _conf = require('../../config');

route.post('/put', (req, res, next) => { //function นี้ใช้งาน
    let personid = req.body.personid;
    let response;
    if (!personid) {
        data.add(req.body, (result) => {
            if (result.error) {
                response = api.error(result.error);
            } else {
                response = api.success(result, 'Success, Add Patron.');
            }
            api.sending(req, res, response);
        });
    } else {
        data.update(req.body, (result) => {
            if (result.error) {
                response = api.error(result.error);
            } else {
                response = api.success(result, 'Success, Edit Patron.');

            }
            api.sending(req, res, response);
        });
    }


});
route.post('/list', async (req, res) => { //function นี้ใช้งาน

    let [error, results] = await data.get(0);
    console.log("results => ", results);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Get Patron');
    }
    api.sending(req, res, response);
});

route.post('/get', async (req, res) => { //function นี้ใช้งาน
    let patronid = req.body.patronid;
    let imgUrl = _conf.getImagePersonUrl() + "/";
    
    let response;
    if (!patronid) {
        response = api.error('patronid Notfound.');
        api.sending(req, res, response);
        return;
    }
    let [error, results] = await data.get(patronid, imgUrl);

    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Get Patron');
    }
    
    api.sending(req, res, response);
});
route.post('/search', async (req, res) => { //function นี้ใช้งาน
    console.log('req.body =>',req.body);
    let fullnamepatron = req.body.fullnamepatron;
    let fullnameorphan = req.body.fullnameorphan;
    console.log('fullnamepatron =>',fullnamepatron);
    console.log('fullnameorphan =>',fullnameorphan);
    let [error, results] = await data.search(fullnamepatron,fullnameorphan);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Search Patron & Orphan');
    }
    api.sending(req, res, response);
});
route.post('/searchpatron', async (req, res) => { 
    console.log('req.body =>',req.body);
    let fullnamepatron = req.body.fullnamepatron;
    let fullnameorphan = req.body.fullnameorphan;
    let [error, results] = await data.searchPatron(fullnamepatron,fullnameorphan);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Search Patron & Orphan');
    }
    api.sending(req, res, response);
});
route.post('/serchorphan', async (req, res) => {
    let fullname = req.body.fullname
    let [error, results] = await data.serchorphan(fullname);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Search Orphan');
    }
    api.sending(req, res, response);
});


module.exports = route;