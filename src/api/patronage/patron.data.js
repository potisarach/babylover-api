var connection = require('../../dbconnection');
var fs = require('fs');
var uuid = require('uuidv4');
var _conf = require('../../config');

let putPerson = async (data) => {
    let pathimg;
    if (data.picturepath) {
        let base64toImage = data.picturepath.split(';base64,').pop();
        picture_name = uuid() + ".jpg";
        console.log('pictureName>>>>>', picture_name)
        fs.writeFile(_conf.getUploadPathImgPerson() + "/" + picture_name, base64toImage, 'base64', function (err) {
            console.log("File Created Success")
            console.log(err)
        });
        pathimg = picture_name;
    }
    let values = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "occupcode": data.occupcode,
        "racecode": data.racecode,
        "nationcode": data.nationcode,
        "religioncode": data.religioncode,
        "educationcode": data.educationcode,
        "schoolname": data.schoolname,
        "picturepath": pathimg
    };
    let valuesupdate = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "occupcode": data.occupcode,
        "racecode": data.racecode,
        "nationcode": data.nationcode,
        "religioncode": data.religioncode,
        "educationcode": data.educationcode,
        "schoolname": data.schoolname
    };
    let sql;
    if (data.personid && data.picturepath == '') {
        sql = 'UPDATE person SET ? WHERE personid=?';
        let person = await connection.query(sql, [valuesupdate, data.personid]);
        return person.affectedRows;
    } else if (data.personid) {
        sql = 'UPDATE person SET ? WHERE personid=?';
        let person = await connection.query(sql, [values, data.personid]);
        return person.affectedRows;
    } else {
        sql = 'INSERT INTO person SET ?';
        let person = await connection.query(sql, values);
        let personId = person.insertId;
        return personId;
    }
}
let putAddress = async (data, personid) => {

    let result = await connection.query('SELECT addressid FROM address WHERE personid=?', [personid]);
    let sql;
    let values = {
        "personid": personid,
        "homeno": data.homeno,
        "moono": data.moono,
        "road": data.road,
        "soi": data.soi,
        "tumbolcode": data.tumbolcode,
        "amphurcode": data.amphurcode,
        "provincecode": data.provincecode,
        "zipcode": data.zipcode
    }
    if (result.length == 0) {
        sql = 'INSERT INTO address SET ?';
        let address = await connection.query(sql, values);
        return address.insertId;
    } else {
        sql = 'UPDATE address SET ? WHERE personid = ?';
        let rows = await connection.query(sql, [values, personid]);
        return rows;
    }
}

let putPatron = async (data, personId) => {
    let sql = 'INSERT INTO patron SET ?';
    let values = {
        "personid": personId,
        "maritalid": data.maritalid
    }
    let patron = await connection.query(sql, values)
    let patronId = patron.insertId

    return patronId;
}
let putPatronage = async (data, patronid) => {
    let resultinsert;
    let resultupdate;
    if (data.patronid) {
        if (data.orphanid) {
            let valuesinsert = {
                "patronid": data.patronid,
                "orphanid": data.orphanid,
                "joinfamilydate": data.joinfamilydate
            }
            let valuesupdate = {
                "patronid": data.patronid,
                "orphanid": data.orphanid
            }
            let orphanstatusvalus = {
                "statusid": 2,
                "orphanid": data.orphanid
            }
            let orphanstatus = {
                "statusid": 1
            }
            if (data.patronageid) {

                let sqlupdatestatus = 'UPDATE orphanstatus SET ? WHERE orphanid = ?'
                let resultupdate = await connection.query(sqlupdatestatus, [orphanstatus, data.patronage.orphanid])
                console.log(data.patronage.orphanid);

                let sql = 'UPDATE patronage SET ? WHERE patronageid = ?';
                resultupdate = await connection.query(sql, [valuesupdate, data.patronageid]);
                console.log(JSON.stringify('Update => ' + valuesupdate));

                let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?';
                resultupdate = await connection.query(sqlupdate, [orphanstatusvalus, data.orphanid]);

            } else {
                let sql = 'INSERT INTO patronage SET ?';
                resultinsert = await connection.query(sql, valuesinsert);
                console.log(JSON.stringify('Insert => ' + valuesinsert));

                let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?';
                resultupdate = await connection.query(sqlupdate, [orphanstatusvalus, data.orphanid]);
            }
        }
        else {
            if (data.patronageid) {
                let statusvalus = {
                    "statusid": 1,
                }
                let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?'
                let resultupdate = await connection.query(sqlupdate, [statusvalus, data.patronage.orphanid])
                console.log(data.patronage.orphanid);

                console.log("OrphanId >>>>>>>>>>", data.patronage.orphanid);
                let sql = 'DELETE FROM patronage WHERE patronageid = ?';
                let resultDelete = await connection.query(sql, data.patronageid);

            }

        }
    } else {
        if (data.orphanid) {
            let valuesinsert = {
                "patronid": patronid,
                "orphanid": data.orphanid,
                "joinfamilydate": data.joinfamilydate
            }
            let orphanstatusvalus = {
                "statusid": 2,
                "orphanid": data.orphanid
            }
            let sql = 'INSERT INTO patronage SET ?';
            resultinsert = await connection.query(sql, valuesinsert);
            console.log(JSON.stringify('Insert => ' + valuesinsert));

            let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?';
            resultupdate = await connection.query(sqlupdate, [orphanstatusvalus, data.orphanid]);
        }
    }
}

module.exports = {  //*
    add: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result;
            try {
                let personId = await putPerson(data);
                let addressid = await putAddress(data, personId);
                let patronId = await putPatron(data, personId);
                let patronageId = await putPatronage(data, patronId);

                result = {
                    "personid": personId,
                    "patronId": patronId,
                    "patronageId": patronageId
                }
                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;
                    } else {
                        console.log('COMMIT !')
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }

            callback({ error: error, data: result });
        })
    },
    update: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result = { success: false };
            try {
                let personid = data.personid;
                let personRows = await putPerson(data);
                let address = await putAddress(data, personid);
                let patronageId = await putPatronage(data);
                result = {
                    success: true,
                    "patronageId": patronageId
                };
                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;
                    } else {
                        console.log('COMMIT !')
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }

            callback({ error: error, data: result });
        })
    },
    get: async (patronid, imgUrl) => { //*

        let results;
        let error;
        let sql;
        try {
            sql = `
        SELECT 
            patron.patronid,
            pp .personid,
            pp.citizenid,
            pp.firstname,
            pp.lastname,
            pp.nickname,
            pp.genderid,
            pp.prefixid,
            pp.birthdate, 
            pp.occupcode, 
            pp.racecode, 
            pp.nationcode, 
            pp.religioncode, 
            pp.educationcode, 
            pp.schoolname,
            CONCAT('${imgUrl}' , pp.picturepath) as picturepath,
            orphan.orphanid,
            pop.personid AS personidorphan,
            pop.firstname AS firstnameorphan,
            pop.lastname AS lastnameorphan,
            pop.birthdate AS birthdateorphan,
            pop.nickname AS nicknameorphan,
            address.homeno,
            address.moono,
            address.road,
            address.soi,
            address.tumbolcode,
            address.amphurcode,
            address.provincecode,
            address.zipcode,
            patronage.patronageid,
            m_province.name AS provincename,
            m_amphur.name AS amphurname,
            m_tumbol.name AS tumbolname,
            m_gender.gendername,
            m_education.name AS educationname,
            m_nationality.name AS nationalityname,
            m_race.name AS racename,
            m_religion.name AS religionname,
            fn_fullname(pp.prefixid, pp.firstname, pp.lastname) AS fullnamepatron,
            fn_fullname(pop.prefixid, pop.Firstname, pop.lastname) AS fullnameorphan,
            fn_age(pop.birthdate) AS ageorphan,
            fn_age(pp.birthdate) AS agepatron
        FROM  patron
            INNER JOIN person pp ON patron.personid=pp.personid
            LEFT JOIN address ON pp.personid=address.personid 
            LEFT JOIN patronage ON patron.patronid=patronage.patronid
            LEFT JOIN orphan ON patronage.orphanid=orphan.orphanid
            LEFT JOIN person pop ON orphan.personid=pop.personid
            LEFT JOIN m_province ON address.provincecode = m_province.code
            LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
            LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
            LEFT JOIN m_gender ON pp.genderid = m_gender.genderid
            LEFT JOIN m_education ON pp.educationcode = m_education.code
            LEFT JOIN m_nationality ON pp.nationcode = m_nationality.code
            LEFT JOIN m_race ON pp.racecode = m_race.code
            LEFT JOIN m_religion ON pp.religioncode = m_religion.code
        WHERE 
            patron.patronid = ? OR ? = 0
        ORDER BY 
            patronid ASC
                `;
            let resultpatron = await connection.query(sql, [patronid, patronid]);

            sql = `
                SELECT 
                    patronage.patronageid,
                    patronage.patronid,
                    patronage.orphanid
                FROM  patron
                    LEFT JOIN patronage ON patron.patronid=patronage.patronid
                WHERE 
                    patron.patronid = ? OR ? = 0
                ORDER BY 
                    patron.patronid ASC
            `;
            let resultpatronage = await connection.query(sql, [patronid, patronid]);

            results = resultpatron[0];
            results.patronage = resultpatronage[0];

        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    search: async (fullnamepatron, fullnameorphan) => {
        let error
        let results;
        let dataPatron = '%' + fullnamepatron + '%';
        let dataOrphan = '%' + fullnameorphan + '%';
        try {
            let sql = `
                SELECT
                    patron.patronid,
                    pp.personid AS personidpatron,
                    pp.prefixid AS prefixid,
                    pp.firstname AS firstname,
                    pp.lastname AS lastname,
                    pp.birthdate AS birthdate,
                    address.homeno,
                    address.moono,
                    address.road,
                    address.soi,
                    address.tumbolcode,
                    address.amphurcode,
                    address.provincecode,
                    m_province.name AS provincename,
                    m_amphur.name AS amphurname,
                    m_tumbol.name AS tumbolname,
                    patronage.patronageid,
                    orphan.orphanid,
                    pop.personid AS personidorphan,
                    pop.prefixid AS prefixidorphan,
                    pop.firstname AS firstnameorphan,
                    pop.lastname AS lastnameorphan,
                    pop.birthdate AS birthdateorphan,
                    pop.nickname AS nicknameorphan,
                    peva.patronevaluationid,
                    fn_fullname(pp.prefixid, pp.firstname, pp.lastname) AS fullnamepatron,
                    fn_fullname(pop.prefixid, pop.Firstname, pop.lastname) AS fullnameorphan,
                    fn_age(pop.birthdate) AS ageorphan,
                    fn_age(pp.birthdate) AS agepatron       
                FROM patron
                    INNER JOIN person pp ON patron.personid = pp.personid
                    LEFT JOIN patronage ON patron.patronid = patronage.patronid
                    LEFT JOIN orphan ON patronage.orphanid = orphan.orphanid
                    LEFT JOIN person pop ON orphan.personid = pop.personid
                    LEFT JOIN patronevaluation peva ON patron.patronid = peva.patronid
                    LEFT JOIN address ON pp.personid =address.personid
                    LEFT JOIN m_province ON address.provincecode = m_province.code
                    LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
                    LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
                WHERE 
                    (fn_fullname(pp.prefixid, pp.firstname, pp.lastname) LIKE ? OR ( ? = ""))
                    AND 
                    (fn_fullname(pop.prefixid, pop.firstname, pop.lastname) LIKE ? OR ( ? = ""))
                ORDER BY 
                    pp.birthdate DESC
                    `;
            results = await connection.query(sql, [dataPatron, fullnamepatron, dataOrphan, fullnameorphan]);

        } catch (err) {
            error = err;
        }
        return [error, results];

    },
    searchPatron: async (fullnamepatron, fullnameorphan) => {
        let error
        let results;
        let dataPatron = '%' + fullnamepatron + '%';
        let dataOrphan = '%' + fullnameorphan + '%';
        try {
            let sql = `
                SELECT
                    patron.patronid,
                    pp.personid AS personidpatron,
                    pp.prefixid AS prefixid,
                    pp.firstname AS firstname,
                    pp.lastname AS lastname,
                    pp.birthdate AS birthdate,
                    address.homeno,
                    address.moono,
                    address.road,
                    address.soi,
                    address.tumbolcode,
                    address.amphurcode,
                    address.provincecode,
                    m_province.name AS provincename,
                    m_amphur.name AS amphurname,
                    m_tumbol.name AS tumbolname,
                    patronage.patronageid,
                    orphan.orphanid,
                    pop.personid AS personidorphan,
                    pop.prefixid AS prefixidorphan,
                    pop.firstname AS firstnameorphan,
                    pop.lastname AS lastnameorphan,
                    pop.birthdate AS birthdateorphan,
                    pop.nickname AS nicknameorphan,
                    peva.patronevaluationid,
                    fn_fullname(pp.prefixid, pp.firstname, pp.lastname) AS fullnamepatron,
                    fn_fullname(pop.prefixid, pop.Firstname, pop.lastname) AS fullnameorphan,
                    fn_age(pop.birthdate) AS ageorphan,
                    fn_age(pp.birthdate) AS agepatron       
                FROM patron
                    INNER JOIN person pp ON patron.personid = pp.personid
                    LEFT JOIN patronage ON patron.patronid = patronage.patronid
                    LEFT JOIN orphan ON patronage.orphanid = orphan.orphanid
                    LEFT JOIN person pop ON orphan.personid = pop.personid
                    LEFT JOIN patronevaluation peva ON patron.patronid = peva.patronid
                    LEFT JOIN address ON pp.personid =address.personid
                    LEFT JOIN m_province ON address.provincecode = m_province.code
                    LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
                    LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
                WHERE 
                    (fn_fullname(pp.prefixid, pp.firstname, pp.lastname) LIKE ? OR ( ? = ""))
                    AND 
                    (fn_fullname(pop.prefixid, pop.firstname, pop.lastname) LIKE ? OR ( ? = ""))
                    AND 
                    ISNULL(patronage.patronageid)
                ORDER BY 
                    pp.birthdate DESC
                    `;
            results = await connection.query(sql, [dataPatron, fullnamepatron, dataOrphan, fullnameorphan]);

        } catch (err) {
            error = err;
        }
        return [error, results];

    },
    serchorphan: async (fullname) => {
        let error
        let result
        fullname = '%' + fullname + '%';
        try {
            let sql = `
            SELECT  
                person.personid,
                person.citizenid,
				person.firstname,
				person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                orphan.orphanid,
                orphanstatus.orphanstatusid,
				orphanstatus.statusid,
				m_orphanstatus.name AS orphanstatusname,
				patronage.patronageid,
                fn_fullname(prefixid, firstname, lastname) AS fullname,
                fn_age(person.birthdate) AS age
            FROM person 
                INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid		
			    LEFT JOIN patronage ON orphan.orphanid = patronage.orphanid
            WHERE 
                (fn_fullname(prefixid, firstname, lastname) LIKE ? OR ( ? = "")) 
                AND 
                ISNULL(patronage.patronageid)
                AND
                orphanstatus.statusid = 1
            ORDER BY 
                BirthDate DESC
            `;

            result = await connection.query(sql, [fullname, fullname]);

        } catch (err) {
            error = err;
        }
        return [error, result];
    }

}
