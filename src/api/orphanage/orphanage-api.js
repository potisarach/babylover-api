var express = require('express');
var router = express.Router();
var orphanageData = require('./orphanage-data');
var responseApi = require('../../response-data');

router.post('/get',(req,res)=>{
    let orphanageid = req.body.orphanageid;

    orphanageData.get(orphanageid,(err,results,field)=>{
        let resp = responseApi.getResponse();
        resp.header.api = "orphanage/get"
        resp.header.token = "AAAAAA";
        
        if(err){
            resp.status = "";
            resp.message = "" + err;

        }
        else{
            resp.status = "";
            resp.message = "";
            resp.body = results;
        
        }
    responseApi.sending(req,res,resp);
    });
});
router.post('/add',(req,res)=>{
    let orphanagename = req.body.orphanagename;
    let contact = req.body.contact;
    let phone = req.body.phone;
    let fax = req.body.fax;
    let email = req.body.email;
    if(!orphanagename){
        let response = {success:false}
    }
    else{
        let values = {
            "orphanagename":orphanagename,
            "contact":contact,
            "phone":phone,
            "fax":fax,
            "email":email
        };
        orphanageData.add(values,(err, results, fields)=>{
            let resp = responseApi.getResponse();
            resp.header.api = "sponsor/add"
            resp.header.token = "AAAAAA";
            if(err){
                resp.status = "";
                resp.message = "" + err;
            }
            else{
                resp.status = "";
                resp.message = "";
                resp.body = results;
        }
        responseApi.sending(req,res,resp);
        });
    };
});

router.post('/update',(req,res)=>{
    let orphanageid = req.body.orphanageid;
    let orphanagename = req.body.orphanagename;
    let contact = req.body.contact;
    let phone = req.body.phone;
    let fax = req.body.fax;
    let email = req.body.email;
    if(!orphanagename){
        let response = {success:false}
    }
    else{
        let values = {
            "orphanagename":orphanagename,
            "contact":contact,
            "phone":phone,
            "fax":fax,
            "email":email
        };
        orphanageData.update(orphanageid,values,(err, results, fields)=>{
            let resp = responseApi.getResponse();
            resp.header.api = "sponsor/update"
            resp.header.token = "AAAAAA";
            if(err){
                resp.status = "";
                resp.message = "" + err;
            }
            else{
                resp.status = 'Success';
                resp.message = 'Suc'
                resp.data = results.affectedRows;
            }
        
        responseApi.sending(req,res,resp)
        });
    };
});

router.post('/delete', (req, res)=>{
    let orphanageid = req.body.orphanageid

    if(!orphanageid){ 
        let response = {success : false}
        res.send(response).end();
        return;
    }

    orphanageData.delete(orphanageid,(err,results,field)=>{
        let resp = responseApi.getResponse();
        resp.header.api = "sponsor/dalete";
        resp.header.token = "AAAAAA";
        if(err){
            resp.status = "";
            resp.message = "";
        }
        else{
            if(results.affectedRows){
                resp.status = "";
                resp.message = "";
                resp.body = {"rowscount":results.affectedRows,"orphanageid":orphanageid}  
            }
            
            else{
                resp.status = "";
                resp.message = "";
                resp.body = {"orphanageid":orphanageid}
            }
               
        }

        responseApi.sending(req,res,resp)
    });
});

module.exports = router;