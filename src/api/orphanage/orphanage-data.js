var connection = require('../../dbconnection');
var orphanageData = {
    
    get: (orphanageid,callback)=>{
        let sql = 'SELECT * FROM orphanage WHERE orphanageid=?';
        connection.query(sql,orphanageid,(err,results,field)=>{
            if(err){
                console.log(err);
            }else{
                console.log(results);
            }
            callback(err,results,field);
        });
    },
    add:(values,callback)=>{
        let sql = 'INSERT INTO orphanage SET ?';
        connection.query(sql, values, (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        });
    },
    update: (orphanageid, values , callback)=>{
        let sql = 'UPDATE orphanage SET ? WHERE orphanageid=?';
        connection.query(sql, values, orphanageid, (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        }); 
    },
    delete: (orphanageid, callback)=>{
        let sql = 'DELETE FROM orphanage WHERE orphanageid=?';
        connection.query(sql,orphanageid, (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        });
    }

   
}

module.exports = orphanageData;
