var express = require('express');
var router = express.Router();
var conn = require('../dbconnection');
var globalFunc = require('../global-function');
var responseApi = require('../response-data');
router.post('/1',(req, res, next)=>{
    //console.log(req.query);
  
    let objectKeysToLowerCase = function (input) {
        if (typeof input !== 'object') return input;
        if (Array.isArray(input)) return input.map(objectKeysToLowerCase);
        return Object.keys(input).reduce(function (newObj, key) {
            let val = input[key];
            let newVal = (typeof val === 'object') ? objectKeysToLowerCase(val) : val;
            newObj[key.toLowerCase()] = newVal;
            return newObj;
        }, {});
    };
    conn.query('select * from person',(err, results, fields)=>{
        if(err) throw err;
        //console.log(fields);
        results[0].OBJ = {"ABC":"123"}
        results[0].ARR = [{"aa":1},{"ss": 2}]
        results[0].ARR2 = [1,[{"AAA":"dddd"},"ed"],5,8,8]
        globalFunc.keyLowercase(results);
        let response = responseApi.getResponse();
        response.body = results;
        responseApi.sending(req, res, response);
    });
}); 
router.post('/2',(req, res, next)=>{
    let response = responseApi.getResponse();
    response.body = "12";
    responseApi.sending(req, res, response);
});
module.exports = router;