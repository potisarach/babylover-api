var express = require('express');
var route = express.Router();
var data = require('./foster.data');
var api = require('../../response-data');

route.post('/put', (req, res, next) => { //function นี้ใช้งาน
    let personid = req.body.personid;
    let response;
    if (!personid) {
        data.add(req.body, (result) => {
            if (result.error) {
                response = api.error(result.error);
            } else {
                response = api.success(result, 'Success Add Foster');
            }
            api.sending(req, res, response);
        });
    } else {
        data.update(req.body, (result) => {
            if (result.error) {
                response = api.error(result.error);
            } else {
                response = api.success(result, 'Success Update Foster');
            }
            api.sending(req, res, response);
        });
    }
});

route.post('/get', async (req, res) => { //function นี้ใช้งาน
    // let fosterid = req.body.fosterid;
    let orphanid = req.body.orphanid;
    let response;
    if (!orphanid) {
        response = api.error('FosterID Notfound!');
        api.sending(req, res, response);
        return;
    }
    let [error, resultData] = await data.get(orphanid);
    if (error) {
        response = api.error(error);
    } else {
        if (resultData.length == 0) {
            result = {
                "fosterid": null,
                "personid": null,
                "citizenid": null,
                "firstname": null,
                "lastname": null,
                "nickname": null,
                "genderid": null,
                "prefixid": null,
                "birthdate": null,
                "occupcode": null,
                "racecode": null,
                "nationcode": null,
                "religioncode": null,
                "educationcode": null,
                "schoolname": null,
                "homeno": null,
                "moono": null,
                "road": null,
                "soi": null,
                "tumbolcode": null,
                "amphurcode": null,
                "provincecode": null,
                "zipcode": null,
                "orphanid": null,
                "fullname": null,
                "Age": null
            }
        } else {
            result = resultData[0];
        }
        response = api.success(result, 'Success Get Foster');
    }
    api.sending(req, res, response);
});

module.exports = route;