var connection = require('../../dbconnection');

let putPerson = async (data) => {
    let values = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "occupcode": data.occupcode,
        "racecode": data.racecode,
        "nationcode": data.nationcode,
        "religioncode": data.religioncode,
        "educationcode": data.educationcode,
        "schoolname": data.schoolname
    }
    let sql;
    if (data.personid) {
        sql = 'UPDATE person SET ? WHERE personid = ?';
        let person = await connection.query(sql, [values, data.personid]);
        return person.affectedRows;
    } else {
        sql = 'INSERT INTO person SET ?';
        let person = await connection.query(sql, values);
        let personId = person.insertId;
        return personId;
    }
}
let putAddress = async (data, personid) => {

    let result = await connection.query('SELECT addressid FROM address WHERE personid=?', [personid]);
    let sql;
    let values = {
        "personid": personid,
        "homeno": data.homeno,
        "moono": data.moono,
        "road": data.road,
        "soi": data.soi,
        "tumbolcode": data.tumbolcode,
        "amphurcode": data.amphurcode,
        "provincecode": data.provincecode,
        "zipcode": data.zipcode
    }
    if (result.length == 0) {
        sql = 'INSERT INTO address SET ?';
        let address = await connection.query(sql, values);
        return address.insertId;
    } else {
        sql = 'UPDATE address SET ? WHERE personid = ?';
        let rows = await connection.query(sql, [values, personid]);
        return rows;
    }
}
let putFoster = async (data, personid) => {
    let sql = 'INSERT INTO foster SET ?';
    let values = {
        "personid": personid,
        "orphanid": data.orphanid,
        "fosterstatusid": data.fosterstatusid
    }
    let foster = await connection.query(sql, values)
    let fosterId = foster.insertId
    return fosterId;
}

module.exports = {
    add: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result;
            try {
                let personId = await putPerson(data);
                let addressid = await putAddress(data, personId);
                let fosterId = await putFoster(data, personId);

                result = {
                    "personid": personId,
                    "fosterId": fosterId,
                }
                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;
                    } else {
                        console.log('COMMIT !')
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }

            callback({ error: error, data: result });
        })
    },
    update: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result;
            try {
                let personid = data.personid;
                let personRows = await putPerson(data);
                let address = await putAddress(data, personid);
                result = {
                    personId: personid
                };
                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;
                    } else {
                        console.log('COMMIT !')
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }
            callback({ error: error, data: result });
        })
    },
    get: async (orphanid) => {
        let result;
        let error;
        let sql;
        try {
            sql = `
                SELECT
                    foster.fosterid,
                    foster.fosterstatusid,
                    person.personid,
                    person.citizenid,
                    person.firstname,
                    person.lastname,
                    person.nickname,
                    person.genderid,
                    person.prefixid,
                    person.birthdate,
                    person.occupcode,
                    person.racecode,
                    person.nationcode,
                    person.religioncode,
                    person.educationcode,
                    person.schoolname,
                    address.homeno,
                    address.moono,
                    address.road,
                    address.soi,
                    address.tumbolcode,
                    address.amphurcode,
                    address.provincecode,
                    m_province.name AS provincename,
                    m_amphur.name AS amphurname,
                    m_tumbol.name AS tumbolname,
                    address.zipcode,
                    orphan.orphanid,
                    fn_fullname(person.prefixid, person.firstname, person.lastname) AS fullname,
                    fn_age(person.birthdate) AS Age
                FROM foster
                    LEFT JOIN person ON foster.personid = person.personid
                    LEFT JOIN address ON person.personid = address.personid
                    LEFT JOIN orphan ON foster.orphanid = orphan.orphanid
                    LEFT JOIN m_province ON address.provincecode = m_province.code
                    LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
                    LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code
                WHERE
                    orphan.orphanid = ? OR ? = 0
            `;
            result = await connection.query(sql, [orphanid, orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result]
    }

}