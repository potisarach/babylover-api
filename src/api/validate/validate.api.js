var express = require('express'); 
var router = express.Router();
var data = require('./validate.data');
var responseData = require('../../response-data');

router.post('/checkduplicate/citizenidall', async(req,res)=>{
    let response ;
    let [error, results] = await data.getAll();

    if (error) {
        response = responseData.error(error);
    } else {
        response = responseData.success(results, 'Successed, Get CitizenID ALL');
    }
    responseData.sending(req, res, response);
    
});
router.post('/checkduplicate/citizenid', async(req,res)=>{
    let response ;
    let personid = req.body.personid;
    let citizenid = req.body.citizenid
    let [error, results] = await data.get(personid,citizenid);

    if (error) {
        response = responseData.error(error);
    } else {
        response = responseData.success(results, 'Successed, Get CitizenID');
    }
    responseData.sending(req, res, response);
});
router.post('/checkduplicate/name', async(req, res) => {
    let response;
    let personid = req.body.personid;
    let [error, results] = await data.GetName(personid);

    if(error){
        response = responseData.error(error);
    }else{
        response = responseData.success(results, 'Successed, Get Name');
    }
    responseData.sending(req, res, response)
});
router.post('/checkduplicate/email', async(req, res) => {
    let response;
    let email = req.body.email;
    let [error, results] = await data.GetUserEmail();

    if(error){
        response = responseData.error(error);
    }else{
        response = responseData.success(results, 'Successed, Get Email');
    }
    responseData.sending(req, res, response)
});


module.exports = router;