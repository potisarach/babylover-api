var connection = require('../../dbconnection');

module.exports = {
    getAll: async () => {
        let results;
        let error;
        try {
            let sql = `
                SELECT citizenid FROM person
                `;
            results = await connection.query(sql);
            console.log(results);
        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    get: async (personid, citizenid) => {
        let results;
        let error;
        console.log(personid, citizenid)
        citizenid = '%' + citizenid + '%';
        try {
            let sql = `
                SELECT citizenid FROM person WHERE personid != ? AND citizenid != ?
                `;
            results = await connection.query(sql, [personid, citizenid]);
            console.log(results);
        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    GetName: async (personid) => {
        let results;
        let error;
        try {
            let sql = `
            SELECT personid,firstname,lastname,birthdate FROM person WHERE personid = ? 
            `;
            results = await connection.query(sql, [personid]);

        } catch (err) {
            error = err;
        }
        console.log("Reaaaa", results);

        return [error, results]
    },
    GetUserEmail: async () => {
        let results;
        let error;
        try {
            let sql = `
            SELECT email FROM user 
            `;
            results = await connection.query(sql);
        } catch (err) {
            error = err;
        }

        return [error, results]
    }
}