var connection = require('../../dbconnection');

let putOrphanevalution = async (data) => {
    let resultinsert;
    let resultupdate;
    let evaluationid = [];

    
    for (let items of data.evaluations) {
        for (let sub of items.subs) {
            let values = {
                "orphanevaluationtopic_subid": sub.sub_id,
                "orphanid": data.orphan.orphanid,
                "value": sub.value,
                "orphanevaluationtime": data.orphan.traimas,
                "ranks": data.age
            };
            if (sub.orphanevaluationid) {
                let sql = 'UPDATE orphanevaluation SET ? WHERE orphanevaluationid = ?';
                resultupdate = await connection.query(sql, [values, sub.orphanevaluationid]);
                console.log('Update => ' + JSON.stringify(values));
            } else {
                let sql = 'INSERT INTO orphanevaluation SET ?';
                resultinsert = await connection.query(sql, values);
                evaluationid.push(resultinsert.insertId);
                console.log('Insert => ' + JSON.stringify(values));
            }
        }
    }
    return evaluationid;
}
let updateOrphan = async (data) => {
    let values = {
        "orphanevaluationstatusid": 1
    }
        let sql = 'UPDATE orphan SET ? WHERE orphanid = ?';
        let orphan = await connection.query(sql, [values, data.orphanid])
        let orphanid = orphan.insertId
        console.log(orphanid);
        return orphanid;
}
var expdata = {
    get: async (orphanid) => {
        let error
        let result
        try {
            let sql = `
            SELECT  
                person.personid,
                person.citizenid,
                person.firstname,
                person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                orphan.orphanid,
                orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                m_orphanstatus.name AS orphanstatusname,
                fn_fullname(prefixid, firstname, lastname) AS fullname
            FROM person 
                INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid 
            WHERE 
                orphan.orphanid = ? OR ? = 0
            ORDER BY 
                orphan.orphanid ASC
            `;
            result = await connection.query(sql, [orphanid, orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    getevaluation: async (ranks, orphanid, traimas) => {
        let error;
        let results;

        try {
            let sql = `
            SELECT
                orphan.orphanid, 
                orphan.personid, 
                topic.orphanevaluation_topicid, 
                topic.name AS topicname, 
                sub.orphanevaluation_subid, 
                eva.orphanevaluationid, 
                eva.orphanevaluationtopic_subid, 
                sub.name AS subname, 
                eva.value,
                eva.orphanevaluationtime,
                eva.createdate, 
                sub.ranks
            FROM m_orphanevaluation_topic topic
                INNER JOIN m_orphanevaluation_sub sub ON  sub.orphanevaluation_topicid = topic.orphanevaluation_topicid
                LEFT JOIN orphanevaluation eva ON  eva.orphanevaluationtopic_subid = sub.orphanevaluation_subid AND eva.orphanid = ? AND eva.orphanevaluationtime = ?
                LEFT JOIN orphan ON orphan.orphanid = eva.orphanid
            WHERE 
                sub.ranks = ? 
                AND (eva.orphanid = ? OR ISNULL(eva.orphanid))
                AND (eva.orphanevaluationtime = ? OR ISNULL(eva.orphanevaluationtime))      
            ORDER BY 
                topic.ordinal ASC, 
                sub.ordinal ASC
            `;
            topics = await connection.query(sql, [orphanid, traimas, ranks, orphanid, traimas]);
            let evals = [];
            for (let topic of topics) {
                let head;
                for (let result of evals) {
                    if (topic.orphanevaluation_topicid == result.topic_id) {
                        head = result;
                        continue;
                    }
                }
                if (!head) {
                    head = {
                        topic_id: topic.orphanevaluation_topicid,
                        topic_name: topic.topicname,
                        createdate: topic.createdate,
                        subs: []
                    };
                    evals.push(head);
                }
                let sub = {
                    orphanevaluationid: topic.orphanevaluationid,
                    sub_id: topic.orphanevaluation_subid,
                    sub_name: topic.subname,
                    orphanevaluationtopic_subid: topic.orphanevaluationtopic_subid,
                    value: topic.value,
                    traimas: topic.orphanevaluationtime
                };
                head.subs.push(sub);

            }
            let [error, result] = await expdata.get(orphanid);
            results = {
                orphan: result[0],
                evaluations: evals
            };

        } catch (err) {
            error = err;
        }

        return [error, results];
    },
    put: (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let result;
            let error;
            try {

                let putorphanevalution = await putOrphanevalution(data);
                result = {
                    "orphanevaluationid": putorphanevalution
                };
                console.log(result);
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                        error = err;
                    } else {
                        console.log('COMMIT')
                        callback({ error: error, data: result })
                    }
                })
            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') })
            }
            callback({ error: error, data: result })
        })

    },
    search: async (fullname, agestart, ageend, evaluationstatus) => {
        let error;
        let result;
        let sql;

        
        fullname = '%' + fullname + '%'; 
         try {
            sql = `
            SELECT
                person.personid,
                person.citizenid,
                person.firstname,
                person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                orphan.orphanid,
                orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                m_orphanstatus.name AS orphanstatusname,
                fn_fullname(prefixid, firstname, lastname) AS fullname,
                fn_age(birthdate) AS age
            FROM person 
                INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
            WHERE 
                (fn_fullname(person.prefixid, person.firstname, person.lastname) LIKE ? OR (? = "")) 
                AND 
                (fn_age(person.birthdate)  BETWEEN ? AND ?)
                AND 
                (fn_age(person.birthdate)  BETWEEN 1.0 AND 6.0)
            ORDER BY 
            person.birthdate DESC

            `;
            let list = await connection.query(sql, [fullname, fullname, agestart, ageend]);
            
            sql = `
                SELECT  DISTINCT 
                    orphan.orphanid,
                    eva.orphanevaluationtime,
                    (case 
						when 
                            (select count(sub.OrphanID) from orphanevaluation sub where sub.orphanid = eva.orphanid 
                            and eva.orphanevaluationtime = sub.orphanevaluationtime 
                            and sub.value is null ) > 0 
						then 0 
						else (case when eva.orphanevaluationtime is not null then 1 else 0 end)
                        end) as completed,
                    eva.ranks AS ranks

                FROM person 
                    INNER JOIN orphan ON person.personid = orphan.personid
                    LEFT JOIN orphanevaluation eva ON orphan.orphanid = eva.orphanid
                WHERE
                    (fn_fullname(prefixid, firstname, lastname) LIKE ? OR (? = "")) 
                AND
                    (fn_age(birthdate)  BETWEEN ? AND ?)
                AND 
                    (fn_age(birthdate)  BETWEEN 1.0 AND 6.0)
                `;
            let traimas =await connection.query(sql, [ fullname, fullname, agestart, ageend]);
            let arrData = []; 
            for(let data of list){
                let head;
                if(!head){
                    head = {
                        age: data.age,
                        birthdate: data.birthdate,
                        citizenid: data.citizenid,
                        firstname: data.firstname,
                        fullname: data.fullname,
                        genderid: data.genderid,
                        lastname: data.lastname,
                        nickname: data.nickname,
                        orphanid: data.orphanid,
                        orphanstatusid: data.orphanstatusid,
                        orphanstatusname: data.orphanstatusname,
                        personid: data.personid,
                        prefixid: data.prefixid,
                        statusid: data.statusid,
                        evaluation:[]
                    }
                    arrData.push(head);
                }
                for(let eva of traimas){
                    if(data.orphanid == eva.orphanid){                        
                        head.evaluation.push({time: eva.orphanevaluationtime, completed: eva.completed , ranks: eva.ranks});
                        continue;
                    }
                }
            }

            result = arrData;

        } catch (err) {
            error = err;
        }
        return [error, result];
    },
};

module.exports = expdata;