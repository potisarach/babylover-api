var express = require('express');
var router = express.Router();
var evaluationData = require('./evaluation.data');
var responseData = require('../../response-data');

router.post('/get', async (req, res) => { //function นี้ใช้งาน
    let orphanid = req.body.orphanid;
    let [error, results] = await evaluationData.get(orphanid);
    let response = {};
    response.status = error ? 'fail' : 'success';
    response.message = error || 'Get List';
    response.data = results[0];
    responseData.sending(req, res, response);
});
router.post('/getevaluation', async (req, res) => { //function นี้ใช้งาน
    let orphanid = req.body.orphanid;
    let age = req.body.age;
     let traimas = req.body.traimas;
    let data;
    if(age >= 5.1 && age <= 6.0 ){
        data = {"ranks": '5-6'};
    }else if(age >= 4.1 && age <= 5.0 ){
        data = {"ranks": '4-5'};
    }else if(age >= 3.1 && age <= 4.0 ){
        data = {"ranks": '3-4'};
    }else if(age >= 2.1 && age <= 3.0 ){
        data = {"ranks": '2-3'};
    }else if(age >= 1.0 && age <= 2.0 ){
        data = {"ranks": '1-2'};
    }else{
        responseData.error('error');
    }
    
    let [error, results] = await evaluationData.getevaluation(data.ranks, orphanid, traimas);
    let response = {};
    response.status = error ? 'fail' : 'success';
    response.message = error || 'Get List';
    response.data = results;
    responseData.sending(req, res, response);

});
router.post('/add', (req, res) => { //function นี้ใช้งาน
    let age = req.body.age;
    let data;
    if(age >= 5.1 && age <= 6.0 ){
        data = {"ranks": '5-6'};
    }else if(age >= 4.1 && age <= 5.0 ){
        data = {"ranks": '4-5'};
    }else if(age >= 3.1 && age <= 4.0 ){
        data = {"ranks": '3-4'};
    }else if(age >= 2.1 && age <= 3.0 ){
        data = {"ranks": '2-3'};
    }else if(age >= 1.0 && age <= 2.0 ){
        data = {"ranks": '1-2'};
    }else{
        responseData.error('error');
    }
    console.log("ranks : ",data.ranks);
    
    req.body.age = data.ranks;

    evaluationData.put(req.body, (result) => {
        let response;
        if (result.error) {
            response = responseData.error(result.error);
        } else {
            response = responseData.success(result.data, 'Successed, Added Orphanevaluation');
        }
        responseData.sending(req, res, response);
    })
});
router.post('/list', async (req, res) => { //function นี้ใช้งาน
    let fullname = req.body.fullname
    let orphanstatus = req.body.orphanstatus
    let ranks = req.body.ranks
    let evaluationstatus = req.body.evaluationstatus
    let agelength ;
    
    if(ranks == "1-2"){
        agelength = {"agestart":1.0,
                     "ageend":2.0
                    }
    }else if(ranks == "2-3"){
        agelength = {"agestart":2.1,
                     "ageend":3.0
                    }
    }else if(ranks == "3-4"){
        agelength = {"agestart":3.1,
                     "ageend":4.0
                    }
    }else if(ranks == "4-5"){
        agelength = {"agestart":4.1,
                     "ageend":5.0
                    }
    }else if(ranks == "5-6"){
        agelength = {"agestart":5.1,
                     "ageend":6.0
                    }
    }else{
        agelength = {"agestart":0,
        "ageend":999
       }
    }
    
    let [error, results] = await evaluationData.search(fullname, agelength.agestart, agelength.ageend, evaluationstatus);
    let response;

    
    
    if (error) {
        response = responseData.error(error);
    } else {
        response = responseData.success(results, 'Successed, Search Orphan');
    }
    responseData.sending(req, res, response);
});

// router.post('/getHistory')

module.exports = router;