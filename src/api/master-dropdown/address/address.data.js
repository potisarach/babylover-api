var connection = require('../../../dbconnection');

module.exports = {

    provinces: async()=>{
        let error;
        let result;
        try {
            result = await connection.query('SELECT code, name FROM m_province ORDER BY name');
        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    amphurs: async(provincecode)=>{
        let error;
        let result;
        try{
            result = await connection.query('SELECT code, name FROM m_amphur WHERE provincecode=? ORDER BY name', provincecode);
        } catch(err){
            error = err;
        }
        return [error, result];
    },
    tumbols: async(amphurcode)=>{
        let error;
        let result;
        try{
            result = await connection.query('SELECT code, name FROM m_tumbol WHERE amphurcode=? ORDER BY name', amphurcode);
        }catch(err){
            error = err;
        }
         return [error, result];
    }
}