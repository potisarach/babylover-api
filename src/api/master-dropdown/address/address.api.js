var express = require('express');
var router = express.Router();
var addressData = require('./address.data');
var responseData = require('../../../response-data');

router.post('/province/list', async (req, res)=>{

    let [error, result] = await addressData.provinces();
    let data = {};
    data.status = error?'fail':'success';
    data.message = error || 'Get List';
    data.data = result;
    responseData.sending(req, res, data);
});

router.post('/amphur/list', async (req, res)=>{
    let [error, result] = await addressData.amphurs(req.body.provincecode);
    let data = {};
    data.status = error?'fail':'success';
    data.message = error || 'Get List';
    data.data = result;
    responseData.sending(req, res, data);

});

router.post('/tumbol/list', async (req, res)=>{
    let [error, result] = await addressData.tumbols(req.body.amphurcode);
    let data = {};
    data.status = error?'fail':'success';
    data.message = error || 'Get List';
    data.data = result;
    responseData.sending(req, res, data);
});

module.exports = router;