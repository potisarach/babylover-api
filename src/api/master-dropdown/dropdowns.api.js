var express = require('express');
var route = express.Router();
var resData = require('../../response-data');
var dropdownData = require('./dropdown.data');

route.post('/prefix/list', async (req, res)=>{
    //If no value Set Default is 0
    let genderid = req.body.genderid || 0;
    let [error,list] = await dropdownData.prefix.list(genderid);
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/gender/list', async (req, res)=>{
    let [error,list] = await dropdownData.gender.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    //req.secure
    resData.sending(req, res, data);
});

route.post('/bloodtype/list',async (req,res)=>{
    let [error,list] = await dropdownData.bloodtype.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/rhgroup/list',async(req,res)=>{
    let [error,list] = await dropdownData.rhgroup.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/education/list',async(req,res)=>{
    let [error,list] = await dropdownData.education.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/nationality/list',async(req,res)=>{
    let [error,list] = await dropdownData.nationality.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/occupation/list',async(req,res)=>{
    let [error,list] = await dropdownData.occupation.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/race/list',async(req,res)=>{
    let [error,list] = await dropdownData.race.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/religion/list',async(req,res)=>{
    let [error,list] = await dropdownData.religion.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});

route.post('/relationship/list',async(req,res)=>{
    let [error,list] = await dropdownData.relationship.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/maritalstatus/list',async(req,res)=>{
    let [error,list] = await dropdownData.maritalstatus.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/vaccine/list',async(req,res)=>{
    let [error,list] = await dropdownData.vaccine.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/statusOrphan/list', async(req, res)=>{
    let [error, list] = await dropdownData.statusOrphan.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List ' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/evaluationStatusOrphan/list', async(req, res)=>{
    let [error, list] = await dropdownData.evalutionStatusOrphan.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List '+ req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});
route.post('/fosterStatus', async (req, res) => {
    let [error, list] = await dropdownData.fosterStatus.list();
    let data = {};
    data.status = (error?'fail':'success');
    data.message = error || 'Get List' + req.originalUrl;
    data.data = list;
    resData.sending(req, res, data);
});

module.exports = route;

