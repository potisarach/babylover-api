var conn = require('../../dbconnection');

module.exports = {
    prefix:{
        list: async (genderid=0)=>{
            let results = [];
            let error;
            try {
                results = await conn.query('SELECT prefixid, name FROM m_prefix WHERE isactive=1 AND (genderid=? OR ?=0) ORDER BY prefixid', [genderid,genderid]);

            } catch (err) {
                
                error = err ;
                console.error('ERROR::', error);
            }
            return [error,results];
        }
    },
    gender:{
        list: async ()=>{
            let results = [];
            let error;
            try {
                results = await conn.query('SELECT genderid,gendername FROM m_gender ORDER BY genderid');
            } catch (err) {
                
                error = err;
                console.error('ERROR::', error);
            }
            return [error, results];
        }
    },
    bloodtype:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT * FROM m_bloodtyp');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    rhgroup:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT * FROM m_rhgroup');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    education:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT code,name FROM m_education');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    nationality:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT code,name FROM m_nationality WHERE isactive = 1 ORDER BY name');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    occupation:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT code,name FROM m_occupation');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    race:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT code,name FROM m_race WHERE isactive = 1 ORDER BY name');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    religion:{
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT code,name FROM m_religion ORDER BY name');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    relationship: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT relationshipid, name FROM m_relationship');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    maritalstatus: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT maritalstatusid, name FROM m_maritalstatus');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    vaccine: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT vaccineid, name FROM m_vaccine');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    statusOrphan: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT orphanstatusid, name FROM m_orphanstatus');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    evalutionStatusOrphan: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results = await conn.query('SELECT orphanevaluationstatusid, name FROM m_orphanevaluationstatus');
            }catch(err){
                console.error('ERROR::', error);
                error = err
            }
            return [error, results];
        }
    },
    fosterStatus: {
        list: async ()=>{
            let results = [];
            let error;
            try{
                results =await conn.query('SELECT fosterstatusid, name FROM m_fosterstatus');
            }catch(err){
                console.error('ERROR::', error);
                error =err                
            }
            return [error, results];
        }
    }
}
