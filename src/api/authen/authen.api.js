var express = require('express');
var route = express.Router();
var response = require('../../response-data');
var dataApi = require('./authen.data');
var jwt = require('jsonwebtoken');

route.post('/login', async(req, res)=>{
    
    // TODO select user where username & password
    let params = req.body;
    let [error,result,token] = await dataApi.login(params);
    let data;
    if(error){
        data = response.error(error);
    }else{
        data = response.success(result,'login Success');
    }
  
    response.sending(req, res, data, token);
});
route.post('/verify', (req, res)=>{

    // TODO Verify Token jwt 
    let token = req.body.token;
    let data;
    jwt.verify(token, 'babylover', (err, result) =>{

        if(err){
           data = response.error(err.message);
           res.status(403);
        }else{
            let [error,result] =  dataApi.decode(token);
            if(error){
                data = response.error(error);
            }else{
                data = response.success(result,'verify Success');
            } 
        }
        response.sending(req, res, data);
    });

});
module.exports = route;