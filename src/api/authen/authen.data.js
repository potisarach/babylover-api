var connection = require('../../dbconnection');
var jwt = require('jsonwebtoken');

let datalogin = async (data) => {
    let sql =  'SELECT username,password,email FROM user WHERE username = ? AND password = ?';
    let results = await connection.query(sql,[data.username,data.password]);
    if(results.length != 0){
        return results;
    }else{
        return false;
    }  
};

let res = {  
    login:async (data) => {
        let result;
        let error;
        let temp;
        let token;
        try {
            result = await datalogin(data);
            if(result.length != 0){
                for(let login of result){
                    temp = {
                        username: login.username,
                        email: login.email
                    } 
                }
                token = jwt.sign(
                    {temp} ,
                    'babylover',
                    {expiresIn: '1h'}
                ); 
                result = temp;
                result.token = token;
            }
        } catch (error) {
            error = error;
        }
        return [error, result, token];
    },
    decode: (data) => {
        let result;
        let error;
        let dataDecode;
        let temp;
        let token;
        try {
            dataDecode = jwt.decode(data, 'babylover');
            if(dataDecode){
                temp = {
                    username: dataDecode.temp.username,
                    email: dataDecode.temp.email
                } 
                token = jwt.sign(
                    {temp} ,
                    'babylover',
                    {expiresIn: '1h'}
                );
                result = dataDecode.temp;
                result.token = token;
            }
        } catch (error) {
            error = error;
        }
        return [error, result];
    },
};
module.exports = res;