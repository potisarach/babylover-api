var connection = require('../../dbconnection');
var _conf = require('../../config');
var fs = require('fs')
var uuid = require('uuidv4')

let putPerson = async (data) => {

    let pathimg;
    //console.log('picture>>>>>>>>>>>', data.picturepath);

    if (data.picturepath) {
        let base64toImage = data.picturepath.split(';base64,').pop();
        let picture_name = uuid() + ".jpg";
        pathimg =  picture_name;
        fs.writeFile(_conf.getUploadPathImgPerson()+"/" + picture_name, base64toImage, 'base64', function (err) {
            console.log("Error Upload : "+err);
            if(!err){
                
            }
        });
    }
    console.log('pictureName>>>>>', pathimg)
    let values = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "picturepath": pathimg
    }
    let valuesupdate = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
        "picturepath": pathimg
    }

    let sql;
    if (data.personid && data.picturepath == '') {
        sql = 'UPDATE person SET ? WHERE personid=?';
        let person = await connection.query(sql, [valuesupdate, data.personid]);
        return person.affectedRows;
    } else if (data.personid) {
        sql = 'UPDATE person SET ? WHERE personid=?';
        let person = await connection.query(sql, [values, data.personid]);
        return person.affectedRows;
    } else {
        sql = 'INSERT INTO person SET ?';
        let person = await connection.query(sql, values);
        let personId = person.insertId;
        return personId;
    }
}
let insertOrphan = async (data, personid) => {
    let sql = 'INSERT INTO orphan SET ?'
    let values = {
        "orphancode": data.orphancode,
        "orphanageid": data.orphanageid,
        "personid": personid,
        "orphanevaluationstatusid": 2
    }
    let orphan = await connection.query(sql, values)
    let orphanid = orphan.insertId
    console.log(orphanid);
    return orphanid;
}
let updateOrphan = async (data) => {
    let sql = 'UPDATE orphan SET ? WHERE orphanid=?'
    let values = {
        "orphancode": data.orphancode,
        "orphanageid": data.orphanageid,
    }
    let orphan = await connection.query(sql, [values, data.orphanid])
    let orphanid = orphan.insertId
    console.log(orphanid);
    return orphanid;
}
let putorphanstatus = async (orphanid) => {
    let values = {
        "orphanid": orphanid,
        "statusid": 1,
        "detail": "เริ่มเข้าสู่โครงการอุปถัมภ์"
    }
    let sql = 'INSERT INTO orphanstatus SET ?';
    let results = await connection.query(sql, values);
    return results.insertId;
}
let updateorphanstatus = async (data) => {
    let resultupdate;
    let resultinsert;
    let values = {
        "orphanid": data.orphanid,
        "statusid": data.statusid,
        "detail": data.detail
    }    
    if (data.orphanstatusid) {
        if (data.statusid != 2 && data.patronageid != null) {
            let sql = 'UPDATE orphanstatus SET ? WHERE orphanstatusid = ?';
            resultupdate = await connection.query(sql, [values, data.orphanstatusid]);
            console.log(JSON.stringify(values));
            let sqldel = 'DELETE FROM patronage WHERE patronageid = ?'
            resultdel = await connection.query(sqldel, data.patronageid)            
        }
        else {
            let sql = 'UPDATE orphanstatus SET ? WHERE orphanstatusid = ?';
            resultupdate = await connection.query(sql, [values, data.orphanstatusid]);
            console.log(JSON.stringify(values));
            if (data.statusid != 3 && data.statusid != 5 && data.fosterid != null) {
                let sqldeletefoster = 'DELETE FROM foster WHERE orphanid = ?';
                let resultdelfoster = await connection.query(sqldeletefoster, data.orphanid);
                let sqldeleteaddress = 'DELETE FROM address WHERE personid = ?';
                let resultdeleteaddress = await connection.query(sqldeleteaddress, data.fosterperson);
                let sqldeleteperson = 'DELETE FROM person WHERE personid = ?';
                let resultdelperson = await connection.query(sqldeleteperson, data.fosterperson);  
            } else if(data.statusid != 4 && data.originalfamilyid != null){
                let sqldeleteoriginalfamily = 'DELETE FROM originalfamily WHERE orphanid = ?';
                let resultdeloriginalfamily = await connection.query(sqldeleteoriginalfamily, data.orphanid);
                let sqldeleteaddress = 'DELETE FROM address WHERE personid = ?';
                let resultdeleteaddress = await connection.query(sqldeleteaddress, data.originalfamilyperson);
                let sqldeleteperson = 'DELETE FROM person WHERE personid = ?';
                let resultdelperson = await connection.query(sqldeleteperson, data.originalfamilyperson); 
            }
        }
    } else {
        let sql = 'INSERT INTO orphanstatus SET ?';
        resultinsert = await connection.query(sql, values);
        console.log(JSON.stringify(values));
        console.log('id => ' + resultinsert.insertId);
        return resultinsert.insertId;        
    }
}

let putPatronage = async (data) => {
    let resultinsert;
    let resultupdate;
    if (data.orphanid) {
        if (data.patronid) {
            let valuesinsert = {
                "patronid": data.patronid,
                "orphanid": data.orphanid,
                "joinfamilydate": data.joinfamilydate
            }
            let valuesupdate = {
                "patronid": data.patronid,
                "orphanid": data.orphanid
            }
            let orphanstatusvalus = {
                "statusid": 2,
                "orphanid": data.orphanid
            }
            if (data.patronageid) {

                let sql = 'UPDATE patronage SET ? WHERE patronageid = ?';
                resultupdate = await connection.query(sql, [valuesupdate, data.patronageid]);
                console.log(JSON.stringify('Update => ' + valuesupdate));

            } else {
                let sql = 'INSERT INTO patronage SET ?';
                resultinsert = await connection.query(sql, valuesinsert);
                console.log(JSON.stringify('Insert => ' + valuesinsert));
                let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?';
                resultupdate = await connection.query(sqlupdate, [orphanstatusvalus, data.orphanid]);
                if(data.fosterid){
                    let sqldeletefoster = 'DELETE FROM foster WHERE orphanid = ?';
                    let resultdelfoster = await connection.query(sqldeletefoster, data.orphanid);
                    let sqldeleteaddress = 'DELETE FROM address WHERE personid = ?';
                    let resultdeleteaddress = await connection.query(sqldeleteaddress, data.fosterperson);
                    let sqldeleteperson = 'DELETE FROM person WHERE personid = ?';
                    let resultdelperson = await connection.query(sqldeleteperson, data.fosterperson);  
                }
                else if(data.originalfamilyid){
                    let sqldeleteoriginalfamily = 'DELETE FROM originalfamily WHERE orphanid = ?';
                    let resultdeloriginalfamily = await connection.query(sqldeleteoriginalfamily, data.orphanid);
                    let sqldeleteaddress = 'DELETE FROM address WHERE personid = ?';
                    let resultdeleteaddress = await connection.query(sqldeleteaddress, data.originalfamilyperson);
                    let sqldeleteperson = 'DELETE FROM person WHERE personid = ?';
                    let resultdelperson = await connection.query(sqldeleteperson, data.originalfamilyperson); 
                }
            }
        } else {
            if (data.patronageid) {
                let statusvalus = {
                    "statusid": 1,
                }
                let sqlupdate = 'UPDATE orphanstatus SET ? WHERE orphanid = ?'
                let resultupdate = await connection.query(sqlupdate, [statusvalus, data.orphanid])

                let sql = 'DELETE FROM patronage WHERE patronageid = ?';
                let resultDelete = await connection.query(sql, data.patronageid);
            }

        }
    }
}
var data = {
    put: (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let result;
            let error;
            let personid = data.personid;
            let path;
            try {

                if (personid) {
                    let effects = await putPerson(data);
                    let orphanid = await updateOrphan(data);
                    result = { success: true };
                } else {
                    personid = await putPerson(data, path);
                    let orphanid = await insertOrphan(data, personid);
                    let orphanstatusid = await putorphanstatus(orphanid);
                    result = {
                        "personid": personid,
                        "orphanid": orphanid,
                        "orphanstatusid": orphanstatusid
                    }
                }
                // console.log(result)
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                        error = err;
                    } else {
                        console.log('COMMIT')
                        callback({ error: error, data: result })
                    }
                })
            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') })
            }
            callback({ error: error, data: result })
        })
    },
    getorphanstatus: async (orphanid) => {
        let results;
        let error;
        try {
            let sql = `
        SELECT 
            orphan.personid AS orphanpersonid,
            originalfamily.originalfamilyid AS originalfamilyid,
            foster.fosterid AS fosterid,
            foster.personid AS fosterperson,
            originalfamily.personid AS originalfamilyperson, 
            orphanstatus.orphanstatusid,
            orphan.orphanid,
            orphanstatus.statusid,
            orphanstatus.updatestatusdate,
            orphanstatus.detail,
            patronage.patronageid,
            patron.patronid,
            pap.firstname,
            pap.lastname,
            pap.birthdate,
            pap.nickname,
            orp.birthdate AS birthdateorphan,
            orp.nickname AS nicknameorphan,
            address.homeno,
            address.moono,
            address.road,
            address.soi,
            address.tumbolcode,
            address.amphurcode,
            address.provincecode,
            address.zipcode,
            m_province.name AS provincename,
            m_amphur.name AS amphurname,
            m_tumbol.name AS tumbolname,
            fn_fullname(orp.prefixid, orp.Firstname, orp.lastname) AS fullnameorphan,
            fn_fullname(pap.prefixid, pap.Firstname, pap.lastname) AS fullname,
            fn_age(pap.birthdate) AS agepatron,
            fn_age(orp.birthdate) AS ageorphan
        FROM orphan
            LEFT JOIN person orp ON orphan.personid = orp.personid
            LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
            LEFT JOIN patronage ON orphan.orphanid = patronage.orphanid
            LEFT JOIN patron ON patronage.patronid = patron.patronid
            LEFT JOIN person pap ON patron.personid = pap.personid
            LEFT JOIN foster ON foster.orphanid = orphan.orphanid
            LEFT JOIN originalfamily ON originalfamily.orphanid = orphan.orphanid
            LEFT JOIN address ON pap.personid = address.personid
            LEFT JOIN m_province ON address.provincecode = m_province.code
            LEFT JOIN m_amphur ON address.amphurcode = m_amphur.code
            LEFT JOIN m_tumbol ON address.tumbolcode = m_tumbol.code  
                
        WHERE 
                orphan.orphanid = ? OR ? = ''
            `;

            results = await connection.query(sql, [orphanid, orphanid]);

        } catch (error) {
            error = error;
        }
        return [error, results];
    },
    updateOrphanStatus: async (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let result;
            let error;
            try {
                if (data.orphanstatusid && data.statusid == 2) {
                    let patronage = await putPatronage(data);
                    result = { success: true };
                } else if (data.orphanstatusid) {
                    let orphanstatusid = await updateorphanstatus(data);
                    result = { success: true };
                } else {
                    let orphanstatusid = await updateorphanstatus(data);
                    result = {
                        "orphanid": data.orphanid,
                        "orphanstatusid": orphanstatusid
                    }
                }
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                        error = err;
                    } else {
                        console.log('COMMIT')
                        callback({ error: error, data: result })
                    }
                })
            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') })
            }
            callback({ error: error, data: result })
        })
    },
    list: async (orphanid) => {
        let error
        let result
        try {
            let sql = `
                SELECT  
                    person.personid,
                    person.citizenid,
                    person.firstname,
                    person.lastname,
                    person.nickname,
                    person.genderid,
                    person.prefixid,
                    person.birthdate,
                    orphan.orphanid,
					orphanstatus.orphanstatusid,
					orphanstatus.statusid,
					m_orphanstatus.name AS orphanstatusname
                FROM person 
			        INNER JOIN orphan ON person.personid = orphan.personid
                    LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
			        LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid 
                WHERE 
                    orphan.orphanid = ? OR ? = 0
                ORDER BY 
                    orphan.orphanid ASC
                `;

            result = await connection.query(sql, [orphanid, orphanid]);

        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    get: async (orphanid, serverhost) => {

        let error
        let result

        try {
            let sql = `
            SELECT  
                person.personid,
                person.citizenid,
                person.firstname,
                person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                m_gender.gendername,
                CONCAT('${serverhost}' , person.picturepath) as picturepath,
                orphan.orphanid,
				orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                fn_fullname(person.prefixid, person.firstname, person.lastname) AS fullnameorphan,
                m_orphanstatus.name AS orphanstatusname,
                fn_age(birthdate) AS age
            FROM person 
			    INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
                LEFT JOIN m_gender ON m_gender.genderid = person.genderid
            WHERE 
                orphan.orphanid = ? OR ? = 0
            ORDER BY 
                orphan.orphanid ASC
        `;
            result = await connection.query(sql, [orphanid, orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result];

    },  getorphanadvance: async (orphanid, serverhost) => {

        let error
        let result

        try {
            let sql = `
            SELECT 
            orphan.personid AS orphanpersonid,
            orphan.orphanid,
            patronage.patronageid,
            patronage.joinfamilydate,
            patron.patronid,
            par.firstname,
            par.lastname,
            par.birthdate,
            par.nickname,
            orp.birthdate AS birthdateorphan,
            orp.nickname AS nicknameorphan,
			CONCAT('${serverhost}' , orp.picturepath) as picturepath,
            fn_fullname(orp.prefixid, orp.Firstname, orp.lastname) AS fullnameorphan,
            fn_fullname(par.prefixid, par.Firstname, par.lastname) AS fullnamepatron,
            fn_age(par.birthdate) AS agepatron,
            fn_age(orp.birthdate) AS ageorphan
        FROM orphan
            LEFT JOIN person orp ON orphan.personid = orp.personid
            LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
            LEFT JOIN patronage ON orphan.orphanid = patronage.orphanid
            LEFT JOIN patron ON patronage.patronid = patron.patronid
            LEFT JOIN person par ON patron.personid = par.personid
            WHERE 
                orphan.orphanid = ? OR ? = 0
            ORDER BY 
                orphan.orphanid ASC
        `;
            result = await connection.query(sql, [orphanid, orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result];

    }, search: async (fullname, agestartorphan, ageendorphan, orphanstatus) => {

        if (agestartorphan == "") {
            agestartorphan = 0;
        }
        if (ageendorphan == "") {
            ageendorphan = 999;
        }
        let error
        let result
        fullname = '%' + fullname + '%';
        try {
            let sql = `
            SELECT  
                person.personid,
                person.citizenid,
				person.firstname,
				person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                orphan.orphanid,
                orphanhis.orphanhistoryid,
                orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                orphanstatus.updatestatusdate,
                orphanstatus.detail,
				m_orphanstatus.name AS orphanstatusname,
                fn_fullname(prefixid, firstname, lastname) AS fullname,
                fn_age(birthdate) AS age
            FROM person 
                INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
                LEFT JOIN orphanhistory orphanhis ON orphan.orphanid = orphanhis.orphanid
            WHERE 
                (fn_fullname(prefixid, firstname, lastname) LIKE ? OR (? = "")) 
                AND 
                (fn_age(birthdate)  BETWEEN ? AND ?)
                AND 
                (orphanstatus.statusid = ? OR (? = ""))
            ORDER BY 
                BirthDate DESC
        `;
            result = await connection.query(sql, [fullname, fullname, agestartorphan, ageendorphan, orphanstatus, orphanstatus]);
        } catch (err) {
            error = err;
        }
        return [error, result];

    }, searchorphanadvancement: async (fullname, agestartorphan, ageendorphan) => {

        if (agestartorphan == "") {
            agestartorphan = 0;
        }
        if (ageendorphan == "") {
            ageendorphan = 999;
        }
        let error
        let result
        fullname = '%' + fullname + '%';
        try {
            let sql = `
            SELECT  
                person.personid,
                person.citizenid,
				person.firstname,
				person.lastname,
                person.nickname,
                person.genderid,
                person.prefixid,
                person.birthdate,
                orphan.orphanid,
                orphanhis.orphanhistoryid,
                orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                orphanstatus.updatestatusdate,
                orphanstatus.detail,
				m_orphanstatus.name AS orphanstatusname,
                fn_fullname(prefixid, firstname, lastname) AS fullname,
                fn_age(birthdate) AS age
            FROM person 
                INNER JOIN orphan ON person.personid = orphan.personid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
                LEFT JOIN orphanhistory orphanhis ON orphan.orphanid = orphanhis.orphanid
            WHERE 
                (fn_fullname(prefixid, firstname, lastname) LIKE ? OR (? = "")) 
                AND 
                (fn_age(birthdate)  BETWEEN ? AND ?)
                AND 
                (orphanstatus.statusid = 1 || orphanstatus.statusid = 2 )
            ORDER BY 
                BirthDate DESC
        `;
            result = await connection.query(sql, [fullname, fullname, agestartorphan, ageendorphan]);
        } catch (err) {
            error = err;
        }
        return [error, result];

    }

}
module.exports = data;