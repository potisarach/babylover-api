var connection = require('../../dbconnection');
var _conf = require('../../config');
var fs = require('fs');
var uuid = require('uuidv4');

let InsertAdvancement = async (data) => {
    console.log("Data Advancement > > >", data.orphanadvancementid);

    let pathimg;

    if (data.picturepath) {
        let base64toImage = data.picturepath.split(';base64').pop();
        let picture_name = uuid() + ".jpg";
        pathimg = picture_name;
        fs.writeFile(_conf.getUploadPathImgPerson() + "/" + picture_name, base64toImage, 'base64', function (err) {
            console.log("Error Upload : " + err);
            if (!err) {

            }
        });
    }
    console.log('pictureName>>>>>', pathimg)
    value = {
        "orphanid": data.orphanid,
        "picturepath": pathimg,
        "weight": data.weight,
        "height": data.height,
        "aroundhead": data.aroundhead,
        "chest": data.chest,
        "pastchanges": data.pastchanges,
        "eat": data.eat,
        "sleep": data.sleep,
        "excretion": data.excretion,
        "characters": data.characters,
        "updatedate": data.updatedate
    }
    update = {
        "picturepath": pathimg,
        "weight": data.weight,
        "height": data.height,
        "aroundhead": data.aroundhead,
        "chest": data.chest,
        "pastchanges": data.pastchanges,
        "eat": data.eat,
        "sleep": data.sleep,
        "excretion": data.excretion,
        "characters": data.characters,
        "updatedate": data.updatedate
    }
    updatenotpic = {
        "weight": data.weight,
        "height": data.height,
        "aroundhead": data.aroundhead,
        "chest": data.chest,
        "pastchanges": data.pastchanges,
        "eat": data.eat,
        "sleep": data.sleep,
        "excretion": data.excretion,
        "characters": data.characters,
        "updatedate": data.updatedate,
    }

    if (data.orphanadvancementid && data.picturepath == "") {
        sql = 'UPDATE orphanadvancement SET ? WHERE orphanadvancementid = ?';
        let orphanadvancement = await connection.query(sql, [updatenotpic, data.orphanadvancementid]);
        return orphanadvancement.affectedRows;
    } else if (data.orphanadvancementid && data.picturepath != "") {
        sql = 'UPDATE orphanadvancement SET ? WHERE orphanadvancementid = ?';
        let orphanadvancement = await connection.query(sql, [update, data.orphanadvancementid]);
        return orphanadvancement.affectedRows;
    } else {
        sql = 'INSERT INTO orphanadvancement SET ?'
        let InsertAdvancementDate = await connection.query(sql, value);
        let OrphanAdvancementID = InsertAdvancementDate.insertId;
        return OrphanAdvancementID;
    }

}
var data = {
    PutAdvancement: (data, callback) => {
        connection.beginTransaction(async (beginError) => {
            if (beginError) throw beginError;
            let error;
            let result;
            let orphanadvancementid = data.orphanadvancementid;
            try {
                if (orphanadvancementid) {
                    let effects = await InsertAdvancement(data);
                    result = { success: true };
                } else {
                    let orphanadvancementid = await InsertAdvancement(data);
                    result = {
                        "OrphanAdvancementID": orphanadvancementid,
                    }
                }

                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log('ROLLBACK')
                        })
                        error = err;
                    } else {
                        console.log('COMMIT !')
                    }
                })
            } catch (err) {
                error = err
                connection.rollback((e) => {
                    console.log('ROLLBACK 2 ', e)
                })
            }

            callback({ error: error, data: result });
        })
    },
    GetAdvancement: async (orphanadvancementid, orphanid) => {
        let results;
        let error;
        try {
            sql = `
        SELECT 	
	        orphanadvancementid,
	        orphanid,
	        pastchanges,
        	eat,
        	sleep,
            excretion,
        	characters,
        	updatedate,
	        (YEAR(updatedate)+543) AS Year
        FROM
	        orphanadvancement
        WHERE
            orphanadvancementid = ? OR ? = 0
        AND
            orphanid = ? OR ? = 0
        
               `;
            results = await connection.query(sql, [orphanadvancementid, orphanadvancementid, orphanid, orphanid])

        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    SearchAdvancement: async (searchdate, orphanid, serverhost) => {
        let result;
        let error;
        try {
            let sql = `
        SELECT 	
	        orphanadvancementid,
            orphanid,
            CONCAT('${serverhost}' , picturepath) AS picturepath,
            weight,
            height,
            aroundhead,
            chest,
	        pastchanges,
        	eat,
        	sleep,
            excretion,
        	characters,
        	updatedate,
	        (YEAR(updatedate)+543) AS Year
        FROM
	        orphanadvancement
	    WHERE
			(YEAR(updatedate)+543) = ?
		AND 
            orphanid = ?
        ORDER BY orphanadvancementid DESC
            `;
            result = await connection.query(sql, [searchdate, orphanid]);


        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    GetYear: async (orphanid) => {
        let results;
        let error;
        try {
            sql = `
        SELECT DISTINCT
            (YEAR(updatedate)+543) AS Year
        FROM
	        orphanadvancement
        WHERE
            orphanid = ? OR ? = 0
               `;
            results = await connection.query(sql, [orphanid, orphanid]);

        } catch (err) {
            error = err;
        }
        return [error, results]
    },
    CountYear: async (orphanid, years) => {
        let error;
        let results;
        try {
            sql = `
            SELECT 
                COUNT(YEAR(UpdateDate)) AS Years
            FROM 
                orphanadvancement
            WHERE 
                YEAR(UpdateDate) = ? 
            AND 
                orphanadvancement.orphanid = ?
                `
            results = await connection.query(sql, [years, orphanid]);
        } catch (err) {
            error = err;
        }
        console.log("Results > > >", results);

        return error, results;
    },

    DeleteOrphanadvancement: async (orphanadvancementid) => { // ลบข้อมูลล่าสุดของเด็ก
        let error;
        let results;
        try {
            sql = `
            DELETE FROM 
            orphanadvancement 
            WHERE orphanadvancementid = ?
            `
            results = await connection.query(sql, [orphanadvancementid]);
        } catch (err) {
            error = err;
        }
        return [error, results]
    },

    GetGuideAdvancement: async (orphanadvancementid, serverhost) => { // ดึงข้อมูลล่าสุดของเด็ก
        let error;
        let results;
        try {
            sql = `
            SELECT 	
                orphanadvancementid,
                orphanid,
                CONCAT('${serverhost}' , picturepath) AS picturepath,
                weight,
				height,
				aroundhead,
				chest,
                pastchanges,
                eat,
                sleep,
                excretion,
                characters,
                updatedate
            FROM
                orphanadvancement
            WHERE
                orphanid = ?
                    ORDER BY orphanadvancementid DESC
                    LIMIT 1
            `
            results = await connection.query(sql, [orphanadvancementid]);
        } catch (err) {
            error = err
        }
        return [error, results]
    }
}

module.exports = data;