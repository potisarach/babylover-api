var express = require('express');
var route = express.Router();
var data = require('./advancement.data');
var api = require('../../response-data');
var _conf = require('../../config');

route.post('/put', (req, res, next) => {
    let response;
    console.log("Body > > >", req.body);

    data.PutAdvancement(req.body, (result) => {
        if (result.error) {
            response = api.error(result.error);
        } else {
            response = api.success(result, 'Success');
        }
        api.sending(req, res, response);
    });

});

route.post('/get', async (req, res) => {
    let orphanadvancementid = req.body.orphanadvancementid;
    let orphanid = req.body.orphanid;
    let [error, results] = await data.GetAdvancement(orphanadvancementid, orphanid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }
    api.sending(req, res, response);

});

route.post('/search', async (req, res) => {
    let serverhost = _conf.getImagePersonUrl() + '/';
    let searchdate = req.body.searchdate;
    let orphanid = req.body.orphanid;
    let [error, results] = await data.SearchAdvancement(searchdate, orphanid, serverhost)
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }
    api.sending(req, res, response);
});

route.post('/getyear', async (req, res) => {
    let orphanid = req.body.orphanid;
    let [error, results] = await data.GetYear(orphanid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }
    api.sending(req, res, response);
});

route.post('/countyear', async (req, res) => {
    let orphanid = req.body.orphanid;
    let years = req.body.years;

    console.log("Orphanid > > >", orphanid);
    console.log("Years > > >", years);

    let error, results = await data.CountYear(orphanid, years);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }
    console.log("Results >  >  >", results);

    api.sending(req, res, response);

});

route.post('/delete', async (req, res) => {
    console.log("Body", req.body);

    let orphanadvancementid = req.body.orphanadvancementid;

    let error, results = await data.DeleteOrphanadvancement(orphanadvancementid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed');
    }

    api.sending(req, res, response);

});

route.post('/getguide', async(req, res) =>{ 
    // เรียกใช้ข้อมูลล่าสุดของเด็กเพื่อแสดงเป็น guide ให้ผู้ใช้กรอกข้อมูลใหม่ได้จากข้อมูลเดิม
    let serverhost = _conf.getImagePersonUrl() + '/';

    let orphanid = req.body.orphanid

    let error, results = await data.GetGuideAdvancement(orphanid, serverhost)
    let response
    if(error) {
        response = api.error(error)
    }else{
        response = api.success(results, 'Successed')
    }

    api.sending(req,res, response)
});

module.exports = route;