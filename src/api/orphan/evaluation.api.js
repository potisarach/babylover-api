var express = require('express');
var route = express.Router();
var data = require('./evaluation.data');
var api = require('../../response-data');

route.post('/put', (req, res) => { //function นี้ใช้งาน
    data.put(req.body, (result) => {
        let response;
        if (result.error) {
            response = api.error(error);
        } else {
            response = api.success(result.data, 'Successed, Added Orphan');
        }
        api.sending(req, res, response);
    })
});

route.post('/addorphandetail', async (req, res) => { //function นี้ใช้งาน
    data.addorphandetail(req.body, (result) => {
        let response;
        if (result.error) {
            response = api.error(result.error);
        } else {
            response = api.success(result.data, 'Successed, Added OrphanDetail');
        }
        api.sending(req, res, response);
    })
});

route.post('/orphan/list', async (req, res) => { //function นี้ใช้งาน

    let [error, results] = await data.list(0);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Get Orphan');
    }
    api.sending(req, res, response);
});

route.post('/orphan/getUpdatePerson', async (req, res) => {
    let orphanid = req.body.orphanid
    let [error, results] = await data.getUpdatePerson(orphanid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Update Orphan');
    }
    api.sending(req, res, response);
});

route.post('/get', async (req, res) => { //function นี้ใช้งาน
    let orphanid = req.body.orphanid;
    let response;
    if (!orphanid) {
        response = api.error('orphanid Not found.');
        api.sending(req, res, response);
        return;
    }
    let [error, orphanresult, vaccineresult] = await data.get(orphanid);
    if (error) {
        response = api.error(error);
    } else {
        let result = orphanresult[0];
        result.vaccine = vaccineresult;
        response = api.success(result, 'Successed Get Orphan.');
    }
    api.sending(req, res, response);
});

module.exports = route;