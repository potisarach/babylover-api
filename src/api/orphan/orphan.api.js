var express = require('express');
var route = express.Router();
var data = require('./orphan.data');
var api = require('../../response-data');
var multer = require('multer')
var _conf = require('../../config');

route.post('/put', (req, res) => { //function นี้ใช้งาน
    data.put(req.body, (result) => {
        let response;
        if (result.error) {
            response = api.error(result.error);
        } else {
            response = api.success(result.data, 'Successed, Added Orphan');
        }
        api.sending(req, res, response);
    })
});

route.post('/orphan/addOrphanDetail', async (req, res) => { //function นี้ใช้งาน
    let orphanid = req.body.orphanid;
    let response;
    if (!orphanid) {
        response = api.error('orphanid Not found.');
        api.sending(req, res, response);
        return;
    }
    let [error, results] = await data.addOrphanDetail(req.body, orphanid);
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, update person');
    }
    api.sending(req, res, response);
});

route.post('/updateorphanstatus', async (req,res) => { //function นี้ใช้งาน
    let orphanid = req.body.orphanid;
    let response;
    if(!orphanid){
        response = api.error('orphanid Not found');
        api.sending(req, res, response);
    }
    data.updateOrphanStatus(req.body, (results) => {
        if(results.error){
            response = api.error(results.error);
        }else{
            response = api.success(results, 'putOrphanStatus Success');
        }
        api.sending(req, res, response);
    });
});

route.post('/getorphanstatus', async(req, res)=>{
    let response;
    let orphanid = req.body.orphanid;
    let [error,results] = await data.getorphanstatus(orphanid);
    if(error){
        response = api.error(error);
    }else{
        response = api.success(results[0], 'Getorphanstatus Success');
    }
    api.sending(req, res, response);
});

route.post('/orphan/updatePerson', async (req, res) => {
    let personid = req.body.personid;
    let response;
    if (!personid) {
        response = api.error('personid Not found.');
        api.sending(req, res, response);
        return;
    }
    let [error, results] = await data.updatePerson(req.body, personid);

    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, update person');
    }
    api.sending(req, res, response);
});

route.post('/orphan/list', async (req, res) => { //function นี้ใช้งาน

    let [error, results] = await data.list(0);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Get Orphan');
    }
    api.sending(req, res, response);
});

route.post('/orphan/getUpdatePerson', async (req, res) => {
    let orphanid = req.body.orphanid
    let [error, results] = await data.getUpdatePerson(orphanid);
    let response;
    if (error) {
        response = api.error(error);
    } else {
        response = api.success(results, 'Successed, Update Orphan');
    }
    api.sending(req, res, response);
});

route.post('/get', async (req, res) => { //function นี้ใช้งาน
    let serverhost = _conf.getImagePersonUrl()+'/';
    let orphanid = req.body.orphanid;
    let response;
    if (!orphanid) {
        response = api.error('orphanid Notfound.');
        api.sending(req, res, response);
        return;
    }

    let [error, result] = await data.get(orphanid, serverhost);
    if(error){
        response = api.error(result, error);
    }else{
        response = api.success(result[0], 'Successed Get Orphan.');
    }
    //console.log("serverhost :: " + serverhost);
    api.sending(req, res, response);
    
});

route.post('/getorphanadvance', async (req, res) => {
    let serverhost = _conf.getImagePersonUrl()+'/';
    let orphanid = req.body.orphanid;
    let response;
    if(!orphanid){
        response = api.error('OrphanID Notfound.');
        api.sending(req, res, response);
        return;
    }
    let [error, result] = await data.getorphanadvance(orphanid, serverhost);
    if(error){
        response = api.error(result, error);
    }else{
        response = api.success(result[0],'Success Get Orphan.');
    }
    api.sending(req, res, response)
});

route.post('/orphan/search', async (req, res) => { //function นี้ใช้งาน
    let fullname = req.body.fullname;
    let agestartorphan = req.body.agestartorphan;
    let ageendorphan = req.body.ageendorphan;
    let orphanstatus = req.body.orphanstatus;
    let [error, results] = await data.search(fullname, agestartorphan, ageendorphan, orphanstatus);
    let response;
    if (error) {
        response = api.error(error);        
    } else {
        response = api.success(results, 'Successed, Search Orphan');
    }
    api.sending(req, res, response);
});

route.post('/searchadvancement', async(req, res) =>{ 
    let fullname = req.body.fullname;
    let agestartorphan = req.body.agestartorphan;
    let ageendorphan = req.body.ageendorphan;
    let [error, results] = await data.searchorphanadvancement(fullname, agestartorphan, ageendorphan);
    let response;
    if(error){
        response = api.error(error)
    }else{
        response = api.success(results, "Successed, Search Orphan");
    }
    api.sending(req, res, response);

});
module.exports = route;