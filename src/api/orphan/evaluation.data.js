var connection = require('../../dbconnection');

let putPerson = async (data) => {
    let values = {
        "citizenid": data.citizenid,
        "firstname": data.firstname,
        "lastname": data.lastname,
        "nickname": data.nickname,
        "genderid": data.genderid,
        "prefixid": data.prefixid,
        "birthdate": data.birthdate,
    }
    let sql;
    if (data.personid) {
        sql = 'UPDATE person SET ? WHERE personid=?';
        let person = await connection.query(sql, [values, data.personid]);
        return person.affectedRows;
    } else {
        sql = 'INSERT INTO person SET ?';
        let person = await connection.query(sql, values);
        return person.insertId;
    }
}

let insertOrphan = async (data, personid) => {
    let sql = 'INSERT INTO orphan SET ?'
    let values = {
        "orphancode": data.orphancode,
        "orphanageid": data.orphanageid,
        "personid": personid
    }
    let orphan = await connection.query(sql, values)
    let orphanid = orphan.insertId
    return orphanid;
}

let updateOrphan = async (data) => {
    let sql = 'UPDATE orphan SET ? WHERE orphanid=?'
    let values = {
        "orphancode": data.orphancode,
        "orphanageid": data.orphanageid
    }
    let orphan = await connection.query(sql, [values, data.orphanid])
    let orphanid = orphan.insertId
    return orphanid;
}

let putOrphanhistory = async (data) => {
    let values = {
        "orphanid": data.orphanid,
        "fathername": data.fathername,
        "mathername": data.mathername,
        "bornlocation": data.bornlocation,
        "historybefore": data.historybefore,
        "bodygrowth": data.bodygrowth,
        "emotionsocial": data.emotionsocial,
        "languagelearn": data.languagelearn,
        "planworkwithorphan": data.planworkwithorphan
    }
    let sql;
    if (data.orphanhistoryid) {
        sql = 'UPDATE orphanhistory SET ? WHERE orphanid=?';
        let orphanhistory = await connection.query(sql, [values, data.orphanid])
        let orphan = orphanhistory.insertId;
        return orphan.affectedRows;
    } else {
        sql = 'INSERT INTO orphanhistory SET ?';
        let orphanhistory = await connection.query(sql, [values]);
        return orphanhistory.insertId
    }
}

let putVaccine = async (data) => {

    let sqlvaccine = `
                SELECT
                    orphan.orphanid,
                    orphan.personid,
                    vccinject.vaccineinjectionid,
                    vccinject.vaccineid,
                    vccinject.vaccinedate
                FROM orphan
                    INNER JOIN vaccineinjection vccinject ON orphan.orphanid = vccinject.orphanid
                WHERE 
                    orphan.orphanid = ? OR ? = 0
                `;
    vaccineresult = await connection.query(sqlvaccine, [data.orphanid, data.orphanid]);

    let vaccines = data.vaccine
    for (let newitem of vaccines) {
        if (newitem.vaccineinjectionid) {
            for (let item of vaccineresult) {
                if (newitem.vaccineinjectionid == item.vaccineinjectionid) {
                    let updatevaccinevalues = {
                        "vaccineid": newitem.vaccineid,
                        "vaccinedate": newitem.vaccinedate
                    }

                    let sqlupdatevaccine = 'UPDATE vaccineinjection SET ? WHERE vaccineinjectionid = ?';
                    updatevaccine = await connection.query(sqlupdatevaccine, [updatevaccinevalues, newitem.vaccineinjectionid]);
                }
            }
        }
        else {
            let insertvaccinevalues = {
                "vaccineid": newitem.vaccineid,
                "vaccinedate": newitem.vaccinedate,
                "orphanid": data.orphanid
            }

            let sqlinsertvaccine = 'INSERT INTO vaccineinjection SET ?';
            insertvaccine = await connection.query(sqlinsertvaccine, insertvaccinevalues)
        }
    }
    for (let item of vaccineresult) {
        let isHere = false;
        for (let newitem of vaccines) {
            if (item.vaccineinjectionid == newitem.vaccineinjectionid) {
                isHere = true;
            }
        }
        if (!isHere) {
            let sqldelvaccine = 'DELETE FROM vaccineinjection WHERE vaccineinjectionid = ?';
            delvaccine = await connection.query(sqldelvaccine, item.vaccineinjectionid);
        }
    }
}

var data = {
    put: (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let result;
            let error;
            let personid = data.personid;
            try {

                if (personid) {
                    let effects = await putPerson(data);
                    let orphanid = await updateOrphan(data);
                    result = { success: true };
                } else {
                    personid = await putPerson(data);
                    let orphanid = await insertOrphan(data, personid);
                    result = {
                        "personid": personid,
                        "orphanid": orphanid,
                    }
                }
                console.log(result)
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                        error = err;
                    } else {
                        console.log('COMMIT')
                        callback({ error: error, data: result })
                    }
                })
            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') })
            }
            callback({ error: error, data: result })
        })

    },

    addorphandetail: (data, callback) => {
        connection.beginTransaction(async (err) => {
            if (err) throw err;
            let result;
            let error;

            try {
                if (data.orphanid) {
                    let personUpdate = await putPerson(data);
                    let updateorphan = await updateOrphan(data);
                    let orphanhistoryUpdate = await putOrphanhistory(data);
                    let vaccine = await putVaccine(data);
                    result = {
                        success: true,
                        "personid": data.personid,
                        "orphanid": data.orphanid,
                        "uporphanhistory": orphanhistoryUpdate,
                        "vaccine": vaccine
                    };
                } else {
                    personid = await putPerson(data);
                    let orphan = await insertOrphan(data, personid);
                    let orphanhistory = await putOrphanhistory(data);
                    let vaccine = await putVaccine(data);
                    result = {
                        "personid": personid,
                        "orphanid": orphan,
                        "uporphanhistory": orphanhistory,
                        "vaccine": vaccine
                    }
                }

                console.log(result)
                connection.commit(async (err) => {
                    if (err) {
                        connection.rollback(async () => { console.log('RB 1') });
                        error = err;
                    } else {
                        console.log('COMMIT')
                        callback({ error: error, data: result })
                    }
                })
            } catch (err) {
                error = err;
                connection.rollback(async () => { console.log('RB 2') })
            }
            callback({ error: error, data: result })
        })

    },
    list: async (orphanid) => {
        let error
        let result
        try {
            let sql = `
                SELECT  
                    person.personid,
                    person.citizenid,
                    person.firstname,
                    person.lastname,
                    person.nickname,
                    person.genderid,
                    person.prefixid,
                    person.birthdate,
                    orphan.orphanid,
                    orphanstatus.orphanstatusid,
                    orphanstatus.statusid,
                    m_orphanstatus.name AS orphanstatusname
                FROM person 
                    INNER JOIN orphan ON person.personid = orphan.personid
                    LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                    LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
                WHERE
                    orphan.orphanid = ? OR ? = 0
                ORDER BY 
                    orphan.orphanid ASC
                `;

            result = await connection.query(sql, [orphanid, orphanid]);

        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    getUpdatePerson: async (orphanid) => {
        let error
        let result
        try {
            let sql = `
        SELECT  
            person.personid,
            person.citizenid,
			person.firstname,
			person.lastname,
            person.nickname,
            person.genderid,
            person.prefixid,
            person.birthdate,
            orphan.orphanid
        FROM person
            INNER JOIN orphan ON person.personid = orphan.personid
        WHERE 
            orphan.orphanid = ? OR ? = 0
        ORDER BY 
            orphan.orphanid ASC
        `;
            result = await connection.query(sql, [orphanid, orphanid]);
        } catch (err) {
            error = err;
        }
        return [error, result];
    },
    get: async (orphanid) => {
        let error;
        let orphanresult;
        let vaccineresult;
        let sql;
        try {
            sql = `
            SELECT
                orphan.orphanid,
                orphan.personid,
                orphanstatus.orphanstatusid,
                orphanstatus.statusid,
                m_orphanstatus.name AS orphanstatusname,
	            person.citizenid,
                person.genderid,
                person.prefixid,
				m_prefix.name prefixname,
                person.firstname,
                person.lastname,
                person.nickname,
                person.birthdate,
                ophis.orphanhistoryid,
                ophis.bodygrowth,
                ophis.bornlocation,
                ophis.emotionsocial,
                ophis.fathername,
                ophis.mathername,
                ophis.historybefore,
                ophis.languagelearn,
                ophis.planworkwithorphan
            FROM orphan
                INNER JOIN person ON orphan.personid = person.personid
                LEFT JOIN orphanhistory ophis ON orphan.orphanid = ophis.orphanid
                LEFT JOIN orphanage ON orphan.orphanageid = orphanage.orphanageid	
                LEFT JOIN m_prefix ON person.prefixid = m_prefix.prefixid
                LEFT JOIN orphanstatus ON orphan.orphanid = orphanstatus.orphanid
                LEFT JOIN m_orphanstatus ON m_orphanstatus.orphanstatusid = orphanstatus.statusid
            WHERE  
                orphan.orphanid = ? OR ? = 0
            ORDER BY 
                orphan.orphanid ASC
            `;
            orphanresult = await connection.query(sql, [orphanid, orphanid]);

            sql = `
                SELECT
                    orphan.orphanid,
                    orphan.personid,
                    vccinject.vaccineinjectionid,
                    vccinject.vaccineid,
                    vccinject.vaccinedate
                FROM orphan
                    INNER JOIN vaccineinjection vccinject ON orphan.orphanid = vccinject.orphanid
                WHERE 
                    orphan.orphanid = ? OR ? = 0
                ORDER BY 
                    orphan.orphanid ASC
                `;
            vaccineresult = await connection.query(sql, [orphanid, orphanid]);

        } catch (err) {
            error = err;
        }
        return [error, orphanresult, vaccineresult];
    }

}
module.exports = data;