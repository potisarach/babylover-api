module.exports = {
    keyLowercase: function(obj){
       if(Array.isArray(obj)){
            for(let i=0; i<obj.length; i++){
               this.keyLowercase(obj[i]); 
            }
       }else{
            let keyLower;
            for(let key in obj){
                keyLower = key.toLowerCase();
                obj[keyLower] = obj[key];
                if(typeof obj[key] == "object"){
                    this.keyLowercase(obj[key]); 
                }
                if(keyLower != key){
                    delete obj[key];
                }
            }
       }
    },
}