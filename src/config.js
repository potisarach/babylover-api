var path = require('path');
var appDir = path.dirname(require.main.filename);
var mode = 'prod';
var conf = { 
    protocal: 'https',
    prod_host: 'babyloverapi.herokuapp.com',
    prod_image: 'babyloverapi.herokuapp.com',
    prod_db_host: 'db4free.net',
    prod_db: 'babylover',
    prod_db_usr: 'babylover',
    prod_db_pwd: 'P@$$w0rd',
    
    dev_host: '192.168.1.203',
    dev_image: 'localhost:8000',
    dev_db: 'babylover',
    dev_db_usr: 'root',
    dev_db_pwd: 'password',
    root_path: appDir,
    image_public_path: '/images',
    getHost: function(){ return this[mode+'_host']},
    getImage: function () { return this[mode+'_image']},
    getDBHost: function(){ return this[mode+'_db_host']},
    getDBUser: function(){ return this[mode+'_db_usr']},
    getDBPassword: function(){ return this[mode+'_db_pwd']},
    getDBName: function(){ return this[mode+'_db']},
    getImageHostUrl: function(){
        return this.protocal+'://'+this.getImage()+''+this.image_public_path
    },
    getImagePersonUrl: function(filepath){
        var path = this.protocal+'://'+this.getImage()+''+this.image_public_path;
        path += "/img/person";
        if(filepath){
            path += "/"+filepath;
        }
        return path
    },
    getUploadPathImgPerson: function(){
       return this.root_path + "/upload/img/person";
    }
}

module.exports = conf;